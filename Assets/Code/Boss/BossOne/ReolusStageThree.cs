﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReolusStageThree : Boss
{
    private float abilityCooldownTime = 4;
    private float autoAttackCooldownTime = 4;

    private GameObject sword;
    private GameObject wanderingPrefab;
    private float abilityTimer = 0;
    private float autoAttackTimer = 0;
    private PureCorruption pureCorrupt;
    private Explosion explosion;
    private ChaosWave chaosWave;
    private AutoAttack autoAttack;

    private bool useExplosion = false;
    private bool usePureCorruption = false;
    private bool useWanderingCorruption = false;

    enum Abilities { Explosion, ChaosWave, PureCorruption, WanderingCorruption };
    
    public void Start()
    {
        GameStateController gameState = GameObject.Find("GameStateController(Clone)").GetComponent<GameStateController>();
        health = float.Parse(gameState.GetProperty(ReolusStageOne.REOLUS_HEALTH));
        
        sword = Resources.Load("Prefabs/Reolus/StageThree/SwordThrow", typeof(GameObject)) as GameObject;
        wanderingPrefab = Resources.Load("Prefabs/Reolus/StageThree/WanderingCorruption", typeof(GameObject)) as GameObject;
        pureCorrupt = gameObject.GetComponent<PureCorruption>();
        explosion = gameObject.GetComponent<Explosion>();
        chaosWave = (ChaosWave)gameObject.AddComponent<ChaosWave>().SetBoss(this);
        autoAttack = (AutoAttack)gameObject.AddComponent<AutoAttack>().SetBoss(this);

        pureCorrupt.SetBoss(this);
        explosion.SetBoss(this);
        EnableAbility(30, Abilities.Explosion);
        EnableAbility(Random.Range(5, 15), Abilities.PureCorruption);
        EnableAbility(Random.Range(5, 15), Abilities.WanderingCorruption);
        autoAttackTimer = Time.time + autoAttackCooldownTime;
        chaosWave.TriggerAbility();
    }

    public override void Update()
    {
        if (isServer)
        {
            if (Time.time > abilityTimer)
            {
                if (useExplosion)
                {
                    explosion.TriggerAbility();
                    useExplosion = false;
                    chaosWave.HaltWaves();
                    pureCorrupt.StopAbility();
                    EnableAbility(30, Abilities.Explosion);
                }
                else if (usePureCorruption)
                {
                    pureCorrupt.TriggerAbility();
                    usePureCorruption = false;
                    EnableAbility(Random.Range(20, 30), Abilities.PureCorruption);
                }
                else if (useWanderingCorruption)
                {
                    GameObject wanderingObject = SpawnGameObject(wanderingPrefab, gameObject.transform.position, Quaternion.identity);
                    WanderingCorruption wanderingCorruption = wanderingObject.GetComponent<WanderingCorruption>();
                    wanderingCorruption.TriggerAbility();
                    useWanderingCorruption = false;
                    EnableAbility(Random.Range(15, 20), Abilities.WanderingCorruption);
                }
                else
                {
                    GameObject swordObject = SpawnGameObject(sword, gameObject.transform.position, Quaternion.identity);
                    SwordThrow swordThrow = swordObject.GetComponent<SwordThrow>();
                    swordThrow.SetBoss(this);
                    swordThrow.TriggerAbility();
                }

                abilityTimer = Time.time + abilityCooldownTime;
            }

            if (Time.time > autoAttackTimer)
            {
                autoAttackTimer = Time.time + autoAttackCooldownTime;
                autoAttack.TriggerAbility();
            }

            if (explosion.UpdateAbility())
            {
                chaosWave.StartWaves();
            }
            pureCorrupt.UpdateAbility();
            chaosWave.UpdateAbility();
            autoAttack.UpdateAbility();
            FindMostDamage();
        }
    }

    private void EnableAbility(float timeToStop, Abilities ability)
    {
        StartCoroutine(EnableAbilityCoroutine(timeToStop, ability));
    }

    private IEnumerator EnableAbilityCoroutine(float timeToStop, Abilities ability)
    {
        yield return new WaitForSeconds(timeToStop);
        if (ability == Abilities.Explosion)
        {
            useExplosion = true;
        }
        else if (ability == Abilities.WanderingCorruption)
        {
            useWanderingCorruption = true;
        }
        else if (ability == Abilities.PureCorruption)
        {
            usePureCorruption = true;
        }
    }
}