﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Knight : SpawnedCharacter {
    public static float knightSpeed = 5;
    public static float knightHealth = 1000;

    private float autoAttackTimer;
    private float abilityTimer;
    private int abilityCooldown = 3;
    private bool isCasting = false;
    private KnightCharge charge;
    
    public void Start()
    {
        if (isServer)
        {
            SetSpeed(knightSpeed);
            maxHealth = knightHealth;
            health = maxHealth;
            autoAttackTimer = Time.time;
            targetedPlayer = FindClosestPlayer();
            abilityTimer = Time.time;
            stopDistance = 1;
        }
    }

    public override void Update()
    {
        if (isServer)
        {
            if (!isCasting)
            {
                MoveTo(targetedPlayer.transform.position);
                AutoAttack();
            }
            if (Time.time - abilityTimer > abilityCooldown)
            {
                isCasting = true;
                StopMovement();
                abilityTimer = Time.time;
                abilityCooldown = Random.Range(8, 15);
                charge = new KnightCharge();
                charge.knight = this;
                charge.TriggerAbility();
            }
            if (isCasting)
            {
                if (charge.UpdateAbility())
                {
                    isCasting = false;
                    abilityTimer = Time.time;
                    autoAttackTimer = Time.time;
                }
            }
                
            FindMostDamage();
            base.Update();
        }
    }

    private void AutoAttack()
    {
        if (Time.time - autoAttackTimer > 1.5)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                if (Vector3.Distance(transform.position, player.transform.position) < 2)
                {
                    float cone = Mathf.Cos(45 * Mathf.Deg2Rad);
                    Vector3 heading = (player.transform.position - transform.position).normalized;

                    if (Vector3.Dot(transform.forward, heading) > cone)
                    {
                        CauseDamage(player.GetComponent<Character>(), 100);
                    }
                }
            }
            autoAttackTimer = Time.time;
        }
    }

    public class KnightCharge
    {
        private static float minimumDistance = 3;
        private static float chargeSpeed = 10;
        private static float channelTime = 1;
        private static float damageRange = 2;
        private static float damageAmount = 400;
        private static float damageOverTimeAmount = 200;
        private static float damageOverTimeLength = 10;

        private float chargeTimer = -99999999;
        private Vector3 targetPosition;
        private bool isCharging = false;
        private bool isChanneling = false;
        private float nextTickTimer = -99999999;
        public Boss knight;

        public void TriggerAbility()
        {
            chargeTimer = Time.time;
            List<GameObject> players = GameObject.FindGameObjectsWithTag("Player").ToList();
            List<GameObject> filteredPlayers = new List<GameObject>();
            GameObject farthestPlayer = null;
            float farthestDistance = 0;

            foreach (GameObject player in players)
            {
                float distanceBetween = Vector3.Distance(player.transform.position, knight.transform.position);
                if (distanceBetween < minimumDistance)
                {
                    filteredPlayers.Add(player);
                }
                if (distanceBetween > farthestDistance)
                {
                    farthestPlayer = player;
                }
            }
            if (filteredPlayers.Count() != 0)
            {
                targetPosition = filteredPlayers.ElementAt(UnityEngine.Random.Range(0, filteredPlayers.Count())).transform.position;
            }
            else
            {
                targetPosition = farthestPlayer.transform.position;
            }
            isChanneling = true;
        }

        public bool UpdateAbility()
        {
            if (isCharging && !knight.isMoving)
            {
                knight.SetSpeed(Knight.knightSpeed);
                isCharging = false;
                return true;
            }
            else if (isCharging && Time.time > nextTickTimer)
            {
                nextTickTimer = Time.time + .2F;
                PulseDamage();
            }
            if (isChanneling && !isCharging && Time.time - chargeTimer > channelTime)
            {
                chargeTimer = Time.time;
                knight.SetSpeed(chargeSpeed);
                knight.MoveTo(targetPosition);
                isCharging = true;
                isChanneling = false;
            }
            return false;
        }

        private void PulseDamage()
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                if (Vector3.Distance(knight.transform.position, player.transform.position) < damageRange)
                {
                    knight.CauseDamage(player.GetComponent<Character>(), damageAmount + damageAmount / 100);
                    knight.CmdCauseDamageOverTime(player, damageOverTimeAmount + damageAmount / 100, damageOverTimeLength, "Charge");
                }
            }
        }
    }
}
