﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class DoorKeeperRegen : Boss {
    private static float doorKeeperSpeed = 5;
    private static float doorKeeperHealth = 10000;
    private static float spinDamage = 1800;
    private static float regenAmount = 200;

    private float autoAttackTimer;
    private float abilityTimer;
    private int abilityCooldown = 3;
    private bool isCasting = false;
    private Spin spin = new Spin();
    private Slam slam = new Slam();
    private Regen regen = new Regen();
    public GameObject slamBullet;
    private enum MovePool { Slam, Spin, Regen };
    private MovePool currentMove = MovePool.Slam;
    private MovePool nextMove = MovePool.Spin;
    
    public void Start()
    {
        if (isServer)
        {
            SetSpeed(doorKeeperSpeed);
            maxHealth = doorKeeperHealth;
            health = maxHealth;
            autoAttackTimer = Time.time;
            targetedPlayer = FindClosestPlayer();
            abilityTimer = Time.time;
            stopDistance = 1;
        }
    }

    public override void Update()
    {
        if (isServer)
        {
            if (!isCasting)
            {
                MoveTo(targetedPlayer.transform.position);
                AutoAttack();
            }

            if (Time.time - abilityTimer > abilityCooldown)
            {
                switch (currentMove)
                {
                    case MovePool.Slam:
                        slam.TriggerAbility(this, targetedPlayer.transform.position, slamBullet);
                        nextMove = MovePool.Spin;
                        break;
                    case MovePool.Spin:
                        spin.TriggerAbility(this);
                        nextMove = MovePool.Regen;
                        break;
                    case MovePool.Regen:
                        regen.TriggerAbility(this);
                        nextMove = MovePool.Slam;
                        break;
                }
                isCasting = true;
                StopMovement();
                abilityTimer = Time.time;
                abilityCooldown = Random.Range(8, 15);

            }
            if (isCasting)
            {
                bool updateFinished = false;
                switch (currentMove)
                {
                    case MovePool.Slam:
                        updateFinished = slam.UpdateAbility();
                        break;
                    case MovePool.Spin:
                        updateFinished = spin.UpdateAbility();
                        break;
                    case MovePool.Regen:
                        updateFinished = regen.UpdateAbility();
                        break;
                }
                if (updateFinished)
                {
                    isCasting = false;
                    abilityTimer = Time.time;
                    autoAttackTimer = Time.time;
                    currentMove = nextMove;
                }
            }
                
            FindMostDamage();
            base.Update();
        }
    }

    private void HandleAbilityTrigger()
    {

    }

    private void AutoAttack()
    {
        if (Time.time - autoAttackTimer > 1.5)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                if (Vector3.Distance(transform.position, player.transform.position) < 2)
                {
                    float cone = Mathf.Cos(45 * Mathf.Deg2Rad);
                    Vector3 heading = (player.transform.position - transform.position).normalized;

                    if (Vector3.Dot(transform.right, heading) > cone)
                    {
                        CauseDamage(player.GetComponent<Character>(), 150);
                    }
                }
            }
            autoAttackTimer = Time.time;
        }
    }

    public class Spin
    {
        private static float channelTime = 2;
        private float chargeTimer = -99999999;
        private Character owner;

        public void TriggerAbility(Character owner)
        {
            chargeTimer = Time.time;
            this.owner = owner;
        }

        public bool UpdateAbility()
        {
            if (Time.time - chargeTimer > channelTime)
            {
                GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                if (Vector3.Distance(owner.transform.position, player.transform.position) < 2)
                {
                    owner.CauseDamage(player.GetComponent<Character>(), spinDamage);
                }
            }
                return true;
            }
            return false;
        }
    }

    public class Slam
    {
        private static float channelTime = 2;
        private static float range = 11;
        private float chargeTimer = -99999999;
        private Character owner;
        private Vector3 shootingPosition;
        private GameObject slamBullet;

        public void TriggerAbility(DoorKeeperRegen owner, Vector3 shootingPosition, GameObject slamBullet)
        {
            chargeTimer = Time.time;
            this.shootingPosition = shootingPosition;
            this.owner = owner;
            this.slamBullet = slamBullet;
        }

        public bool UpdateAbility()
        {
            if (Time.time - chargeTimer > channelTime)
            {
                GameObject slamShockwave = owner.SpawnGameObject(slamBullet, owner.transform.position, Quaternion.identity);
                Vector3 aimingPosition = (shootingPosition - owner.transform.position).normalized * range;
                aimingPosition = shootingPosition + aimingPosition;
                slamShockwave.GetComponent<SlamShockwave>().ShootAt(aimingPosition);
                return true;
            }
            return false;
        }
    }

    public class Regen
    {
        private float chargeTimer = -99999999;
        private Character owner;
        public float regenDamageTaken = 0;

        public void TriggerAbility(Character owner)
        {
            chargeTimer = Time.time;
            this.owner = owner;
            regenDamageTaken = 0;
        }

        public bool UpdateAbility()
        {
            owner.HealDamage(regenAmount * Time.deltaTime + regenAmount * ((Time.time - chargeTimer) / 4) * Time.deltaTime);
            return regenDamageTaken > 1000 || owner.health == owner.maxHealth;
        }
    }

    public override void TakeDamage(float damageAmount, Character damageDealer, string moveName = null)
    {
        if (isCasting && currentMove == MovePool.Regen)
        {
            regen.regenDamageTaken += damageAmount;
        }
        base.TakeDamage(damageAmount, damageDealer, moveName);
    }

    public void OnDestroy()
    {
        GameObject boss = GameObject.Find("Boss(Clone)");
        if (boss != null)
        {
            boss.GetComponent<ReolusStageTwo>().InformMiniBossDead();
        }
    }
}
