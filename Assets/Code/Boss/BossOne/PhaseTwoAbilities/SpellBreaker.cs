﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class SpellBreaker : SpawnedCharacter {
    public static float spellBreakerSpeed = 5;
    public static float spellBreakerHealth = 1000;
    public static float chainDamage = 500;
    public static float chainDamageTime = 4;

    private float autoAttackTimer;
    private float abilityTimer;
    private int abilityCooldown = 3;
    private bool isCasting = false;
    private ChainPull chainPull;
    public GameObject reflectShield;
    private bool lastAbilityReflect = true;
    
    public void Start()
    {
        if (isServer)
        {
            SetSpeed(spellBreakerSpeed);
            maxHealth = spellBreakerHealth;
            health = maxHealth;
            autoAttackTimer = Time.time;
            targetedPlayer = FindClosestPlayer();
            abilityTimer = Time.time;
            stopDistance = 1;
        }
    }

    public override void Update()
    {
        if (isServer)
        {
            if (!isCasting)
            {
                MoveTo(targetedPlayer.transform.position);
                AutoAttack();
            }
            if (Time.time - abilityTimer > abilityCooldown)
            {
                if (lastAbilityReflect)
                {
                    isCasting = true;
                    StopMovement();
                    abilityTimer = Time.time;
                    abilityCooldown = Random.Range(8, 15);
                    chainPull = new ChainPull();
                    chainPull.TriggerAbility(gameObject);
                    lastAbilityReflect = false;
                }
                else
                {
                    abilityTimer = Time.time;
                    abilityCooldown = Random.Range(8, 15);
                    ReflectShield shield = SpawnGameObject(reflectShield, transform.position, Quaternion.identity).GetComponent<ReflectShield>();
                    shield.SetOwner(gameObject);
                    lastAbilityReflect = true;
                }

            }
            if (isCasting)
            {
                if (chainPull.UpdateAbility())
                {
                    isCasting = false;
                    abilityTimer = Time.time;
                    autoAttackTimer = Time.time;
                }
            }
                
            FindMostDamage();
            base.Update();
        }
    }

    private void AutoAttack()
    {
        if (Time.time - autoAttackTimer > 1.5)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                if (Vector3.Distance(transform.position, player.transform.position) < 2)
                {
                    float cone = Mathf.Cos(45 * Mathf.Deg2Rad);
                    Vector3 heading = (player.transform.position - transform.position).normalized;

                    if (Vector3.Dot(transform.right, heading) > cone)
                    {
                        CauseDamage(player.GetComponent<Character>(), 125);
                    }
                }
            }
            autoAttackTimer = Time.time;
        }
    }

    public class ChainPull
    {
        private static float channelTime = 2;
        private float chargeTimer = -99999999;
        public Boss spellBreaker;

        public void TriggerAbility(GameObject owner)
        {
            chargeTimer = Time.time;
            List<GameObject> players = GameObject.FindGameObjectsWithTag("Player").ToList();
            if (players.Count() > 2)
            {
                int playerIndex = Random.Range(0, players.Count);
                
                GameObject player = players.ElementAt(playerIndex);
                Vector3 target = Vector3.Normalize(owner.transform.position - player.transform.position) * 3;
                PlayerCharacter playerChar = player.GetComponent<PlayerCharacter>();
                playerChar.ForceMovementTo(target, 1);
                playerChar.RpcSlow(0.3F, 4);
                players.RemoveAt(playerIndex);

                player = players.ElementAt(playerIndex);
                target = Vector3.Normalize(owner.transform.position - player.transform.position) * 3;
                playerChar = player.GetComponent<PlayerCharacter>();
                playerChar.ForceMovementTo(target, 1);
                playerChar.RpcSlow(0.3F, 4);
                spellBreaker.CmdCauseDamageOverTime(player, chainDamage, chainDamageTime, "ChainPull");
            }
            else
            {
                foreach (GameObject player in players)
                {
                    Vector3 target = Vector3.Normalize(owner.transform.position - player.transform.position) * 3;
                    PlayerCharacter playerChar = player.GetComponent<PlayerCharacter>();
                    playerChar.ForceMovementTo(target, 1);
                    playerChar.RpcSlow(0.3F, 4);
                }
            }
        }

        public bool UpdateAbility()
        {
            if (Time.time - chargeTimer > channelTime)
            {
                return true;
            }
            return false;
        }
    }
}
