﻿using UnityEngine;
using System.Collections;
using System;
using System.Timers;
using System.Collections.Generic;
using UnityEngine.Networking;

public class HealingFieldAOE : NetworkBehaviour {
    private float pulseTimer = -999999;
    private List<GameObject> enemies = new List<GameObject>();
    public static float healTickAmount = 190;
    public static float fieldLength = 8;
    public static float tickFrequency = 1;
    
    public void Start()
    {
        if (isServer)
        {
            StartCoroutine(DestroyThis());
        }
    }

    private IEnumerator DestroyThis()
    {
        yield return new WaitForSeconds(fieldLength);
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Enemy"))
        {
            enemies.Add(collider.gameObject);
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.CompareTag("Enemy"))
        {
            enemies.Remove(collider.gameObject);
        }
    }
    
    void OnTriggerStay(Collider collision)
    {
        if (Time.time - pulseTimer > tickFrequency) 
        {
            foreach (GameObject enemy in enemies)
            {
                if (enemy != null)
                {
                    enemy.GetComponent<Character>().HealDamage(healTickAmount);
                }
            }
            pulseTimer = Time.time;
        }
    }
}
