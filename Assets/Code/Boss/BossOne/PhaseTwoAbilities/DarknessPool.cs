﻿using UnityEngine;
using System.Collections;
using System;
using System.Timers;
using System.Collections.Generic;
using UnityEngine.Networking;

public class DarknessPool : NetworkBehaviour {
    private static float damageAmount = 1000;
    private Character owner;

    private List<GameObject> players = new List<GameObject>();

    public void SetOwner(Character owner)
    {
        this.owner = owner;
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            players.Add(collider.gameObject);
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            players.Remove(collider.gameObject);
        }
    }

    void OnTriggerStay(Collider collision)
    {
        if (isServer)
        {
            foreach (GameObject player in players)
            {
                owner.CauseDamage(player.GetComponent<Character>(), damageAmount * Time.deltaTime);
            }
        }
    }
}
