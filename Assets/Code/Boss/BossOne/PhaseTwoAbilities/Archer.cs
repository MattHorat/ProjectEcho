﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Archer : SpawnedCharacter {
    private static float archerSpeed = 5;
    private static float archerHealth = 500;
    private static float range = 7;
    private static float focusShotLength = 5;
    private static float singleShotLength = .4F;
    private static float autoAttackPause = 2;
    private static float chargeShotChannelTime = 1.5F;
    private static float chargedShotSizeIncrease = 3;
    private static float autoAttackDamage = 100;
    private static float focusDamage = 400;
    private static float chargeDamage = 800;

    private float autoAttackTimer;
    private float abilityTimer;
    private int abilityCooldown = 3;
    public GameObject arrow;

    private bool isFocusShot = false;
    private float focusShotTimer;
    private float singleShotTimer;

    private bool isChargeShot = false;
    private float chargeShotTimer;
    private Vector3 chargeAimPosition;
    
    public void Start()
    {
        if (isServer)
        {
            SetSpeed(archerSpeed);
            maxHealth = archerHealth;
            health = maxHealth;
            targetedPlayer = FindClosestPlayer();
            abilityTimer = Time.time;
        }
    }

    public override void Update()
    {
        if (isServer)
        {
            if (Vector3.Distance(transform.position, targetedPlayer.transform.position) < 6 || isChargeShot || isFocusShot)
            {
                StopMovement();
                AutoAttack();
            }
            else
            {
                MoveTo(targetedPlayer.transform.position);
            }
            base.Update();
        }
    }

    private void AutoAttack()
    {
        if (isFocusShot)
        {
            FocusShot();
        }
        else if (isChargeShot) 
        {
            ChargedShot();
        }
        else if (abilityTimer + abilityCooldown < Time.time)
        {
            if (Random.Range(0, 2) == 0)
            {
                isChargeShot = true;
                chargeShotTimer = Time.time + chargeShotChannelTime;
                chargeAimPosition = (targetedPlayer.transform.position - transform.position).normalized * range;
                chargeAimPosition = targetedPlayer.transform.position + chargeAimPosition;
            }
            else
            {
                focusShotTimer = Time.time;
                singleShotTimer = focusShotTimer;
                isFocusShot = true;
            }
            abilityTimer = Time.time;
        }
        else if (Time.time - autoAttackTimer > autoAttackPause)
        {
            Vector3 aimingPosition = (targetedPlayer.transform.position - transform.position).normalized * range;
            aimingPosition = targetedPlayer.transform.position + aimingPosition;
            GameObject arrowBullet = SpawnGameObject(arrow, transform.position, Quaternion.identity);
            Arrow arrowComp = arrowBullet.GetComponent<Arrow>();
            arrowComp.ShootAt(aimingPosition);
            arrowComp.SetDamage(autoAttackDamage);
            autoAttackTimer = Time.time;
            targetedPlayer = FindClosestPlayer();
        }
    }

    private void FocusShot()
    {
        if (focusShotTimer + focusShotLength < Time.time)
        {
            isFocusShot = false;
        }
        else if (singleShotTimer + singleShotLength < Time.time)
        {
            singleShotTimer = Time.time;
            Vector3 aimingPosition = (targetedPlayer.transform.position - transform.position).normalized * range;
            aimingPosition = targetedPlayer.transform.position + aimingPosition;
            GameObject arrowBullet = SpawnGameObject(arrow, transform.position, Quaternion.identity);
            Arrow arrowComp = arrowBullet.GetComponent<Arrow>();
            arrowComp.ShootAt(aimingPosition);
            arrowComp.SetDamage(focusDamage);
            autoAttackTimer = Time.time;
        }
    }

    private void ChargedShot()
    {
        if (Time.time > chargeShotTimer)
        {
            isChargeShot = false;
            GameObject arrowBullet = SpawnGameObject(arrow, transform.position, Quaternion.identity);
            Arrow arrowComp = arrowBullet.GetComponent<Arrow>();
            arrowComp.ShootAt(chargeAimPosition);
            arrowComp.SetDamage(chargeDamage);
            arrowBullet.transform.localScale = new Vector3(transform.localScale.x + chargedShotSizeIncrease, transform.localScale.y, transform.localScale.z + chargedShotSizeIncrease);
            autoAttackTimer = Time.time;
        }
    }
}
