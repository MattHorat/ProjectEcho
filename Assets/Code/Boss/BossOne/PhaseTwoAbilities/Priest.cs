﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Priest : SpawnedCharacter {
    public static float priestSpeed = 5;
    public static float priestHealth = 500;

    private float abilityTimer;
    private int abilityCooldown = 3;
    private bool isCastingBeam = false;
    private bool lastCastWasBeam = true;
    private HealingBeam healingBeam;
    public GameObject healingField;
    
    public void Start()
    {
        if (isServer)
        {
            SetSpeed(priestSpeed);
            maxHealth = priestHealth;
            health = maxHealth;
            targetedPlayer = FindClosestKnight();
            abilityTimer = Time.time;
        }
    }

    public override void Update()
    {
        if (isServer)
        {
            if (!isCastingBeam)
            {
                if (targetedPlayer != null)
                {
                    MoveTo(targetedPlayer.transform.position);
                }
                else
                {
                    targetedPlayer = FindClosestKnight();
                }
            }
            if (Time.time - abilityTimer > abilityCooldown)
            {
                if (lastCastWasBeam)
                {
                    lastCastWasBeam = false;
                    if (targetedPlayer != null)
                    {
                        SpawnHealingField();
                    }
                    abilityTimer = Time.time;
                    abilityCooldown = Random.Range(8, 15);
                }
                else
                { 
                    isCastingBeam = true;
                    lastCastWasBeam = true;
                    StopMovement();
                    abilityTimer = Time.time;
                    abilityCooldown = Random.Range(8, 15);
                    healingBeam = new HealingBeam();
                    healingBeam.TriggerAbility(FindClosestKnight(), this);
                }
            }
            if (isCastingBeam)
            {
                if (healingBeam.UpdateAbility())
                {
                    isCastingBeam = false;
                    abilityTimer = Time.time;
                    targetedPlayer = FindClosestKnight();
                }
            }
            base.Update();
        }
    }

    private void SpawnHealingField()
    {
        SpawnGameObject(healingField, targetedPlayer.gameObject.transform.position, Quaternion.identity);
    }

    private GameObject FindClosestKnight()
    {
        GameObject closestKnight = null;
        GameObject[] gameEntities = GameObject.FindGameObjectsWithTag("Enemy");
        IEnumerable<GameObject> knights = gameEntities.Where(entity => entity.GetComponent<Knight>() != null);
        foreach (GameObject knight in knights)
        {
            if (closestKnight == null || Vector3.Distance(transform.position, knight.transform.position) < Vector3.Distance(transform.position, closestKnight.transform.position))
            {
                closestKnight = knight;
            }
        }
        return closestKnight;
    }

    public class HealingBeam
    {
        private static float healBeamLength = 8;

        private GameObject knight;
        private Character priest;
        private float healBeamTimer;

        public void TriggerAbility(GameObject knight, Character priest)
        {
            this.knight = knight;
            this.priest = priest;
            healBeamTimer = Time.time + healBeamLength;
        }

        public bool UpdateAbility()
        {
            if (Time.time < healBeamTimer && knight != null)
            {
                Vector3 aimingPosition = (knight.gameObject.transform.position - priest.gameObject.transform.position).normalized;
                RaycastHit hitObject;
                if (Physics.Raycast(priest.gameObject.transform.position, aimingPosition, out hitObject))
                {
                    if (hitObject.transform.gameObject != null)
                    {
                        Character character = hitObject.transform.gameObject.GetComponent<Character>();
                        if (character != null)
                        {
                            character.HealDamage(150 * Time.deltaTime);
                            Debug.DrawLine(priest.gameObject.transform.position, character.gameObject.transform.position, Color.green);
                        }
                    }
                }
            }
            else
            {
                return true;
            }
            return false;
        }
    }
}
