﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Lever : MonoBehaviour, Interactable
{
    private ReolusStageTwo owner;
    private GameObject spawn;
    private Vector3 position;
    private bool used = false;
    private bool isLeftSide;

    public void SetupLever(ReolusStageTwo owner, GameObject spawn, Vector3 position, bool isLeftSide)
    {
        this.owner = owner;
        this.spawn = spawn;
        this.position = position;
        this.isLeftSide = isLeftSide;
    }

    public void Interact()
    {
        if (!used)
        {
            owner.SpawnGameObject(spawn, position, Quaternion.identity);
            used = true;
            if (isLeftSide)
            {
                owner.DisableLeftSide();
            }
            else
            {
                owner.DisableRightSide();
            }
        }
    }
}