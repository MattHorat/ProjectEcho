﻿using UnityEngine;
using System.Collections;
using System;

public class Dazzle : AICharacter {
    private static float explosionRange = 15;
    private static float explosionTime = 2;

    private float explosionTimer;

    public override void Start()
    {
        base.Start();
        explosionTimer = Time.time;
    }

    public override void Update()
    {
        if (Time.time - explosionTimer > explosionTime)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                if (player.GetComponent<PlayerCharacter>().isLocalPlayer)
                {
                    if (Vector3.Distance(transform.position, player.transform.position) < explosionRange)
                    {
                        Vector3 targetDir = transform.position - player.transform.position;
                        Vector3 forward = player.transform.forward;
                        float angle = Vector3.Angle(targetDir, forward);
                        if (angle < 40)
                        {
                            player.gameObject.GetComponent<PlayerCharacter>().CauseFlash();
                        }
                    }
                }
            }
            Destroy(gameObject);
        }
    }
}
