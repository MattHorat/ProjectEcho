﻿using UnityEngine;
using System.Collections;
using System;

public class ArcaneOrb : AICharacter {
    private static float orbSpeed = 5;
    private static float damageAmount = 700;
    private static float orbLifeTime = 4;
    private static float orbPulseRange = 4;
    private static float pulseTime = 1;
    private static float pulseExplosionDamage = 160;

    public Character creator;
    private float pulseTimer = -999999;

    public override void Update()
    {
        if (isServer)
        {
            base.Update();

            if (pulseTimer + pulseTime > Time.time)
            {
                PulseDamage(damageAmount);
                pulseTimer = Time.time;
            }

            if (!isMoving)
            {
                Destroy(gameObject);
            }
        }
    }

    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            speed = orbSpeed;
            isMoving = true;
            StartCoroutine(DestroyThis());
        }
    }

    private IEnumerator DestroyThis()
    {
        yield return new WaitForSeconds(orbLifeTime);
        PulseDamage(pulseExplosionDamage);
        Destroy(gameObject);
    }

    public void ShootAt(Vector3 target)
    {
        MoveTo(target);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (isServer)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                Character player = collision.gameObject.GetComponent<Character>();
                CauseDamage(player, damageAmount);
                Destroy(gameObject);
            }
        }
    }

    private void PulseDamage(float damage)
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            if (Vector3.Distance(transform.position, player.transform.position) < orbPulseRange)
            {
                if (!player.GetComponent<PlayerCharacter>())
                {
                    Debug.Log(player);
                }
                creator.CauseDamage(player.GetComponent<PlayerCharacter>(), damage);
            }
        }
    }
}
