﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class Arrow : AICharacter {
    private float arrowSpeed = 10;
    private float damageAmount = 400;

    [SyncVar]
    public Vector3 scale;

    public override void Update()
    {
        if (isServer)
        {
            base.Update();
            scale = transform.localScale;
            if (!isMoving)
            {
                Destroy(gameObject);
            }
        }
        else
        {
            transform.localScale = scale;
        }
    }

    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            speed = arrowSpeed;
            isMoving = true;
        }
        else
        {
            transform.localScale = scale;
        }
    }

    public void ShootAt(Vector3 target)
    {
        MoveTo(target);
    }

    public void SetDamage(float damage)
    {
        damageAmount = damage;
    }

    void OnTriggerEnter(Collider collision)
    {
        if (isServer)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                Character player = collision.gameObject.GetComponent<Character>();
                CauseDamage(player, damageAmount);
            }
        }
    }
}
