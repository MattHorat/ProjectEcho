﻿using UnityEngine;
using System.Collections;
using System;

public class Orb : AICharacter {
    private static float orbSpeed = 5;
    private static float damageAmount = 150;
    private static float orbLifeTime = 4;

    private GameObject target;

    public override void Update()
    {
        if (isServer)
        {
            MoveTo(target.transform.position);
            base.Update();
            if (!isMoving)
            {
                Destroy(gameObject);
            }
        }
    }

    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            speed = orbSpeed;
            isMoving = true;
            StartCoroutine(DestroyThis());
        }
    }

    private IEnumerator DestroyThis()
    {
        yield return new WaitForSeconds(orbLifeTime);
        Destroy(gameObject);
    }

    public void ShootAt(GameObject target)
    {
        this.target = target;
        MoveTo(target.transform.position);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (isServer)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                Character player = collision.gameObject.GetComponent<Character>();
                CauseDamage(player, damageAmount);
                Destroy(gameObject);
            }
        }
    }
}
