﻿using UnityEngine;
using System.Collections;
using System;

public class SlamShockwave : AICharacter {
    private static float shockwaveSpeed = 5;
    private static float damageAmount = 500;

    public Character creator;

    public override void Update()
    {
        if (isServer)
        {
            base.Update();

            if (!isMoving)
            {
                Destroy(gameObject);
            }
        }
    }

    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            stopDistance = 0.5F;
            speed = shockwaveSpeed;
            isMoving = true;
        }
    }

    public void ShootAt(Vector3 target)
    {
        MoveTo(target);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Character player = collision.gameObject.GetComponent<Character>();
            CauseDamage(player, damageAmount);
        }
    }
}
