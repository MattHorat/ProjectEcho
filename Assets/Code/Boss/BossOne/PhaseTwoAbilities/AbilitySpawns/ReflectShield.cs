﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class ReflectShield : NetworkBehaviour {
    private static float despawnTime = 5;

    private GameObject owner;

    public void Start()
    {
        if (isServer)
        {
            StartCoroutine(DestroyThis());
        }
    }

    private IEnumerator DestroyThis()
    {
        yield return new WaitForSeconds(despawnTime);
        Destroy(gameObject);
    }

    public void Update()
    {
        if (isServer)
        {
            if (owner == null)
            {
                Destroy(gameObject);
            }
            else
            {
                transform.position = owner.transform.position;
            }
        }
    }

    public void SetOwner(GameObject owner)
    {
        this.owner = owner;
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            AICharacter projectile = collision.gameObject.GetComponent<AICharacter>();
            projectile.ReverseMovement();
        }
    }
}
