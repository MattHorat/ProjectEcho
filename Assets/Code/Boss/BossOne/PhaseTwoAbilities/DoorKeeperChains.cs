﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class DoorKeeperChains : Boss {
    private static float doorKeeperSpeed = 5;
    private static float doorKeeperHealth = 10000;
    private static float spinDamage = 1800;

    private float autoAttackTimer;
    private float abilityTimer;
    private int abilityCooldown = 3;
    private bool isCasting = false;
    private Spin spin = new Spin();
    private Slam slam = new Slam();
    private ChainPull chainPull = new ChainPull();
    public GameObject slamBullet;
    private enum MovePool { Slam, Spin, ChainPull };
    private MovePool currentMove = MovePool.Slam;
    private MovePool nextMove = MovePool.Spin;
    
    public void Start()
    {
        if (isServer)
        {
            SetSpeed(doorKeeperSpeed);
            maxHealth = doorKeeperHealth;
            health = maxHealth;
            autoAttackTimer = Time.time;
            targetedPlayer = FindClosestPlayer();
            abilityTimer = Time.time;
            stopDistance = 1;
        }
    }

    public override void Update()
    {
        if (isServer)
        {
            if (!isCasting)
            {
                MoveTo(targetedPlayer.transform.position);
                AutoAttack();
            }

            if (Time.time - abilityTimer > abilityCooldown)
            {
                switch (currentMove)
                {
                    case MovePool.Slam:
                        slam.TriggerAbility(this, targetedPlayer.transform.position, slamBullet);
                        nextMove = MovePool.Spin;
                        break;
                    case MovePool.Spin:
                        spin.TriggerAbility(this);
                        nextMove = MovePool.ChainPull;
                        break;
                    case MovePool.ChainPull:
                        chainPull.TriggerAbility(this);
                        nextMove = MovePool.Slam;
                        break;
                }
                isCasting = true;
                StopMovement();
                abilityTimer = Time.time;
                abilityCooldown = Random.Range(8, 15);

            }
            if (isCasting)
            {
                bool updateFinished = false;
                switch (currentMove)
                {
                    case MovePool.Slam:
                        updateFinished = slam.UpdateAbility();
                        break;
                    case MovePool.Spin:
                        updateFinished = spin.UpdateAbility();
                        break;
                    case MovePool.ChainPull:
                        updateFinished = chainPull.UpdateAbility();
                        break;
                }
                if (updateFinished)
                {
                    isCasting = false;
                    abilityTimer = Time.time;
                    autoAttackTimer = Time.time;
                    currentMove = nextMove;
                }
            }
                
            FindMostDamage();
            base.Update();
        }
    }

    private void HandleAbilityTrigger()
    {

    }

    private void AutoAttack()
    {
        if (Time.time - autoAttackTimer > 1.5)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                if (Vector3.Distance(transform.position, player.transform.position) < 2)
                {
                    float cone = Mathf.Cos(45 * Mathf.Deg2Rad);
                    Vector3 heading = (player.transform.position - transform.position).normalized;

                    if (Vector3.Dot(transform.right, heading) > cone)
                    {
                        CauseDamage(player.GetComponent<Character>(), 150);
                    }
                }
            }
            autoAttackTimer = Time.time;
        }
    }

    public class Spin
    {
        private static float channelTime = 2;
        private float chargeTimer = -99999999;
        private Character owner;

        public void TriggerAbility(Character owner)
        {
            chargeTimer = Time.time;
            this.owner = owner;
        }

        public bool UpdateAbility()
        {
            if (Time.time - chargeTimer > channelTime)
            {
                GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                if (Vector3.Distance(owner.transform.position, player.transform.position) < 2)
                {
                    owner.CauseDamage(player.GetComponent<Character>(), spinDamage);
                }
            }
                return true;
            }
            return false;
        }
    }

    public class Slam
    {
        private static float channelTime = 2;
        private static float range = 11;
        private float chargeTimer = -99999999;
        private Character owner;
        private Vector3 shootingPosition;
        private GameObject slamBullet;

        public void TriggerAbility(DoorKeeperChains owner, Vector3 shootingPosition, GameObject slamBullet)
        {
            chargeTimer = Time.time;
            this.shootingPosition = shootingPosition;
            this.owner = owner;
            this.slamBullet = slamBullet;
        }

        public bool UpdateAbility()
        {
            if (Time.time - chargeTimer > channelTime)
            {
                GameObject slamShockwave = owner.SpawnGameObject(slamBullet, owner.transform.position, Quaternion.identity);
                Vector3 aimingPosition = (shootingPosition - owner.transform.position).normalized * range;
                aimingPosition = shootingPosition + aimingPosition;
                slamShockwave.GetComponent<SlamShockwave>().ShootAt(aimingPosition);
                return true;
            }
            return false;
        }
    }

    public class ChainPull
    {
        private Character owner;
        public float regenDamageTaken = 0;
        private GameObject targetedPlayer;

        public void TriggerAbility(Character owner)
        {
            this.owner = owner;
            regenDamageTaken = 0;
            List<GameObject> players = GameObject.FindGameObjectsWithTag("Player").ToList();
            int playerNumber = Random.Range(0, players.Count - 1);
            targetedPlayer = players.ElementAt(playerNumber);
            targetedPlayer.GetComponent<PlayerCharacter>().StunPermanently();
            targetedPlayer.GetComponent<PlayerCharacter>().AlterHealing(-.9F);
        }

        public bool UpdateAbility()
        {
            owner.CauseDamage(targetedPlayer.GetComponent<PlayerCharacter>(), 300 * Time.deltaTime);
            if (regenDamageTaken > 1000)
            {
                targetedPlayer.GetComponent<PlayerCharacter>().RemovePermanentStun();
                targetedPlayer.GetComponent<PlayerCharacter>().AlterHealing(.9F);
                return true;
            }
            return false;
        }
    }

    public override void TakeDamage(float damageAmount, Character damageDealer, string moveName = null)
    {
        if (isCasting && currentMove == MovePool.ChainPull)
        {
            chainPull.regenDamageTaken += damageAmount;
            if (health <= 0)
            {
                targetedPlayer.GetComponent<PlayerCharacter>().RemovePermanentStun();
                targetedPlayer.GetComponent<PlayerCharacter>().AlterHealing(.9F);
            }
        }
        
        base.TakeDamage(damageAmount, damageDealer, moveName);
    }

    public void OnDestroy()
    {
        GameObject boss = GameObject.Find("Boss(Clone)");
        if (boss != null)
        {
            boss.GetComponent<ReolusStageTwo>().InformMiniBossDead();
        }
    }
}
