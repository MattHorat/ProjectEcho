﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class SpawnedCharacter : Boss {
    private static float empowerLength = 100;
    private static float empowerAmount = 5;

    private bool isEmpowered = false;
    private float empowerTimer = 0;
    
    public void Empower()
    {
        empowerTimer = Time.time;
        isEmpowered = true;
    }

    public override void CauseDamage(Character target, float damageAmount, string moveName = null)
    {
        if (isEmpowered && Time.time - empowerTimer < empowerLength)
        {
            damageAmount += damageAmount * empowerAmount;
        }
        else
        {
            isEmpowered = false;
        }

        base.CauseDamage(target, damageAmount);
    }

    public override void CmdCauseDamageOverTime(GameObject target, float damageAmount, float time, string abilityName)
    {
        if (isEmpowered && Time.time - empowerTimer < empowerLength)
        {
            damageAmount += damageAmount * empowerAmount;
        }
        else
        {
            isEmpowered = false;
        }

        base.CmdCauseDamageOverTime(target, damageAmount, time, abilityName);
    }
}
