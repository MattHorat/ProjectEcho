﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Arcanist : SpawnedCharacter {
    private static float arcanistSpeed = 5;
    private static float arcanistHealth = 500;
    private float range = 7;
    private float autoAttackPause = 2;
    private float arcaneOrbCooldown = 5;

    private float autoAttackTimer;
    private float abilityTimer;
    private int abilityCooldown = 3;
    public GameObject orb;
    public GameObject arcaneOrb;
    public GameObject dazzle;
    private float arcaneOrbTimer = -99999;

    public void Start()
    {
        if (isServer)
        {
            SetSpeed(arcanistSpeed);
            maxHealth = arcanistHealth;
            health = maxHealth;
            targetedPlayer = FindClosestPlayer();
            abilityTimer = Time.time;
        }
    }

    public override void Update()
    {
        if (isServer)
        {
            if (Time.time - abilityTimer > abilityCooldown)
            {
                StopMovement();
                SpawnDazzle();
                abilityTimer = Time.time;
                abilityCooldown = Random.Range(8, 15);
                targetedPlayer = FindClosestPlayer();
            }
            else
            {
                if (targetedPlayer == null)
                {
                    targetedPlayer = FindClosestPlayer();
                }
                if (Vector3.Distance(transform.position, targetedPlayer.transform.position) < 6)
                {
                    StopMovement();
                    AutoAttack();
                }
                else
                {
                    MoveTo(targetedPlayer.transform.position);
                }
            }
            
            FindMostDamage();
            base.Update();
        }
    }

    private void AutoAttack()
    {
        if (arcaneOrbTimer + arcaneOrbCooldown < Time.time)
        {
            arcaneOrbTimer = Time.time;
            Vector3 aimingPosition = (targetedPlayer.transform.position - transform.position).normalized * range;
            aimingPosition = targetedPlayer.transform.position + aimingPosition;
            GameObject arrowBullet = SpawnGameObject(arcaneOrb, transform.position, Quaternion.identity);
            arrowBullet.GetComponent<ArcaneOrb>().ShootAt(aimingPosition);
            autoAttackTimer = Time.time;
            targetedPlayer = FindClosestPlayer();
        }
        else if (Time.time - autoAttackTimer > autoAttackPause)
        {
            GameObject orbBullet = SpawnGameObject(orb, transform.position, Quaternion.identity);
            orbBullet.GetComponent<Orb>().ShootAt(targetedPlayer);
            autoAttackTimer = Time.time;
            targetedPlayer = FindClosestPlayer();
        }
    }

    private void SpawnDazzle()
    {
        SpawnGameObject(dazzle, new Vector3(Random.Range(-10, 10), 1, Random.Range(-10, 10)), Quaternion.identity);
    }
}
