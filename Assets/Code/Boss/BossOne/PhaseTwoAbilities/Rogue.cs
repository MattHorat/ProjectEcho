﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Rogue : SpawnedCharacter {
    private static float rogueSpeed = 5;
    private static float rogueHealth = 500;
    private static float shieldLength = 5;
    private static float shieldReturnDamage = .1F;

    private float autoAttackTimer;
    private float abilityTimer;
    private float shieldTimer;
    private int abilityCooldown = 3;
    private Vanish vanish = new Vanish();
    private bool shieldedLast = true;
    private bool isShielded = false;
    
    public void Start()
    {
        SetSpeed(rogueSpeed);
        if (isServer)
        {
            maxHealth = rogueHealth;
            health = maxHealth;
            targetedPlayer = FindClosestPlayer();
            abilityTimer = Time.time;
            stopDistance = 1;
        }
    }

    public override void Update()
    {
        if (isServer)
        {
            MoveTo(targetedPlayer.transform.position);
            AutoAttack();
            if (isShielded && shieldTimer + shieldLength < Time.time)
            {
                isShielded = false;
            }
            if (Time.time - abilityTimer > abilityCooldown)
            {
                if (shieldedLast)
                {
                    StopMovement();
                    abilityTimer = Time.time;
                    abilityCooldown = Random.Range(8, 15);
                    vanish.TriggerAbility(this);
                    shieldedLast = false;
                    targetedPlayer = FindClosestPlayer();
                }
                else
                {
                    StopMovement();
                    abilityTimer = Time.time;
                    abilityCooldown = Random.Range(8, 15);
                    isShielded = true;
                    shieldTimer = Time.time;
                    targetedPlayer = FindClosestPlayer();
                    shieldTimer = Time.time;
                }
            }
            
            FindMostDamage();
            base.Update();
        }
    }

    private void AutoAttack()
    {
        if (Time.time - autoAttackTimer > 1.5)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                if (Vector3.Distance(transform.position, player.transform.position) < 2)
                {
                    float cone = Mathf.Cos(45 * Mathf.Deg2Rad);
                    Vector3 heading = (player.transform.position - transform.position).normalized;

                    if (Vector3.Dot(transform.right, heading) > cone)
                    {
                        CauseDamage(player.GetComponent<Character>(), 100);
                    }
                }
            }
            autoAttackTimer = Time.time;
        }
    }

    public override void TakeDamage(float damageAmount, Character damageDealer, string moveName = null)
    {
        if (isShielded)
        {
            CauseDamage(damageDealer, damageAmount * shieldReturnDamage);
        }
        base.TakeDamage(damageAmount, damageDealer, moveName);
    }

    public class Vanish
    {
        private static float damageAmount = 400;
        private static float damageOverTimeAmount = 900;
        private static float damageOverTimeLength = 10;

        public void TriggerAbility(Rogue rogue)
        {
            List<GameObject> players = GameObject.FindGameObjectsWithTag("Player").ToList();
            int playerNumber = Random.Range(0, players.Count - 1);
            GameObject targetedPlayer = players.ElementAt(playerNumber);
            rogue.transform.position = targetedPlayer.transform.position;
            rogue.CmdCauseDamageOverTime(targetedPlayer, damageOverTimeAmount, damageOverTimeLength, "RoguePoison");
            rogue.CauseDamage(targetedPlayer.GetComponent<Character>(), damageAmount);
        }
    }
}
