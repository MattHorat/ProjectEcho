﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class Boss : PathfindingCharacter {
    protected Vector3 desiredPosition;
    protected Dictionary<string, float> damageChart = new Dictionary<string, float>();
    public GameObject targetedPlayer;

    protected GameObject FindClosestPlayer()
    {
        GameObject closestPlayer = null;
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            if (closestPlayer == null || Vector3.Distance(transform.position, player.transform.position) < Vector3.Distance(transform.position, closestPlayer.transform.position))
            {
                closestPlayer = player;
            }
        }
        return closestPlayer;
    }

    public void GenerateThreat(float threatAmount, string threatCauserName)
    {
        if (isServer)
        {
            if (damageChart.ContainsKey(threatCauserName))
            {
                damageChart[threatCauserName] += threatAmount;
            }
            else
            {
                damageChart.Add(threatCauserName, threatAmount);
            }
        }
    }

    public void LowerThreat(float threatAmount, string threatCauserName)
    {
        if (isServer)
        {
            if (damageChart.ContainsKey(threatCauserName))
            {
                damageChart[threatCauserName] -= threatAmount;
                if (damageChart[threatCauserName] < 0)
                {
                    damageChart[threatCauserName] = 0;
                }
            }
        }
    }

    protected void FindMostDamage()
    {
        float mostDamage = 0;
        string highestDamageDealer = "";
        bool foundPlayer = false;
        if (damageChart.Count > 0)
        {
            foreach(KeyValuePair<string, float> damage in damageChart)
            {
                if (damage.Value > mostDamage)
                {
                    if (GameObject.Find(damage.Key + "(Clone)").GetComponent<PlayerCharacter>().isActive)
                    {
                        foundPlayer = true;
                        mostDamage = damage.Value;
                        highestDamageDealer = damage.Key;
                    }
                }
            }
            if (foundPlayer)
            {
                targetedPlayer = GameObject.Find(highestDamageDealer + "(Clone)");
            }
            else
            {
                targetedPlayer = FindClosestPlayer();
            }
        }
        else
        {
            targetedPlayer = FindClosestPlayer();
        }
    }

    public override void TakeTaunt(string tauntingCharacter)
    {
        float mostDamage = 0;
        string highestDamageDealer = "";
        if (damageChart.Count > 0)
        {
            foreach (KeyValuePair<string, float> damage in damageChart)
            {
                if (damage.Value > mostDamage)
                {
                    mostDamage = damage.Value;
                    highestDamageDealer = damage.Key;
                }
            }
            damageChart[tauntingCharacter] = damageChart[highestDamageDealer] + 3000;
        }
        else
        {
            damageChart[tauntingCharacter] = 3000;
        }
    }

    protected override void LocalTakeDamage(float damageAmount, GameObject damageDealer, string moveName = null) 
    {
        Character damageDealerChar = damageDealer.GetComponent<Character>();
        if (damageChart.ContainsKey(damageDealerChar.characterName))
        {
            damageChart[damageDealerChar.characterName] += damageAmount;
        }
        else
        {
            damageChart.Add(damageDealerChar.characterName, damageAmount);
        }
        base.LocalTakeDamage(damageAmount, damageDealer, moveName);
    }

    public virtual void SkipPhase()
    {
    }
}
