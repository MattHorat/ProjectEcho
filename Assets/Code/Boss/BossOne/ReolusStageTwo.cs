﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReolusStageTwo : Boss
{
    private GameObject lever;
    private GameObject darknessPool;
    private GameObject knight;
    private GameObject priest;
    private GameObject archer;
    private GameObject rogue;
    private GameObject arcanist;
    private GameObject spellBreaker;
    private GameObject doorKeeperRegen;
    private GameObject doorKeeperChains;

    private float spawnTime = 20;
    private float empowerTime = 10;
    private float tetherTime = 10;
    private float tetherChannelTime = 5;
    
    private int lastLeftSpawn = 1;
    private int lastRightSpawn = 2;

    private float spawnTimer = 0;
    private float empowerTimer = 0;
    private float tetherTimer = 0;
    private bool leftSideOpen = true;
    private bool rightSideOpen = true;

    private int deadMiniBosses = 0;

    public void Start()
    {
        lever = Resources.Load("Prefabs/Reolus/Lever", typeof(GameObject)) as GameObject;
        darknessPool = Resources.Load("Prefabs/Reolus/DarknessPool", typeof(GameObject)) as GameObject;
        knight = Resources.Load("Prefabs/Reolus/StageTwo/Knight", typeof(GameObject)) as GameObject;
        priest = Resources.Load("Prefabs/Reolus/StageTwo/Priest", typeof(GameObject)) as GameObject;
        archer = Resources.Load("Prefabs/Reolus/StageTwo/Archer", typeof(GameObject)) as GameObject;
        rogue = Resources.Load("Prefabs/Reolus/StageTwo/Rogue", typeof(GameObject)) as GameObject;
        arcanist = Resources.Load("Prefabs/Reolus/StageTwo/Arcanist", typeof(GameObject)) as GameObject;
        spellBreaker = Resources.Load("Prefabs/Reolus/StageTwo/SpellBreaker", typeof(GameObject)) as GameObject;
        doorKeeperRegen = Resources.Load("Prefabs/Reolus/StageTwo/DoorKeeperRegen", typeof(GameObject)) as GameObject;
        doorKeeperChains = Resources.Load("Prefabs/Reolus/StageTwo/DoorKeeperChains", typeof(GameObject)) as GameObject;
        SpawnGameObject(lever, new Vector3(-10, 0, -1), Quaternion.identity).GetComponent<Lever>().SetupLever(this, doorKeeperRegen, new Vector3(-11, 0, -1), true);
        SpawnGameObject(lever, new Vector3(10, 0, -1), Quaternion.identity).GetComponent<Lever>().SetupLever(this, doorKeeperChains, new Vector3(11, 0, -1), false);
        SpawnGameObject(darknessPool, new Vector3(0, 0, 0), Quaternion.identity).GetComponent<DarknessPool>().SetOwner(this);
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        tetherTimer = Time.time + tetherTime;
    }

    public override void Update()
    {
        if (isServer)
        {
            if (Time.time - spawnTime > spawnTimer)
            {
                if (leftSideOpen)
                {
                    lastLeftSpawn = SpawnSquad(lastLeftSpawn, -10);
                }
                if (rightSideOpen)
                {
                    lastRightSpawn = SpawnSquad(lastRightSpawn, 11);
                }
                spawnTimer = Time.time;
            }
            if (Time.time - empowerTime > empowerTimer)
            {
                GameObject[] possibleSpawns = GameObject.FindGameObjectsWithTag("Enemy");
                List<SpawnedCharacter> confirmedSpawns = new List<SpawnedCharacter>();
                SpawnedCharacter currentChar;
                foreach (GameObject spawn in possibleSpawns)
                {
                    currentChar = spawn.GetComponent<SpawnedCharacter>();
                    if (currentChar != null)
                    {
                        confirmedSpawns.Add(currentChar);
                    }
                }
                if (confirmedSpawns.Count > 0)
                {
                    confirmedSpawns[Random.Range(0, confirmedSpawns.Count)].Empower();
                }
                empowerTimer = Time.time;
            }
            if (Time.time > tetherTimer)
            {
                GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
                if (players.Length > 0)
                {
                    GameObject player = players[Random.Range(0, players.Length)];
                    player.GetComponent<PlayerCharacter>().RpcPullTowards(transform.position, tetherChannelTime);
                }
                tetherTimer = Time.time + tetherTime;
            }
            FindMostDamage();
        }
    }

    public override void TakeDamage(float damageAmount, Character damageDealer, string moveName = null)
    {
    }

    public void SpawnKnightSquad(float xPos)
    {
        SpawnGameObject(knight, new Vector3(xPos, 0, -1), Quaternion.identity);
        SpawnGameObject(knight, new Vector3(xPos, 0, 1), Quaternion.identity);
        SpawnGameObject(knight, new Vector3(xPos, 0, -2), Quaternion.identity);
        float priestXPos = xPos > 0 ? xPos + 2 : xPos - 2;
        SpawnGameObject(priest, new Vector3(priestXPos, 0, -1), Quaternion.identity);
    }

    public void SpawnScoutSquad(float xPos)
    {
        SpawnGameObject(rogue, new Vector3(xPos, 0, 0), Quaternion.identity);
        SpawnGameObject(rogue, new Vector3(xPos, 0, -2), Quaternion.identity);
        float archerXPos = xPos > 0 ? xPos + 1 : xPos - 1;
        SpawnGameObject(archer, new Vector3(archerXPos, 0, 0), Quaternion.identity);
        SpawnGameObject(archer, new Vector3(archerXPos, 0, -2), Quaternion.identity);
    }
    
    public void SpawnArcaneSquad(float xPos)
    {
        SpawnGameObject(spellBreaker, new Vector3(xPos, 0, -1), Quaternion.identity);
        float arcanistXPos = xPos > 0 ? xPos + 1 : xPos - 1;
        SpawnGameObject(arcanist, new Vector3(arcanistXPos, 0, 1), Quaternion.identity);
        SpawnGameObject(arcanist, new Vector3(arcanistXPos, 0, -3), Quaternion.identity);
    }

    public int SpawnSquad(int lastSpawn, float xPos) 
    {
        int nextSpawn = 1;
        if (lastSpawn == 1)
        {
            nextSpawn = Random.Range(2, 4);
        }
        else if (lastSpawn == 2)
        {
            nextSpawn = Random.Range(1, 3) == 1 ? 1 : 3;
        }
        else if (lastSpawn == 3)
        {
            nextSpawn = Random.Range(1, 3);
        }
        
        if (nextSpawn == 1)
        {
            SpawnKnightSquad(xPos);
        }
        else if (nextSpawn == 2) 
        {
            SpawnScoutSquad(xPos);
        }
        else if (nextSpawn == 3)
        {
            SpawnArcaneSquad(xPos);
        }

        return nextSpawn;
    }

    public void DisableLeftSide()
    {
        leftSideOpen = false;
    }

    public void DisableRightSide()
    {
        rightSideOpen = false;
    }

    public void InformMiniBossDead()
    {
        deadMiniBosses++;
        if (deadMiniBosses == 2)
        {
            SkipPhase();
        }
    }

    public override void SkipPhase()
    {
        if (isServer)
        {
            ReolusStageThree newStage = gameObject.AddComponent<ReolusStageThree>();
            GameObject.Find("DarknessPool(Clone)").GetComponent<DarknessPool>().SetOwner(newStage);
            Destroy(this);
        }
    }
}