﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;

public class ReolusSpawner : BossSpawner
{
    public override GameObject SpawnBoss()
    {
        GameObject bossPrefab = Resources.Load("Prefabs/Reolus/Boss", typeof(GameObject)) as GameObject;
        GameObject boss = (GameObject)Instantiate(bossPrefab, new Vector3(0f, 0f, 0f), Quaternion.identity);
        NetworkServer.Spawn(boss);
        return boss;
    }
    public override void DestroyBoss()
    {

    }
}