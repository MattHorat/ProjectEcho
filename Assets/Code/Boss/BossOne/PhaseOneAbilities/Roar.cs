﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Roar : BossAbility
{
    private float channelTime = 5;
    private float spawnPause = 2;

    private float spawnTimer;
    private float channelTimer;
    public GameObject roarPrefab;
    
    public Roar()
    {
        abilityCooldown = 0;
    }

    public override void TriggerAbility()
    {
        channelTimer = Time.time + channelTime;
        spawnTimer = Time.time + (spawnPause / ((ReolusStageOne)boss).rageStacks);
    }

    public override bool UpdateAbility()
    {
        if (Time.time < channelTimer)
        {
            if (Time.time > spawnTimer)
            {
                GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
                foreach (GameObject player in players)
                {
                    GameObject spawn = boss.SpawnGameObject(roarPrefab, player.transform.position, Quaternion.identity);
                    spawn.GetComponent<RoarAOE>().boss = boss;
                }
                spawnTimer = Time.time + (spawnPause / ((ReolusStageOne)boss).rageStacks);
            }
            return false;
        }
        return true;
    }
}