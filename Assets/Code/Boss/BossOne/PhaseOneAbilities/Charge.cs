﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Charge : BossAbility
{
    private static float minimumDistance = 7;
    private static float chargeSpeed = 12;
    private static float channelTime = 1;
    private static float damageRange = 2;
    private static float damageAmount = 1000;
    private static float damageOverTimeAmount = 600;
    private static float damageOverTimeLength = 10;
    private static float shotInterval = .2F;
    
    private float chargeTimer = -99999999;
    private GameObject targetPlayer;
    private bool isCharging = false;
    private bool isChanneling = false;
    private float nextTickTimer = -99999999;
    public GameObject bulletPrefab;

    public override void TriggerAbility()
    {
        chargeTimer = Time.time;
        List<GameObject> players = GameObject.FindGameObjectsWithTag("Player").ToList();
        List<GameObject> filteredPlayers = new List<GameObject>();
        GameObject farthestPlayer = null;
        float farthestDistance = 0;

        foreach (GameObject player in players)
        {
            float distanceBetween = Vector3.Distance(player.transform.position, boss.transform.position);
            if (distanceBetween < minimumDistance && ((ReolusStageOne)boss).targetedPlayer != player) {
                filteredPlayers.Add(player);
            }
            if (distanceBetween > farthestDistance)
            {
                farthestPlayer = player;
            }
        }
        if (filteredPlayers.Count() != 0)
        {
            targetPlayer = filteredPlayers.ElementAt(UnityEngine.Random.Range(0, filteredPlayers.Count()));
        }
        else
        {
            targetPlayer = farthestPlayer;
        }
        isChanneling = true;
    }

    public override bool UpdateAbility()
    {
        if (isCharging && !boss.isMoving)
        {
            boss.SetSpeed(ReolusStageOne.bossSpeed);
            isCharging = false;
            PulseDamage();
            FinalExplosion();
            return true;
        }
        else if (isCharging && Time.time > nextTickTimer)
        {
            nextTickTimer = Time.time + .2F;
            PulseDamage();
        }
        if (isChanneling && !isCharging && Time.time - chargeTimer > channelTime)
        {
            chargeTimer = Time.time;
            boss.SetSpeed(chargeSpeed);
            if (targetPlayer != null)
            {
                boss.MoveTo(targetPlayer.transform.position);
            }
            isCharging = true;
            isChanneling = false;
        }
        return false;
    }

    private void PulseDamage()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            if (Vector3.Distance(boss.transform.position, player.transform.position) < damageRange)
            {
                boss.CauseDamage(player.GetComponent<Character>(), damageAmount + damageAmount / 100 * 5 * ((ReolusStageOne)boss).rageStacks);
                boss.CmdCauseDamageOverTime(player, damageOverTimeAmount + damageAmount / 100 * 5 * ((ReolusStageOne)boss).rageStacks, damageOverTimeLength, "Charge");
            }
        }
    }

    private void FinalExplosion()
    {
        float size = (int)((1f / shotInterval) + 1f);
        float theta = 0;

        for (int i = 0; i < size; i++)
        {
            theta += (2.0f * Mathf.PI * shotInterval);
            float x = 20 * Mathf.Cos(theta);
            float z = 20 * Mathf.Sin(theta);

            Vector3 pos = new Vector3(transform.position.x + x, transform.position.y, transform.position.z + z);
            GameObject chargeBullet = boss.SpawnGameObject(bulletPrefab, transform.position, Quaternion.identity);
            chargeBullet.GetComponent<ChargeBullet>().ShootAt(pos);
        }
    }
}