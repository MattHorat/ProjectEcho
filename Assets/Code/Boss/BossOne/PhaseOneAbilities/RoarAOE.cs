﻿using UnityEngine;
using System.Collections;
using System;
using System.Timers;
using UnityEngine.Networking;

public class RoarAOE : NetworkBehaviour {
    private static float explosionTime = 1;
    private static float damageAmount = 600;

    public Character boss;
    private float explosionTimer;

    public void Start()
    {
        explosionTimer = Time.time;
    }

    public void Update()
    {
        if (isServer)
        {
            if (Time.time - explosionTimer > explosionTime)
            {
                GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
                foreach (GameObject player in players)
                {
                    if (Vector3.Distance(transform.position, player.transform.position) < 1)
                    {
                        boss.CauseDamage(player.GetComponent<Character>(), damageAmount + damageAmount / 100 * 5 * ((ReolusStageOne)boss).rageStacks);
                    }
                }
                Destroy(gameObject);
            }
        }
    }
}
