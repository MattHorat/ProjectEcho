﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class FrontalSwing : BossAbility
{
    private static float damageAmount = 800;
    private static float damageAngle = 90;
    private static float range = 4;
    private static float channelTime = 2;
    private static float pushDistance = 8;
    private static float threatLowerAmount = 400;

    private float chargeTimer = -9999999999;

    public override void TriggerAbility()
    {
        chargeTimer = Time.time;
    }

    public override bool UpdateAbility()
    {
        if (Time.time - chargeTimer > channelTime)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                if (Vector3.Distance(boss.transform.position, player.transform.position) < range)
                {
                    float cone = Mathf.Cos(damageAngle * Mathf.Deg2Rad);
                    Vector3 heading = (player.transform.position - boss.transform.position).normalized;
                    if (Vector3.Dot(boss.transform.forward, heading) > cone)
                    {
                        PlayerCharacter playerChar = player.GetComponent<PlayerCharacter>();
                        boss.CauseDamage(playerChar, damageAmount + damageAmount / 100 * 5 * ((ReolusStageOne)boss).rageStacks);
                        Vector3 relativePos = player.transform.position - boss.transform.position;
                        relativePos = relativePos.normalized;
                        Vector3 targetPos = relativePos * pushDistance;
                        targetPos = targetPos + boss.transform.position;
                        if (playerChar == null)
                        {
                            Debug.Log("oh noes2");
                            Debug.Log(player);
                        }
                        playerChar.ForceMovementTo(targetPos, .5F);
                        boss.GetComponent<Boss>().LowerThreat(threatLowerAmount, playerChar.characterName);
                    }
                }
            }
            return true;
        }
        return false;
    }
}