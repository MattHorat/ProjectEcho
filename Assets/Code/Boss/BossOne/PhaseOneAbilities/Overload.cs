﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Overload : BossAbility
{
    private static float channelTime = 2;
    private static float damageAmount = 1000;
    
    private float chargeTimer;
    private int firstTower;
    private int secondTower;
    private bool isFirst = true;

    private List<GameObject> altars = new List<GameObject>();

    public Overload SetAltars(List<GameObject> altars)
    {
        abilityCooldown = 0;
        this.altars = altars;
        return this;
    }

    public override void TriggerAbility()
    {
        SetupAltars();
        isFirst = true;
    }

    public override bool UpdateAbility()
    {
        if (Time.time > chargeTimer)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                float smallestDistance = 1000;
                GameObject closest = null;
                foreach (GameObject altar in altars)
                {
                    if (Vector3.Distance(player.transform.position, altar.transform.position) < smallestDistance)
                    {
                        closest = altar;
                    }
                }
                if (altars.IndexOf(closest) == firstTower || altars.IndexOf(closest) == secondTower)
                {
                    boss.CauseDamage(player.GetComponent<PlayerCharacter>(), damageAmount);
                }
            }
            if (!isFirst)
            {
                return true;
            }
            isFirst = false;
            SetupAltars();
        }
        return false;
    }

    private void SetupAltars()
    {
        chargeTimer = Time.time + channelTime;
        firstTower = UnityEngine.Random.Range(0, altars.Count);
        secondTower = UnityEngine.Random.Range(0, altars.Count - 1);
        if (firstTower == secondTower)
        {
            secondTower = altars.Count;
        }
        Debug.Log("First tower: " + firstTower);
        Debug.Log("Second tower: " + secondTower);
    }
}