﻿using System;
using UnityEngine;
using UnityEngine.Networking;

class Corruption : StatusEffect
{
    private static float damageAmount = 100;
    private static float cooldownTime = 1;
    private static int pulseCount = 5;
    private float corruptionAdditionDamageAmount = 50;
    private static float corruptionDecrease = 3;

    private float pulseTimer = -9999;
    private Character boss;
    public bool isCorrupted;
    private int pulseCounter = 0;

    public Corruption SetBoss(Character boss)
    {
        this.boss = boss;
        return this;
    }

    public override string GetTooltip()
    {
        return "Hi!";
    }

    public override void Tick()
    {
        if (isLocalPlayer)
        {
            if (isCorrupted)
            {
                if (pulseTimer < Time.time)
                {
                    displayNumber = cooldownTime * (pulseCount - pulseCounter);
                    pulseCounter++;

                    GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
                    foreach (GameObject player in players)
                    {
                        if (player.tag != character.tag && Vector3.Distance(character.transform.position, player.transform.position) < 1)
                        {
                            boss.CauseDamage(player.GetComponent<Character>(), damageAmount + damageAmount / 100 * 5 * ((ReolusStageOne)boss).rageStacks);
                        }
                    }

                    pulseTimer = Time.time + cooldownTime;

                    if (pulseCounter > pulseCount)
                    {
                        HandleCorrupted();
                    }
                }
            }
            else
            {
                displayNumber -= corruptionDecrease * Time.deltaTime;
                if (displayNumber <= 0)
                {
                    character.RemoveStatusEffect(title);
                }
            }
        }
    }

    public override void AddNumber(float number)
    {
        if (isCorrupted)
        {
            character.TakeDamage(corruptionAdditionDamageAmount * Time.deltaTime, boss);
        }
        else
        {
            displayNumber += number;
            if (displayNumber >= 100)
            {
                isCorrupted = true;
                displayNumber = (float)Math.Ceiling(pulseTimer - Time.time);
            }
        }
    }

    private void HandleCorrupted()
    {
        character.GetComponent<Character>().CmdSpawnGameObject("Prefabs/Reolus/Corruption", character.transform.position, Quaternion.identity);
        character.RemoveStatusEffect(title);
    }

    public Corruption SetDamage(float damageAmount)
    {
        corruptionAdditionDamageAmount = damageAmount;
        return this;
    }
}