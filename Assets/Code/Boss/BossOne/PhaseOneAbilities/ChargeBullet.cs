﻿using UnityEngine;
using System.Collections;
using System;

public class ChargeBullet : AICharacter {
    private float arrowSpeed = 10;
    private float damageAmount = 100;

    public override void Update()
    {
        if (isServer)
        {
            base.Update();
            if (!isMoving)
            {
                Destroy(gameObject);
            }
        }
    }

    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            speed = arrowSpeed;
            isMoving = true;
        }
    }

    public void ShootAt(Vector3 target)
    {
        MoveTo(target);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (isServer)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                Character player = collision.gameObject.GetComponent<Character>();
                CauseDamage(player, damageAmount);
            }
        }
    }
}
