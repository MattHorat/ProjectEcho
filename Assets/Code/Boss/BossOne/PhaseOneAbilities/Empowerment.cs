﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class Empowerment : BossAbility
{
    private static float chargeTime = 10;
    private static float debuffIncrease = 13F;

    private GameObject altarPrefab;
    private float chargeTimer = -9999999999;
    private int triggerCount = 0;
    private List<GameObject> altars = new List<GameObject>();
    private static string title = "Empowerment";

    public Empowerment SetAltars(List<GameObject> altars)
    {
        abilityCooldown = 0;
        this.altars = altars;
        return this;
    }

    public override void TriggerAbility()
    {
        chargeTimer = Time.time;
        boss.transform.position = new Vector3(0, 0, 0);
        if (triggerCount < 4)
        {
            triggerCount++;
        }
    }

    public override bool UpdateAbility()
    {
        if (Time.time - chargeTimer < chargeTime)
        {
            for (int index = 0; index < triggerCount; index++)
            {
                GameObject altar = altars[index];
                RaycastHit hitObject;
                int layerMask = ~(1 << 8);
                if (Physics.Linecast(altar.transform.position, boss.transform.position, out hitObject, layerMask))
                {
                    if (hitObject.transform.gameObject != null)
                    {
                        if (hitObject.transform.gameObject.CompareTag("Player"))
                        {
                            GameObject player = hitObject.transform.gameObject;
                            RpcRegisterStatus(player, boss.gameObject);
                            Debug.DrawLine(altar.transform.position, player.transform.position);
                        }
                        else
                        {
                            ((ReolusStageOne)boss).currentEmpowerment += 100 / chargeTime * Time.deltaTime;
                            Debug.DrawLine(altar.transform.position, boss.transform.position);
                        }
                    }
                }
            }
            return false;
        }

        return true;
    }

    [ClientRpc]
    public void RpcRegisterStatus(GameObject playerGO, GameObject bossGO)
    {
        Character player = playerGO.GetComponent<Character>();
        player.RegisterStatusEffect(title, playerGO.AddComponent<Corruption>().SetBoss(bossGO.GetComponent<Character>()), debuffIncrease * Time.deltaTime);
    }

    public void StopAbility()
    {
        chargeTimer = -999999999;
    }
}