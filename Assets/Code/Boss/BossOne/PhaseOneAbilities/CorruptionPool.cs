﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;

public class CorruptionPool : AICharacter {
    private static float damageAmountPerSecond = 150;
    private static float growthTime = 5;
    private static float growthRate = 0.7F;
 
    private List<GameObject> players = new List<GameObject>();
    private float growthTimer;

    [SyncVar]
    public Vector3 scale;
    [SyncVar]
    public Quaternion rotation;

    public override void Update()
    {
        if (isServer)
        {
            if (Time.time < growthTimer)
            {
                transform.localScale = new Vector3(transform.localScale.x + (growthRate * Time.deltaTime), transform.localScale.y, transform.localScale.z + (growthRate * Time.deltaTime));
                scale = transform.localScale;
                rotation = transform.rotation;
            }
        }
        else
        {
            transform.localScale = scale;
            transform.rotation = rotation;
        }
    }

    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            growthTimer = Time.time + growthTime;
        }
        transform.localScale = scale;
        transform.rotation = rotation;
    }

    public override void TakeDamage(float damageAmount, Character damageDealer, string damageName = null)
    {
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            players.Add(collider.gameObject);
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            players.Remove(collider.gameObject);
        }
    }

    void OnTriggerStay(Collider collision)
    {
        if (isServer)
        {
            foreach (GameObject player in players)
            {
                CauseDamage(player.GetComponent<Character>(), damageAmountPerSecond * Time.deltaTime);
            }
        }
    }
}
