﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReolusStageOne : Boss
{
    public const string REOLUS_HEALTH = "ReolusHealth";

    public static float bossSpeed = 4;
    public static float bossHealth = 1000000;
    private static float empowermentCooldown = 40;
    private static float maxEmpowermentStacks = 4;

    private float autoAttackTimer;
    private enum BossState { Moving, AbilityOne, AbilityTwo, AbilityThree, AbilityFour, AbilityFive }
    private BossState currentState = BossState.Moving;
    private float abilityTimer;
    private int abilityCooldown = 3;
    private float empowermentCooldownTimer;
    private Dictionary<BossState, BossAbility> bossAbilities = new Dictionary<BossState, BossAbility>();
    List<GameObject> altars = new List<GameObject>();
    private BossAbility abilityThree;
    private BossAbility abilityFour;
    public float currentEmpowerment = 0;
    public int empowermentStacks = 0;
    public int rageStacks = 0;
    private int previousEmpowermentStacks = 0;
    private int abilityWeighting = 2;
    
    public void Start()
    {
        if (isServer)
        {
            gameObject.AddComponent<NavMeshAgent>();
            SetSpeed(bossSpeed);
            maxHealth = bossHealth;
            health = maxHealth;
            autoAttackTimer = Time.time;
            targetedPlayer = FindClosestPlayer();
            bossAbilities.Add(BossState.AbilityOne, gameObject.GetComponent<FrontalSwing>().SetBoss(this));
            bossAbilities.Add(BossState.AbilityTwo, gameObject.GetComponent<Charge>().SetBoss(this));
            bossAbilities.Add(BossState.AbilityFive, gameObject.GetComponent<Overload>().SetAltars(altars).SetBoss(this));
            abilityThree = gameObject.GetComponent<Roar>().SetBoss(this);
            altars.Add(GameObject.Find("EmpowermentAltarW"));
            altars.Add(GameObject.Find("EmpowermentAltarN"));
            altars.Add(GameObject.Find("EmpowermentAltarE"));
            altars.Add(GameObject.Find("EmpowermentAltarS"));
            abilityFour = gameObject.GetComponent<Empowerment>().SetAltars(altars).SetBoss(this);
            empowermentCooldownTimer = Time.time + empowermentCooldown;
            stopDistance = 1;
            NavMeshAgent navMeshAgent = GetComponent<NavMeshAgent>();
            navMeshAgent.acceleration = 1000;
            navMeshAgent.updateRotation = false;
            navMeshAgent.autoBraking = false;
            navMeshAgent.stoppingDistance = 0.5F;
        }
    }

    public override void Update()
    {
        if (isServer)
        {
            switch (currentState)
            {
                case BossState.Moving:
                    if (Time.time > empowermentCooldownTimer)
                    {
                        currentState = BossState.AbilityFour;
                        StopMovement();
                        previousEmpowermentStacks = empowermentStacks;
                        abilityFour.TriggerAbility();
                        break;
                    }
                    else
                    {
                        if (targetedPlayer == null)
                        {
                            targetedPlayer = FindClosestPlayer();
                        }
                        MoveTo(targetedPlayer.transform.position);
                        AutoAttack();
                        if (Time.time - abilityTimer > abilityCooldown)
                        {
                            currentState = GetRandomAbility();
                            StopMovement();
                            abilityTimer = Time.time;
                            bossAbilities[currentState].TriggerAbility();
                            abilityCooldown = Random.Range(8, 15);
                        }
                        break;
                    }
                case BossState.AbilityOne:
                case BossState.AbilityTwo:
                case BossState.AbilityFive:
                    if (bossAbilities[currentState].UpdateAbility())
                    {
                        currentState = BossState.Moving;
                        autoAttackTimer = Time.time;
                    }
                    break;
                case BossState.AbilityThree:
                    if (abilityThree.UpdateAbility())
                    {
                        currentState = BossState.Moving;
                        autoAttackTimer = Time.time;
                    }
                    break;
                case BossState.AbilityFour:
                    if (abilityFour.UpdateAbility())
                    {
                        if (previousEmpowermentStacks == empowermentStacks)
                        {
                            rageStacks++;
                            abilityThree.TriggerAbility();
                            currentState = BossState.AbilityThree;
                        }
                        else
                        {
                            if (empowermentStacks == maxEmpowermentStacks)
                            {
                                SkipPhase();
                            }
                            else
                            {
                                currentState = BossState.Moving;
                            }
                        }
                        empowermentCooldownTimer = Time.time + empowermentCooldown;
                        autoAttackTimer = Time.time;
                    }
                    break;
            }
            FindMostDamage();
            CalculateEmpowerment();
            base.Update();
        }
    }

    private void AutoAttack()
    {
        if (Time.time - autoAttackTimer > 1.5)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                if (Vector3.Distance(transform.position, player.transform.position) < 2)
                {
                    float cone = Mathf.Cos(45 * Mathf.Deg2Rad);
                    Vector3 heading = (player.transform.position - transform.position).normalized;

                    if (Vector3.Dot(transform.forward, heading) > cone)
                    {
                        CauseDamage(player.GetComponent<Character>(), 300);
                    }
                }
            }
            autoAttackTimer = Time.time;
        }
    }

    private BossState GetRandomAbility()
    {
        int weightedRandom = Random.Range(0, abilityWeighting + 1);
        BossState returnState = BossState.AbilityOne;
        bool abilityFound = false;
        foreach (KeyValuePair<BossState, BossAbility> ability in bossAbilities)
        {
            if (ability.Value.abilityCooldown > weightedRandom && !abilityFound)
            {
                abilityWeighting -= (ability.Value.abilityCooldown - 1);
                ability.Value.abilityCooldown = 1;
                returnState = ability.Key;
                abilityFound = true;
            }
            else
            {
                ability.Value.abilityCooldown++;
                weightedRandom -= ability.Value.abilityCooldown;
                abilityWeighting++;
            }
        }
        return returnState;
    }

    private void CalculateEmpowerment()
    {
        if (currentEmpowerment >= 99.8)
        {
            empowermentStacks += 1;
            currentEmpowerment = 0;
            ((Empowerment)abilityFour).StopAbility();
        }
    }

    public override void TakeDamage(float damageAmount, Character damageDealer, string moveName = null)
    {
        damageAmount = damageAmount + (damageAmount * (rageStacks * 0.05F));
        base.TakeDamage(damageAmount, damageDealer, moveName);
    }

    public override void SkipPhase()
    {
        if (isServer)
        {
            foreach (GameObject altar in altars)
            {
                Destroy(altar);
            }
            GetComponent<NavMeshAgent>().enabled = false;
            gameObject.AddComponent<ReolusStageTwo>();
            GameStateController gameState = GameObject.Find("GameStateController(Clone)").GetComponent<GameStateController>();
            gameState.AddProperty(REOLUS_HEALTH, health.ToString());
            transform.position = new Vector3(0, 0, 0);
            Destroy(this);
        }
    }
}