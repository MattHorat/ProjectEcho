﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class SwordThrow : AICharacter
{
    private static float minimumDistance = 7;
    private static float waitTime = 1;
    private static float damageRange = 2;
    private static float damageAmount = 800;
    private static float impactDamageAmount = 400;
    
    private float waitTimer = -99999999;
    private GameObject targetPlayer;
    private bool isCharging = false;
    private bool isReturning = false;
    private bool isWaiting = false;

    private Character boss;

    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            speed = 5;
        }
    }

    public void TriggerAbility()
    {
        List<GameObject> players = GameObject.FindGameObjectsWithTag("Player").ToList();
        List<GameObject> filteredPlayers = new List<GameObject>();
        GameObject farthestPlayer = null;
        float farthestDistance = 0;

        foreach (GameObject player in players)
        {
            float distanceBetween = Vector3.Distance(player.transform.position, boss.transform.position);
            if (distanceBetween < minimumDistance && ((ReolusStageThree)boss).targetedPlayer != player) {
                filteredPlayers.Add(player);
            }
            if (distanceBetween > farthestDistance)
            {
                farthestPlayer = player;
            }
        }
        if (filteredPlayers.Count() != 0)
        {
            targetPlayer = filteredPlayers.ElementAt(UnityEngine.Random.Range(0, filteredPlayers.Count()));
        }
        else
        {
            targetPlayer = farthestPlayer;
        }
        MoveTo(targetPlayer.transform.position);
        isCharging = true;
    }

    public override void Update()
    {
        if (isReturning && !isMoving)
        {
            Destroy(gameObject);
        }
        else if (isCharging && !isMoving)
        {
            isWaiting = true;
            waitTimer = Time.time + waitTime;
            isCharging = false;
            PulseDamage();
        }
        else if (isWaiting && Time.time > waitTimer)
        {
            isReturning = true;
            isWaiting = false;
            MoveTo(boss.transform.position);
        }
        base.Update();
    }

    public void SetBoss(Character boss)
    {
        this.boss = boss;
    }

    private void PulseDamage()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            if (Vector3.Distance(boss.transform.position, player.transform.position) < damageRange)
            {
                boss.CauseDamage(player.GetComponent<Character>(), impactDamageAmount);
            }
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (isServer)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                Character player = collision.gameObject.GetComponent<Character>();
                CauseDamage(player, damageAmount);
            }
        }
    }
}