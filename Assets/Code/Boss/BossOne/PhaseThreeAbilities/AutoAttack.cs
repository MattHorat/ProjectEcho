﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class AutoAttack : BossAbility
{
    private static float minimumDistance = 7;
    private static float damageRange = 20;
    private static float damageAmount = 350;
    private static float pulseDamageAmount = 500;

    private bool isPulsing = false;
    private bool lastAttackMissed = false;

    public override void TriggerAbility()
    {
        GameObject bossTarget = ((ReolusStageThree)boss).targetedPlayer;
        if (bossTarget != null && Vector3.Distance(bossTarget.transform.position, boss.transform.position) < minimumDistance)
        {
            boss.CauseDamage(bossTarget.GetComponent<Character>(), damageAmount);
            isPulsing = false;
        }
        else
        {
            List<GameObject> players = GameObject.FindGameObjectsWithTag("Player").ToList();
            List<GameObject> closePlayers = new List<GameObject>();

            foreach (GameObject player in players)
            {
                float distanceBetween = Vector3.Distance(player.transform.position, boss.transform.position);
                if (distanceBetween < minimumDistance) {
                    closePlayers.Add(player);
                }
            }
            if (closePlayers.Count() != 0)
            {
                boss.CauseDamage(closePlayers.ElementAt(UnityEngine.Random.Range(0, closePlayers.Count())).GetComponent<Character>(), damageAmount);
                isPulsing = false;
                lastAttackMissed = false;
            }
            else 
            {
                if (lastAttackMissed)
                {
                    isPulsing = true;
                }
                else
                {
                    lastAttackMissed = true;
                }
            }
        }
    }

    public override bool UpdateAbility()
    {
        if (isPulsing)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                if (Vector3.Distance(boss.transform.position, player.transform.position) < damageRange)
                {
                    boss.CauseDamage(player.GetComponent<Character>(), pulseDamageAmount * Time.deltaTime);
                }
            }
        }
        return true;
    }
}