﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class ChaosWave : BossAbility
{
    private static float swapTime = 10;
    private static float disappearTime = 2;
    private static float safetyTime = 1;
    private static float damageAmount = 1000;

    private Vector3 targetedPosition;
    private int numberOfBeams = 1;
    private float swapTimer;
    private float fadeoutTimer;
    private float fadeinTimer;
    private float directionOffset = 1;
    private bool isFaded = false;

    private bool isActive = false;

    public override void TriggerAbility()
    {
        swapTimer = Time.time;
        fadeoutTimer = swapTimer + disappearTime;
        isActive = true;
    }

    public override bool UpdateAbility()
    {
        if (!isActive)
        {
            return false;
        }
        if (Time.time >= swapTimer)
        {
            isFaded = true;
            swapTimer = Time.time + swapTime + disappearTime;
        }
        if (Time.time >= fadeoutTimer)
        {
            isFaded = false;
            numberOfBeams++;
            directionOffset *= -1;
            fadeoutTimer = swapTimer + disappearTime;
            fadeinTimer = Time.time + safetyTime;
        }
        if (!isFaded)
        {
            for (int i = 0; i < numberOfBeams; i++)
            {
                float time = Time.time;
                if (Time.time < fadeinTimer)
                {
                    time = fadeinTimer;
                }
                float offset = (2 * (float)Math.PI) / numberOfBeams * i;
                //x = cx + Math.sin(time)*rad;
                //y = cy + Math.cos(time)*rad;
                float xPos = boss.transform.position.x + ((float)Math.Sin(time * directionOffset / 2 + offset) * 20F);
                float zPos = boss.transform.position.z + ((float)Math.Cos(time * directionOffset / 2 + offset) * 20F);
                targetedPosition = new Vector3(xPos, 0, zPos);
                RaycastHit hitObject;
                int layerMask = ~(1 << 8);
                if (Physics.Linecast(boss.transform.position, targetedPosition, out hitObject, layerMask))
                {
                    if (hitObject.transform.gameObject != null)
                    {
                        if (hitObject.transform.gameObject.CompareTag("Player"))
                        {
                            GameObject player = hitObject.transform.gameObject;
                            Debug.DrawLine(boss.transform.position, player.transform.position);

                            if (Time.time > fadeinTimer)
                            {
                                boss.CauseDamage(player.GetComponent<Character>(), damageAmount * Time.deltaTime);
                            }
                        }
                        else
                        {
                            Debug.DrawLine(targetedPosition, boss.transform.position);
                        }
                    }
                }
                else
                {
                    Debug.DrawLine(targetedPosition, boss.transform.position);
                }
            }
        }
        return false;
    }

    public void HaltWaves()
    {
        isActive = false;
    }

    public void StartWaves()
    {
        isActive = true;
        TriggerAbility();
    }
}