﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class PureCorruption : BossAbility
{
    private static float chargeTime = 10;
    private static float debuffIncrease = 40;
    private static float corruptionDamage = 100;

    private float chargeTimer = -9999999999;
    private static string title = "Empowerment";
    private GameObject targetedPlayer;

    private bool isActive = false;

    public override void TriggerAbility()
    {
        chargeTimer = Time.time;
        List<GameObject> players = GameObject.FindGameObjectsWithTag("Player").ToList();
        targetedPlayer = players[UnityEngine.Random.Range(0, players.Count())];
        isActive = true;
    }

    public override bool UpdateAbility()
    {
        if (isActive && Time.time - chargeTimer < chargeTime)
        {
            RaycastHit hitObject;
            int layerMask = ~(1 << 8);
            if (Physics.Linecast(boss.transform.position, targetedPlayer.transform.position, out hitObject, layerMask))
            {
                if (hitObject.transform.gameObject != null)
                {
                    if (hitObject.transform.gameObject.CompareTag("Player"))
                    {
                        GameObject player = hitObject.transform.gameObject;
                        RpcRegisterStatus(player, boss.gameObject);
                        Debug.DrawLine(boss.transform.position, player.transform.position);
                    }
                    else
                    {
                        Debug.DrawLine(targetedPlayer.transform.position, boss.transform.position);
                    }
                }
            }
            return false;
        }

        isActive = false;
        return true;
    }

    [ClientRpc]
    public void RpcRegisterStatus(GameObject playerGO, GameObject bossGO)
    {
        Character player = playerGO.GetComponent<Character>();
        player.RegisterStatusEffect(title, playerGO.AddComponent<Corruption>().SetBoss(bossGO.GetComponent<Character>()).SetDamage(corruptionDamage), debuffIncrease * Time.deltaTime);
    }

    public void StopAbility()
    {
        isActive = false;
    }
}