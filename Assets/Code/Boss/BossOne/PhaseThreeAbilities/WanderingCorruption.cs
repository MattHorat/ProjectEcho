﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class WanderingCorruption : AICharacter
{
    private static float damageAmount = 150;
    private static float minWanderTime = 2;
    private static float maxWanderTime = 7;
    private static float slowAmount = .5F;
    private static float slowTimer = 2;
    
    private float changePosTimer = -99999999;
    private Vector3 movePosition;
    private List<GameObject> players = new List<GameObject>();

    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            speed = 3;
        }
    }

    public void TriggerAbility()
    {
        movePosition = new Vector3(UnityEngine.Random.Range(-10, 10), 0, UnityEngine.Random.Range(-10, 10));
        MoveTo(movePosition);
        changePosTimer = Time.time + UnityEngine.Random.Range(minWanderTime, maxWanderTime);
    }

    public override void Update()
    {
        if (Time.time > changePosTimer || !isMoving)
        {
            movePosition = new Vector3(UnityEngine.Random.Range(-10, 10), 0, UnityEngine.Random.Range(-10, 10));
            MoveTo(movePosition);
            changePosTimer = Time.time + UnityEngine.Random.Range(minWanderTime, maxWanderTime);
        }
        base.Update();
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            players.Add(collider.gameObject);
            collider.gameObject.GetComponent<PlayerCharacter>().RpcSlow(slowAmount, slowTimer);
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            players.Remove(collider.gameObject);
        }
    }

    void OnTriggerStay(Collider collision)
    {
        if (isServer)
        {
            foreach (GameObject player in players)
            {
                PlayerCharacter playerChar = player.GetComponent<PlayerCharacter>();
                CauseDamage(playerChar, damageAmount * Time.deltaTime);
            }
        }
    }
}