﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class Explosion : BossAbility
{
    private static float chargeTime = 5;
    private static float damageAmount = 5000;

    private float chargeTimer;
    private bool isActive = false;

    public override void TriggerAbility()
    {
        isActive = true;
        chargeTimer = Time.time + chargeTime;
        List<GameObject> players = GameObject.FindGameObjectsWithTag("Player").ToList();
        foreach (GameObject player in players)
        {
            player.GetComponent<PlayerCharacter>().RpcPullTowards(boss.transform.position, chargeTime);
        }
    }

    public override bool UpdateAbility()
    {
        if (isActive && Time.time > chargeTimer)
        {
            List<GameObject> players = GameObject.FindGameObjectsWithTag("Player").ToList();
            foreach (GameObject player in players)
            {
                player.GetComponent<PlayerCharacter>().TakeDamage(damageAmount * (1 / Vector3.Distance(boss.transform.position, player.transform.position)), boss);
            }
            isActive = false;
            return true;
        }
        return false;

    }
}