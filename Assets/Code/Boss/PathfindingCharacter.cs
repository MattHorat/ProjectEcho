﻿using UnityEngine;
using System.Collections;

public abstract class PathfindingCharacter : Character
{
    private Vector3 movePoint;
    public bool isMoving = false;
    public float stopDistance;

    public override void Update()
    {
        if (isServer)
        {
            if (isMoving && Vector3.Distance(gameObject.transform.position, movePoint) > stopDistance)
            {
                GetComponent<NavMeshAgent>().destination = movePoint;
            }
            else if (isMoving)
            {
                isMoving = false;
                GetComponent<NavMeshAgent>().Stop();
            }
            transform.LookAt(movePoint);
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, 0, gameObject.transform.position.z);
        }
    }

    public void MoveTo(Vector3 position)
    {
        isMoving = true;
        movePoint = position;
        GetComponent<NavMeshAgent>().Resume();
    }

    public void StopMovement()
    {
        isMoving = false;
        GetComponent<NavMeshAgent>().Stop();
    }

    public void ReverseMovement()
    {
        Vector3 targetPos = -(movePoint - transform.position);
        MoveTo(targetPos + transform.position);
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
        GetComponent<NavMeshAgent>().speed = speed;
    }
}
