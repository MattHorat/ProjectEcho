﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Networking;

public abstract class BossAbility : NetworkBehaviour
{
    public abstract void TriggerAbility();
    public abstract bool UpdateAbility();
    public PathfindingCharacter boss;
    public int abilityCooldown = 1;

    public BossAbility SetBoss(PathfindingCharacter boss)
    {
        this.boss = boss;
        return this;
    }
}