﻿using UnityEngine;
using System.Collections;

public abstract class AICharacter : Character
{
    private Vector3 movePoint;
    public bool isMoving = false;
    public float stopDistance;

    public virtual void Start()
    {
        stopDistance = 0.3F;
    }

    public override void Update()
    {
        if (isServer)
        {
            Rigidbody rb = gameObject.GetComponent<Rigidbody>();
            if (isMoving && Vector3.Distance(gameObject.transform.position, movePoint) > stopDistance)
            {
                //gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, movePoint, speed * Time.deltaTime / (Vector3.Distance(gameObject.transform.position, movePoint)));
                Vector3 moveDirection = (movePoint - gameObject.transform.position).normalized * speed * Time.deltaTime;
                Vector3 movementPos = new Vector3(gameObject.transform.position.x + moveDirection.x, -0.5F, gameObject.transform.position.z + moveDirection.z);
                rb.MovePosition(movementPos);
            }
            else if (isMoving)
            {
                isMoving = false;
            }
            transform.LookAt(movePoint);
            rb.velocity = Vector3.zero;
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, -0.5F, gameObject.transform.position.z);
        }
    }

    public void MoveTo(Vector3 position)
    {
        isMoving = true;
        movePoint = position;
    }

    public void StopMovement()
    {
        isMoving = false;
    }

    public void ReverseMovement()
    {
        Vector3 targetPos = -(movePoint - transform.position);
        MoveTo(targetPos + transform.position);
    }
}
