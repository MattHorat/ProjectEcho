﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class Respawn : Ability {
    private static float channelTime = 2F;

    private GameObject respawnTarget;
    public GameStateController gameState;
    
    public void Start()
    {
        channelLength = channelTime;
    }
    
    public override string GetName()
    {
        return "";
    }

	protected override void AbilityTrigger(Vector3 position)
    {
        if (gameState.GetLives() > 0)
        {
            channelTimer = Time.time + channelLength;
            isChanneling = true;
        }
    }

    public override void AbilityUpdate()
    {
        if (isChanneling && Time.time > channelTimer)
        {
            character.GetComponent<PlayerCharacter>().CmdInteract(respawnTarget);
            isChanneling = false;
        }
    }

    public void SetTarget(GameObject target)
    {
        respawnTarget = target;
    }

    protected override void AbilityAim(Vector3 position)
    {
    }

    public override void Interrupt()
    {
        isChanneling = false;
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed && !character.IsMoving();
    }

    public override string GetTooltip()
    {
        return "";
    }
}
