﻿using UnityEngine;
using System.Collections;
using System.Timers;
using System.Collections.Generic;
using UnityEngine.Networking;

public abstract class Character : NetworkBehaviour {
    public enum Direction { Up, Down, Left, Right };
    [SyncVar]
    public float health;
    public float speed;
    public float maxHealth;
    public delegate IEnumerator CoroutineFunction();
    public string characterName;
    private Dictionary<string, float[]> DoTList = new Dictionary<string, float[]>();
    public float damageMultiplier = 1;
    public float healMultiplier = 1;
    private Dictionary<string, StatusEffect> statusEffects = new Dictionary<string, StatusEffect>();
    private List<string> statusEffectsToDelete = new List<string>();

    virtual public void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        foreach (StatusEffect statusEffect in statusEffects.Values)
        {
            statusEffect.Tick();
        }
        foreach (string statusEffectName in statusEffectsToDelete)
        {
            statusEffects.Remove(statusEffectName);
        }
        statusEffectsToDelete = new List<string>();
    }

    public virtual void CauseDamage(Character target, float damageAmount, string moveName = null)
    {
        if (isServer)
        {
            if (target != null)
            {
                target.TakeDamage(damageAmount, this, moveName);
            }
        }
        else
        {
            CmdCauseDamage(target.gameObject, damageAmount, moveName);
        }
    }

    [Command]
    public void CmdCauseDamage(GameObject target, float damageAmount, string moveName)
    {
        if (target != null)
        {
            target.GetComponent<Character>().TakeDamage(damageAmount, this, moveName);
        }
    }

    public virtual void TakeDamage(float damageAmount, Character damageDealer, string moveName = null)
    {
        if (!localPlayerAuthority || isLocalPlayer)
        {
            if (damageDealer != null)
            {
                LocalTakeDamage(damageAmount, damageDealer.gameObject, moveName);
            }
        }
        else if (isServer)
        {
            // We have to check here, because the damage dealer may have died
            if (damageDealer != null)
            {
                RpcTakeDamage(damageAmount, damageDealer.gameObject, moveName);
            }
        }
    }

    [ClientRpc]
    public virtual void RpcHealDamage(float healAmount)
    {
        if (isLocalPlayer)
        {
            LocalTakeHealing(healAmount);
        }
    }

    public virtual void LocalTakeHealing(float healAmount)
    {
        healAmount *= healMultiplier;
        health += healAmount;
        if (health > maxHealth)
        {
            health = maxHealth;
        }
    }

    public virtual void HealDamage(float healAmount)
    {
        if (!localPlayerAuthority || isLocalPlayer)
        {
            LocalTakeHealing(healAmount);
        }
        else
        {
            RpcHealDamage(healAmount);
        }
    }

    public void LowerArmour(float amount, float time)
    {
        RpcLowerArmour(amount, time);
    }

    public void AlterHealing(float amount, float time = 0)
    {
        RpcChangeHealing(amount, time);
    }

    [ClientRpc]
    public void RpcTakeDamage(float damageAmount, GameObject damageDealer, string moveName)
    {
        if (isLocalPlayer)
        {
            LocalTakeDamage(damageAmount, damageDealer, moveName);
        }
    }

    [Command]
    public void CmdTakeDamage(float damageAmount, GameObject damageDealer, string moveName)
    {
        if (isLocalPlayer)
        {
            LocalTakeDamage(damageAmount, damageDealer, moveName);
        }
        else
        {
            RpcTakeDamage(damageAmount, damageDealer, moveName);
        }
    }

    protected virtual void LocalTakeDamage(float damageAmount, GameObject damageDealer, string moveName = null)
    {
        damageAmount *= damageMultiplier;
        health -= damageAmount;
        if (moveName != null)
        {
            GameObject.FindObjectOfType<DamageLog>().AddDamageAmount(moveName, damageAmount);
        }
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }

    [ClientRpc]
    public void RpcLowerArmour(float amount, float time)
    {
        damageMultiplier += amount;
        StartCoroutine(IncreaseArmour(amount, time));
    }

    private IEnumerator IncreaseArmour(float amount, float time)
    {
        yield return new WaitForSeconds(time);
        damageMultiplier -= amount;
    }

    [ClientRpc]
    public void RpcChangeHealing(float amount, float time)
    {
        healMultiplier += amount;
        if (time != 0)
        {
            StartCoroutine(ChangeHealing(amount, time));
        }
    }

    private IEnumerator ChangeHealing(float amount, float time)
    {
        yield return new WaitForSeconds(time);
        healMultiplier -= amount;
    }

    [Command]
    public virtual void CmdCauseDamageOverTime(GameObject target, float damageAmount, float time, string abilityName)
    {
        if (target != null)
        {
            if (target.GetComponent<Character>() == null)
            {
                if (target.GetComponent<PlayerCharacter>() == null)
                {
                    Debug.Log("oh noes");
                    Debug.Log(target);
                }
                target.GetComponent<PlayerCharacter>().TakeDamageOverTime(damageAmount, time, abilityName, this);
                //return;
            }
            else
            {
                target.GetComponent<Character>().TakeDamageOverTime(damageAmount, time, abilityName, this);
            }
        }
    }

    public void TakeDamageOverTime(float damageAmount, float time, string abilityName, Character damageDealer)
    {
        if (DoTList.ContainsKey(abilityName))
        {
            DoTList[abilityName][0] = time;
            DoTList[abilityName][1] = damageAmount;
        }
        else
        {
            float tickDamage = damageAmount / time / 2;
            TakeDamage(tickDamage, damageDealer, abilityName);
            DoTList.Add(abilityName, new float[] { time - .5F, damageAmount - tickDamage });
            StartCoroutine(TakeDoT(abilityName, damageDealer));
        }
    }

    private IEnumerator TakeDoT(string abilityName, Character damageDealer)
    {
        if (DoTList[abilityName][0] > 0.5)
        {
            float tickDamage = DoTList[abilityName][1] / DoTList[abilityName][0] / 2;
            TakeDamage(tickDamage, damageDealer, abilityName);
            DoTList[abilityName][0] = DoTList[abilityName][0] - .5F;
            DoTList[abilityName][1] = DoTList[abilityName][1] - tickDamage;
            yield return new WaitForSeconds(.5F);
            StartCoroutine(TakeDoT(abilityName, damageDealer));
        }
        else
        {
            TakeDamage(DoTList[abilityName][1], damageDealer, abilityName);
            DoTList.Remove(abilityName);
        }
    }

    public void HealOverTime(float healAmount, float time)
    {
        float tickHeal = healAmount / time / 2;
        HealDamage(tickHeal);
        StartCoroutine(TakeHoT(healAmount - tickHeal, time - .5F));
    }

    private IEnumerator TakeHoT(float healAmount, float time)
    {
        if (time > 0.5)
        {
            float tickHeal = healAmount / time / 2;
            HealDamage(tickHeal);
            yield return new WaitForSeconds(.5F);
            StartCoroutine(TakeHoT(healAmount - tickHeal, time - .5F));
        }
        else
        {
            HealDamage(healAmount);
        }
    }

    public float GetHealth()
    {
        return health;
    }

    public GameObject SpawnGameObject(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        GameObject spawn = (GameObject)Instantiate(prefab, position, rotation);
        NetworkServer.Spawn(spawn);
        return spawn;
    }

    [Command]
    public void CmdSpawnGameObject(string prefab, Vector3 position, Quaternion rotation)
    {
        GameObject prefabObject = Resources.Load(prefab, typeof(GameObject)) as GameObject;
        GameObject spawn = (GameObject)Instantiate(prefabObject, position, rotation);
        NetworkServer.Spawn(spawn);
    }

    public void DestroyGameObject(GameObject gameObjectToDestroy)
    {
        Destroy(gameObjectToDestroy);
    }

    public void RunCoroutine(CoroutineFunction function)
    {
        StartCoroutine(function());
    }

    public bool IsDoTApplied(string abilityName)
    {
        return DoTList.ContainsKey(abilityName);
    }

    public virtual void TakeTaunt(string tauntingCharacter)
    {
    }

    public void RegisterStatusEffect(string title, StatusEffect statusEffect, float effectNumber) 
    {
        if (HasStatusEffect(title))
        {
            GetStatusEffect(title).AddNumber(effectNumber);
        }
        else
        {
            statusEffect.SetCharacter(this);
            statusEffect.SetTitle(title);
            statusEffects.Add(title, statusEffect);
            GetStatusEffect(title).AddNumber(effectNumber);
        }
    }

    

    public bool HasStatusEffect(string title)
    {
        return statusEffects.ContainsKey(title);
    }

    public StatusEffect GetStatusEffect(string title)
    {
        return statusEffects[title];
    }

    public List<StatusEffect> GetStatusEffects()
    {
        List<StatusEffect> statusEffectsList = new List<StatusEffect>();
        statusEffectsList.AddRange(statusEffects.Values);
        return statusEffectsList;
    }

    public void RemoveStatusEffect(string title)
    {
        statusEffectsToDelete.Add(title);
    }
}
