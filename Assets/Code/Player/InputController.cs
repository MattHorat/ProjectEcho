﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class InputController : MonoBehaviour {
    public PlayerCharacter playerCharacter;

    private static float cameraSpeed = 10;
    private bool isSpawning = false;
    private bool controlsEnabled = true;
    private GameObject knight;
    private GameObject priest;
    private GameObject archer;
    private GameObject rogue;
    private GameObject arcanist;
    private GameObject spellBreaker;
    private GameObject doorKeeperRegen;
    private GameObject doorKeeperChains;

    void Update () {
        if (playerCharacter != null && playerCharacter.isActive && controlsEnabled)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                playerCharacter.StartMove(Character.Direction.Up);
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                playerCharacter.StartMove(Character.Direction.Left);
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                playerCharacter.StartMove(Character.Direction.Down);
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                playerCharacter.StartMove(Character.Direction.Right);
            }
            if (Input.GetKeyUp(KeyCode.W))
            {
                playerCharacter.StopMove(Character.Direction.Up);
            }
            if (Input.GetKeyUp(KeyCode.A))
            {
                playerCharacter.StopMove(Character.Direction.Left);
            }
            if (Input.GetKeyUp(KeyCode.S))
            {
                playerCharacter.StopMove(Character.Direction.Down);
            }
            if (Input.GetKeyUp(KeyCode.D))
            {
                playerCharacter.StopMove(Character.Direction.Right);
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                playerCharacter.UseAbility(0);
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                playerCharacter.UseAbility(1);
            }
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                playerCharacter.UseAbility(2);
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                playerCharacter.UseAbility(3);
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                playerCharacter.UseAbility(4);
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                playerCharacter.UseAbility(5);
            }
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                playerCharacter.TriggerAbility();
            }
            if (Input.GetKeyDown(KeyCode.F))
            {
                playerCharacter.InteractWithObject();
            }
            if (Input.GetKeyDown(KeyCode.F7))
            {
                playerCharacter.gameObject.GetComponent<DamageLog>().OutputDamageAmounts();
            }
            if (Input.GetKeyDown(KeyCode.F10))
            {
                ((Boss)playerCharacter.boss).SkipPhase();
            }
        }
        else if (playerCharacter != null)
        {
            if (Input.GetKey(KeyCode.W))
            {
                Vector3 currentPos = Camera.main.transform.position;
                Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, new Vector3(currentPos.x, currentPos.y, currentPos.z + cameraSpeed), 0.5F * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.A))
            {
                Vector3 currentPos = Camera.main.transform.position;
                Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, new Vector3(currentPos.x - cameraSpeed, currentPos.y, currentPos.z), 0.5F * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.S))
            {
                Vector3 currentPos = Camera.main.transform.position;
                Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, new Vector3(currentPos.x, currentPos.y, currentPos.z - cameraSpeed), 0.5F * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.D))
            {
                Vector3 currentPos = Camera.main.transform.position;
                Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, new Vector3(currentPos.x + cameraSpeed, currentPos.y, currentPos.z), 0.5F * Time.deltaTime);
            }
        }
        HandleTestSpawns();
	}

    public static Vector3 GetMousePosition()
    {
        RaycastHit hit;
        Ray ray;
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        int layermask = (1 << 9);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layermask))
        {
            Vector3 endPoint;

            endPoint = hit.point;
            endPoint.y = endPoint.y + 0.5F;
            return endPoint;
        }
        return new Vector3();
    }

    public void HandleTestSpawns()
    {
        if (Input.GetKeyDown(KeyCode.F11))
        {
            if (isSpawning)
            {
                controlsEnabled = true;
            }
            else
            {
                controlsEnabled = false;
            }
            isSpawning = !isSpawning;
        }
        if (isSpawning && Input.GetKeyDown(KeyCode.F1))
        {
            SpawnGameObject(knight);
        }
        if (isSpawning && Input.GetKeyDown(KeyCode.F2))
        {
            SpawnGameObject(priest);
        }
        if (isSpawning && Input.GetKeyDown(KeyCode.F3))
        {
            SpawnGameObject(archer);
        }
        if (isSpawning && Input.GetKeyDown(KeyCode.F4))
        {
            SpawnGameObject(rogue);
        }
        if (isSpawning && Input.GetKeyDown(KeyCode.F5))
        {
            SpawnGameObject(arcanist);
        }
        if (isSpawning && Input.GetKeyDown(KeyCode.F6))
        {
            SpawnGameObject(spellBreaker);
        }
        if (isSpawning && Input.GetKeyDown(KeyCode.F7))
        {
            SpawnGameObject(doorKeeperRegen);
        }
        if (isSpawning && Input.GetKeyDown(KeyCode.F8))
        {
            SpawnGameObject(doorKeeperChains);
        }
    }

    private void SpawnGameObject(GameObject spawn)
    {
        GameObject spawnedObject = (GameObject)Instantiate(spawn, Vector3.zero, Quaternion.identity);
        NetworkServer.Spawn(spawnedObject);
    }

    void Start()
    {
        knight = Resources.Load("Prefabs/Reolus/StageTwo/Knight", typeof(GameObject)) as GameObject;
        priest = Resources.Load("Prefabs/Reolus/StageTwo/Priest", typeof(GameObject)) as GameObject;
        archer = Resources.Load("Prefabs/Reolus/StageTwo/Archer", typeof(GameObject)) as GameObject;
        rogue = Resources.Load("Prefabs/Reolus/StageTwo/Rogue", typeof(GameObject)) as GameObject;
        arcanist = Resources.Load("Prefabs/Reolus/StageTwo/Arcanist", typeof(GameObject)) as GameObject;
        spellBreaker = Resources.Load("Prefabs/Reolus/StageTwo/SpellBreaker", typeof(GameObject)) as GameObject;
        doorKeeperRegen = Resources.Load("Prefabs/Reolus/StageTwo/DoorKeeperRegen", typeof(GameObject)) as GameObject;
        doorKeeperChains = Resources.Load("Prefabs/Reolus/StageTwo/DoorKeeperChains", typeof(GameObject)) as GameObject;
    }
}
