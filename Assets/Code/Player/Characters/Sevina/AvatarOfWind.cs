﻿using UnityEngine;
using System.Collections;
using System;

public class AvatarOfWind : Ability {
    private static float range = 5;
    private int charges = 5;
    private static float cooldown = 5;
    private static float speedUpFactor = 200;
    private static float transformLength = 20;

    private bool isActive = false;
    private float transformTimer;
    
    public override string GetName()
    {
        return "Avatar of the Wind God";
    }

	public override string GetTooltip()
	{
        string tooltip;
        if (isActive)
        {
            tooltip = "<color=black>		           <i>Instant</i>\n Teleport a small distance.\n<i>" + charges + " charges</i></color>";
        }
        else
        {
            tooltip = "<color=black>		           <i>Passive</i>\nMove faster as the blood metre fills. When 100 blood is reached this ability can be activated to transform "
            + "into the avatar of the wind god. When transformed this ability becomes <b>Wind Rush</b>.</color>";
        }
		return tooltip;
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        if (!isActive)
        {
            isActive = true;
            character.resource = 0;
            transformTimer = Time.time;
        }
        else
        {
            float distance = (position - character.transform.position).magnitude;
            if (distance > range)
            {
                distance = range;
            }
            Vector3 newPos = character.transform.position + character.transform.forward * distance;
            newPos.y = 0;
            character.transform.position = Vector3.MoveTowards(character.transform.position, position, distance);
            charges--;
            character.StartCoroutine(AddCharge(cooldown));
        }
    }

    public override void AbilityUpdate()
    {
        character.speed = 8F + character.resource / speedUpFactor;
        if (isActive)
        {
            // TODO: Do this in a better way
            character.resource = 0;

            if (transformTimer + transformLength < Time.time)
            {
                isActive = false;
            }
        }
    }

    protected override void AbilityAim(Vector3 position)
    {
        if (isActive)
        {
            DrawRange(range, character.transform.position, 1);
            DrawRange(.5F, position, 2);
        }
    }

    private IEnumerator AddCharge(float yieldTime)
    {
        yield return new WaitForSeconds(yieldTime);
        charges++;
    }

    public override void Interrupt()
    {
    }

    public override bool CanCast()
    {
        return (!isActive && character.resource == 100) || (character.isMovementAllowed && charges > 0 && isActive);
    }
}
