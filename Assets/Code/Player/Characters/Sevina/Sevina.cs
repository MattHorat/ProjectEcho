﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;

public class Sevina : PlayerCharacter {
    public override void Start()
    {
        if (isLocalPlayer)
        {
            health = 4000;
            maxHealth = 4000;
            speed = 8F;
            resource = 0;
            resourceMax = 100;
            abilities = new List<Ability>();
            abilities.Add(gameObject.GetComponent<AvatarOfWind>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<BloodBoil>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<Flurry>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<Gash>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<GroundPound>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<Spin>().SetCharacter(this));
            base.Start();
        }
        characterName = "Sevina";
    }

    protected override void LocalTakeDamage(float damageAmount, GameObject damageDealer, string moveName = null)
    {
        float newResourceAmount = resource + damageAmount / 10;
        if (newResourceAmount > 100)
        {
            newResourceAmount = 100;
        }
        resource = newResourceAmount;

        base.LocalTakeDamage(damageAmount, damageDealer, moveName);
    }

    public void AddResource(float resourceAmount)
    {
        RpcAddResource(resourceAmount);
    }

    [ClientRpc]
    public void RpcAddResource(float amount)
    {
        if (isLocalPlayer)
        {
            if (resource + amount > resourceMax)
            {
                resource = resourceMax;
            }
            else
            {
                resource += amount;
            }
        }
    }
}
