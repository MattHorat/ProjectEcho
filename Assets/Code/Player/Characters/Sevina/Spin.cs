﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spin : Ability {
    private static float channelTime = 2;
    private static float spinTimes = 2;
    private static float range = 3;
    private static float damageAmount = 190;
    private static float spinResourceIncrease = 8.4F;

    public override string GetName()
    {
        return "Spin";
    }

	public override string GetTooltip()
	{
		return "<color=black>\t\t<i>" + channelTime + " sec channel</i>\nSevina’s axes spin around her twice doing <b><color=maroon>"
			+ damageAmount + "</color></b> damage each time an axe hits a target upto a maximum of <b><color=maroon>420</color></b> damage. The axes spin around " + spinTimes + " times.</color>";
	}

    public Spin()
    {
        channelLength = channelTime;
    }

    protected override void AbilityTrigger(Vector3 position)
    {
        isChanneling = true;
        channelTimer = Time.time + channelLength;
        SpinDamage();
        character.DisallowCasts(channelTime);
    }

    public override void AbilityUpdate()
    {
        if (isChanneling && channelTimer < Time.time)
        {
            SpinDamage();
            isChanneling = false;
        }
    }

    public override void Interrupt()
    {
        if (!character.isMovementAllowed)
        {
            isChanneling = false;
            character.ForceAllowCasts();
        }
    }

    private void SpinDamage()
    {
        Collider[] hitColliders = Physics.OverlapSphere(character.transform.position, range);
        List<GameObject> enemies = new List<GameObject>();
        foreach (Collider collider in hitColliders)
        {
            if (collider.CompareTag("Enemy"))
            {
                enemies.Add(collider.gameObject);
            }
        }
        foreach (GameObject enemy in enemies)
        {
            Character enemyCharacter = enemy.GetComponent<Character>();
            character.CauseDamage(enemyCharacter, damageAmount, GetName());
            if (character.resource + spinResourceIncrease > character.resourceMax)
            {
                character.resource = character.resourceMax;
            }
            else
            {
                character.resource += spinResourceIncrease;
            }
        }
    }

    protected override void AbilityAim(Vector3 position)
    {
        DrawRange(range, character.transform.position, 1);
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed;
    }
}
