﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class GroundPound : Ability {
    private static float range = 3;
    private static float damageAmount = 290;
    private static float armourLowerAmount = .25F;
    private static float armourLowerTime = 8;
    private static float resourceIncreaseMultiplier = 7.6F;
    private static float cooldownTime = 5;

    public override string GetName()
    {
        return "Ground Slam";
    }

	public override string GetTooltip()
	{
		return "<color=black>\t\t        <i>Instant</i>\nCause an explosion of blood around Sevina that deals <b><color=maroon>"
			+ damageAmount + "</color></b> to any target hit and also increases the damage they take by <b><color=orange>25%</color></b> for "
			+ armourLowerTime + " seconds.\n<i>" + cooldownTime + " sec cooldown</i></color>";
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        Collider[] hitColliders = Physics.OverlapSphere(character.transform.position, range);
        List<GameObject> enemies = new List<GameObject>();
        foreach (Collider collider in hitColliders)
        {
            if (collider.CompareTag("Enemy"))
            {
                enemies.Add(collider.gameObject);
            }
        }
        foreach (GameObject enemy in enemies)
        {
            character.CauseDamage(enemy.GetComponent<Character>(), damageAmount, GetName());
            CmdLowerArmour(armourLowerAmount, armourLowerTime, enemy);
        }
        cooldownTimer = Time.time + cooldownTime;
        float resourceIncrease = resourceIncreaseMultiplier * enemies.Count;
        if (character.resource + resourceIncrease > character.resourceMax)
        {
            character.resource = character.resourceMax;
        }
        else
        { 
            character.resource += resourceIncrease;
        }
    }

    [Command]
    public void CmdLowerArmour(float armourAmount, float time, GameObject enemy)
    {
        enemy.GetComponent<Character>().LowerArmour(armourLowerAmount, armourLowerTime);
    }

    protected override void AbilityAim(Vector3 position)
    {
        DrawRange(range, character.transform.position, 1);
    }

    public override void AbilityUpdate()
    {
    }

    public override void Interrupt()
    {
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed;
    }
}
