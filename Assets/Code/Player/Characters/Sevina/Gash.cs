﻿using UnityEngine;
using System.Collections;

public class Gash : Ability {
    private static float range = 3;
    private static float initialDamageAmount = 250;
    private static float teleportDamageAmount = 105;
    private static float cooldownTime = 10;
    private static float initialResourceIncrease = 8.4F;
    private static float teleportResourceIncrease = 4.2F;
    private static float teleportTime = 6;

    private bool isActive = false;
    private GameObject targetedEnemy;
    
    public override string GetName()
    {
        return "Gash";
    }

	public override string GetTooltip()
	{
        string tooltip;
        if (!isActive)
        {
            tooltip = "<color=black>\t\t        <i>Instant</i>\nCuts open the target dealing <b><color=maroon>" + initialDamageAmount + "</color></b> damage and allowing Sevina to teleport back to "
            + "the target for the next " + teleportTime + " seconds dealing <b><color=maroon>105</color></b> damage.\n<i>" + cooldownTime + " sec cooldown.</i></color>";
        }
        else
        {
            tooltip = "<color=black>\t\t        <i>Instant</i>\nTeleport back to the target dealing <b><color=maroon>" + teleportDamageAmount + "</color></b> damage</color>";
        }
		return tooltip;
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        Collider[] hitColliders = Physics.OverlapSphere(position, 1);
        GameObject enemy = null;
        foreach (Collider collider in hitColliders)
        {
            if (collider.CompareTag("Enemy"))
            {
                enemy = collider.gameObject;
            }
        }
        if (enemy != null && Vector3.Distance(character.transform.position, enemy.transform.position) > range)
        {
            enemy = null;
        }
        if (isActive)
        {
            character.transform.position = new Vector3(targetedEnemy.transform.position.x, targetedEnemy.transform.position.y, targetedEnemy.transform.position.z);
            isActive = false;
            character.CauseDamage(targetedEnemy.GetComponent<Character>(), teleportDamageAmount, GetName());
            if (character.resource + teleportResourceIncrease > character.resourceMax)
            {
                character.resource = character.resourceMax;
            }
            else
            {
                character.resource += teleportResourceIncrease;
            }
            cooldownTimer = Time.time + cooldownTime;
        }
        else if (enemy != null)
        {
            character.CauseDamage(enemy.GetComponent<Character>(), initialDamageAmount, GetName());
            isActive = true;
            targetedEnemy = enemy;
            if (character.resource + initialResourceIncrease > character.resourceMax)
            {
                character.resource = character.resourceMax;
            }
            else
            { 
                character.resource += initialResourceIncrease;
            }
            character.StartCoroutine(Deactivate());
        }
    }

    protected override void AbilityAim(Vector3 position)
    {
        if (!isActive)
        {
            DrawRange(range, character.transform.position, 1);
            DrawRange(0.5F, position, 2);
        }
        else
        {
            Debug.DrawLine(character.transform.position, targetedEnemy.transform.position, Color.red);
        }
    }

    public override void AbilityUpdate()
    {
    }

    public override void Interrupt()
    {
    }

    private IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(teleportTime);
        if (isActive)
        {
            isActive = false;
            cooldownTimer = Time.time + cooldownTime;
        }
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed;
    }
}
