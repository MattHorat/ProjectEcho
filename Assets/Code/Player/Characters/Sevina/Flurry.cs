﻿using UnityEngine;
using System.Collections;

public class Flurry : Ability {
    private static float channelTime = 3;
    private static float range = 2.5F;
    private static float halfConeAngle = 35;
    private static float damageLengthFactor = .5F;
    private static float damageAmount = 150;
    private static float energyGain = 19;

    public override string GetName()
    {
        return "Flurry";
    }

	public override string GetTooltip()
	{
		return "<color=black>\t\t <i>" + channelTime + " sec channel</i>\nRapidly attack in a cone doing <b><color=maroon>" + damageAmount + "</color></b> damage per second. "
			+ "Each second this ability is channel the damage increases by <b><color=maroon>10%</color></b>. Can move while using this ability.</color>";
	}

    public void Start()
    {
        channelLength = channelTime;
    }

    protected override void AbilityTrigger(Vector3 position)
    {
        isChanneling = true;
        channelTimer = Time.time + channelLength;
        character.DisallowCasts(channelTime);
    }

    public override void AbilityUpdate()
    {
        if (isChanneling && channelTimer < Time.time)
        {
            isChanneling = false;
        }

        if (isChanneling)
        {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (GameObject enemy in enemies)
            {
                if (Vector3.Distance(character.transform.position, enemy.transform.position) < range)
                {
                    float cone = Mathf.Cos(halfConeAngle * Mathf.Deg2Rad);
                    Vector3 heading = (enemy.transform.position - character.transform.position).normalized;

                    if (Vector3.Dot(character.transform.forward, heading) > cone)
                    {
                        character.CauseDamage(enemy.GetComponent<Character>(), ((damageAmount * Time.deltaTime) + (damageAmount * damageLengthFactor * (Time.time - (channelTimer - channelLength)) * Time.deltaTime)), GetName());
                    }
                }
                float resourceIncrease = energyGain / channelTime * Time.deltaTime;
                if (character.resource + resourceIncrease > character.resourceMax)
                {
                    character.resource = character.resourceMax;
                }
                else
                {
                    character.resource += resourceIncrease;
                }
            }
            Transform newRotation = character.transform;
            newRotation.Rotate(Vector3.up, halfConeAngle);
            Debug.DrawLine(character.transform.position, character.transform.position + newRotation.forward * range, Color.green);
            newRotation.Rotate(Vector3.up, -halfConeAngle * 2);
            Debug.DrawLine(character.transform.position, character.transform.position + newRotation.forward * range, Color.green);
        }
    }

    public override void Interrupt()
    {
        if (!character.isMovementAllowed)
        {
            isChanneling = false;
            character.ForceAllowCasts();
        }
    }

    protected override void AbilityAim(Vector3 position)
    {
        Transform newRotation = character.transform;
        newRotation.Rotate(Vector3.up, halfConeAngle);
        Debug.DrawLine(character.transform.position, character.transform.position + newRotation.forward * range, Color.red);
        newRotation.Rotate(Vector3.up, -halfConeAngle * 2);
        Debug.DrawLine(character.transform.position, character.transform.position + newRotation.forward * range, Color.red);
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed;
    }
}
