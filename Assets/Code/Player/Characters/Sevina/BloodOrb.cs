﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class BloodOrb : NetworkBehaviour {
    public static float orbResourceGain = 4;
    private static float spawnLength = 5;

    public void Start()
    {
        if (isServer)
        {
            StartCoroutine(DestroyThis());
        }
    }

    private IEnumerator DestroyThis()
    {
        yield return new WaitForSeconds(spawnLength);
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (isServer)
        {
            if (collision.gameObject.name.Equals("Sevina(Clone)"))
            {
                Sevina sevina = collision.gameObject.GetComponent<Sevina>();
                sevina.AddResource(orbResourceGain);
                Destroy(gameObject);
            }
        }
    }
}
