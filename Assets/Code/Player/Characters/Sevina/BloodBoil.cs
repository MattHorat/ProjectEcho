﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class BloodBoil : Ability {
    private static float damageAmount = 750;
    private static float damageTime = 10;
    private static float minimumOrbSpawnTime = 1;
    private static float maximumOrbSpawnTime = 3;

    private bool isActive = false;
    private float bloodSpawnTimer;
    private List<GameObject> targetedEnemies = new List<GameObject>();
    private List<float> spawnTimes = new List<float>();
    public GameObject BloodOrbPrefab;

    public override string GetName()
    {
        return "Blood Boil";
    }

	public override string GetTooltip()
	{
		return "<color=black>\t\t        <i>Instant</i>\nBoil the blood of the enemy causing <b><color=maroon>" + damageAmount + "</color></b> damage over "
			+ damageTime + " seconds. Every second the target has a chance to drop a blood orb worth <b><color=navy>" + BloodOrb.orbResourceGain + "</color></b> blood.</color>";
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        Collider[] hitColliders = Physics.OverlapSphere(position, 1);
        GameObject enemy = null;
        foreach (Collider collider in hitColliders)
        {
            if (collider.CompareTag("Enemy"))
            {
                enemy = collider.gameObject;
            }
        }
        if (enemy != null)
        {
            if (!targetedEnemies.Contains(enemy))
            {
                character.CmdCauseDamageOverTime(enemy, damageAmount, damageTime, "Blood Boil");
                isActive = true;
                spawnTimes.Add(Time.time);
                targetedEnemies.Add(enemy);
                character.StartCoroutine(Deactivate());
            }
        }
    }

    public override void AbilityUpdate()
    {
        int i = 0;
        foreach (float cooldown in spawnTimes)
        {
            if (isActive && cooldown < Time.time)
            {
                spawnTimes[i] = Time.time + Random.Range(minimumOrbSpawnTime, maximumOrbSpawnTime);
                if (targetedEnemies[i] != null)
                {
                    CmdSpawnBloodOrb(targetedEnemies[i].transform.position);
                }
            }
            i++;
        }
    }

    [Command]
    public void CmdSpawnBloodOrb(Vector3 position)
    {
        NetworkServer.Spawn((GameObject)Instantiate(BloodOrbPrefab, position, Quaternion.identity));
    }

    protected override void AbilityAim(Vector3 position)
    {
        DrawRange(.5F, position, 1);
    }

    public override void Interrupt()
    {
    }

    private IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(10F);
        spawnTimes.RemoveAt(0);
        targetedEnemies.RemoveAt(0);
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed;
    }
}
