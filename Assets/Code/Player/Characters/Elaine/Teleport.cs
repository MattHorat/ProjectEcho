﻿using UnityEngine;
using System.Collections;
using System;

public class Teleport : Ability {
    private static float cooldownTime = 40;
    private static float resourceCost = 30;
    private static float channelTime = 1.5F;

    private bool hasCast = false;
    private Vector3 runePosition;

    public override string GetName()
    {
        return "Teleport";
    }

	public override string GetTooltip()
	{
		return "<color=black>Cost - " + resourceCost + "		           <i>Instant</i>\nTeleport to the nearest rune after a "
			+ channelTime + " second delay.\n<i>" + cooldownTime + " sec cooldown</i></color>";
	}
    
    protected override void AbilityTrigger(Vector3 position)
    {
        GameObject closestRune = null;
        float closestDistance = 400;
        GameObject[] runes = GameObject.FindGameObjectsWithTag("Rune");
        foreach (GameObject rune in runes)
        {
            float distance = Vector3.Distance(rune.transform.position, character.transform.position);
            if (distance < closestDistance)
            {
                closestRune = rune;
                closestDistance = distance;
            }
        }
        if (closestRune != null)
        {
            hasCast = true;
            character.resource -= resourceCost;
            runePosition = closestRune.transform.position;
            channelTimer = Time.time;
        }
    }

    public override void AbilityUpdate()
    {
        if (hasCast && Time.time - channelTimer > channelTime)
        {
            hasCast = false;
            character.transform.position = runePosition;
            cooldownTimer = Time.time + cooldownTime;
            
        }
    }

    protected override void AbilityAim(Vector3 position)
    {
        GameObject closestRune = null;
        float closestDistance = 400;
        GameObject[] runes = GameObject.FindGameObjectsWithTag("Rune");
        foreach (GameObject rune in runes)
        {
            float distance = Vector3.Distance(rune.transform.position, character.transform.position);
            if (distance < closestDistance)
            {
                closestRune = rune;
                closestDistance = distance;
            }
        }
        if (closestRune != null)
        {
            Debug.DrawLine(character.transform.position, closestRune.transform.position, Color.red);
        }
    }

    public override void Interrupt()
    {
    }

    public override bool CanCast()
    {
        return character.resource > resourceCost;
    }
}
