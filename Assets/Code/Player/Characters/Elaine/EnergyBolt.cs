﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class EnergyBolt : Ability
{
    private static float resourceCost = 30;
    private static float range = 10;
    private int charges = 3;
    private static float chargeRegainTimer = 5;
    public static float damage = 250;
    public static float speed = 5;
    public static float runeExplosionRange = 4;

    public GameObject energyBoltPrefab;
    
    public override string GetName()
    {
        return "Energy Bolt";
    }

	public override string GetTooltip()
	{
		return "<color=black>Cost - " + resourceCost + "		             <i>Instant</i>\nShoot a slow moving ball of energy that deals "
			+ "<b><color=maroon>" + damage + "</color></b> damage to any targets hit.\n"
			+ "<i>" + charges + " charges.\nInteracts with Rune.</i></color>";
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        Vector3 aimingPosition = (position - character.transform.position).normalized * range;
        aimingPosition = character.transform.position + aimingPosition;
        charges--;
        character.resource -= resourceCost;
        CmdSpawnEnergyBolt(aimingPosition, character.gameObject);
        character.StartCoroutine(AddCharge(chargeRegainTimer));
    }

    [Command]
    public void CmdSpawnEnergyBolt(Vector3 position, GameObject creator)
    {
        GameObject boltBullet = (GameObject)Instantiate(energyBoltPrefab, creator.transform.position, Quaternion.identity);
        NetworkServer.Spawn(boltBullet);
        boltBullet.GetComponent<EnergyBoltBullet>().ShootAt(position);
        boltBullet.GetComponent<EnergyBoltBullet>().creator = creator.GetComponent<Character>();
    }

    protected override void AbilityAim(Vector3 position)
    {
        Vector3 aimingPosition = (position - character.transform.position).normalized * range;
        Debug.DrawLine(character.transform.position, character.transform.position + aimingPosition, Color.red);
    }

    public override void AbilityUpdate()
    {
    }

    private IEnumerator AddCharge(float yieldTime)
    {
        yield return new WaitForSeconds(yieldTime);
        charges++;
    }

    public override void Interrupt()
    {
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed && charges > 0 && character.resource >= resourceCost;
    }
}
