﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class Protection : Ability {
    private static float resourceCost = 50;
    private static float channelTime = 1.5F;
    public static float spawnLength = 5;
    private static float cooldownTime = 30;

    private Vector3 targetPosition;
    public GameObject protectionPrefab;

    public Protection()
    {
        channelLength = channelTime;
    }

    public override string GetName()
    {
        return "Protection";
    }

	public override string GetTooltip()
	{
		return "<color=black>Cost - " + resourceCost + "		   <i>" + channelTime + " sec cast</i>\nCreates a shield over the target rune that will absorb enemy projectiles "
			+ "for " + spawnLength + " seconds.\n<i>" + cooldownTime + " sec cooldown</i></color>";
	}
    
    protected override void AbilityTrigger(Vector3 position)
    {
        GameObject closestRune = null;
        float closestDistance = 400;
        GameObject[] runes = GameObject.FindGameObjectsWithTag("Rune");
        foreach (GameObject rune in runes)
        {
            if (Vector3.Distance(rune.gameObject.transform.position, character.gameObject.transform.position) < closestDistance)
            {
                closestRune = rune;
            }
        }
        if (closestRune != null)
        {
            channelTimer = Time.time + channelLength;
            targetPosition = closestRune.transform.position;
            isChanneling = true;
            character.resource -= resourceCost;
        }
    }

    public override void AbilityUpdate()
    {
        if (isChanneling && Time.time - channelTimer > 0)
        {
            isChanneling = false;
            CmdSpawnProtection(targetPosition);
            cooldownTimer = Time.time + cooldownTime;
        }
    }

    [Command]
    public void CmdSpawnProtection(Vector3 position)
    {
        NetworkServer.Spawn((GameObject)Instantiate(protectionPrefab, position, Quaternion.identity));
    }

    protected override void AbilityAim(Vector3 position)
    {
        DrawRange(.5F, position, 1);
    }

    public override void Interrupt()
    {
        isChanneling = false;
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed && character.resource > resourceCost && !character.IsMoving();
    }
}
