﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class TwinBolt : Ability {
    private static float channelTime = .7F;
    private static float resourceCost = 40;
    private static float range = 10;
    public static float damageAmount = 125;
    public static float healAmount = 125;
    public static float runeExplosionRadius = 4;
    public static float spawnSpeed = 8;

    public GameObject twinBoltPrefab;
    private Vector3 targetPosition;

    public void Start()
    {
        channelLength = channelTime;
    }

    public override string GetName()
    {
        return "Twin Bolt";
    }

	public override string GetTooltip()
	{
		return "<color=black>Cost - " + resourceCost + "	\t <i>" + channelTime + " sec cast</i>\nFires a fast moving bolt of energy that deals "
			+ "<b><color=maroon>" + damageAmount + "</color></b> damage to any enemies hit and heals any allies hit for "
			+ "<b><color=green>" + healAmount + "</color></b> health.\n<i>Interacts with Rune</i></color>";
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        channelTimer = Time.time + channelLength;
        character.resource -= resourceCost;
        targetPosition = position;
        isChanneling = true;
    }

    public override void AbilityUpdate()
    {
        if (isChanneling && Time.time - channelTimer > 0)
        {
            Vector3 aimingPosition = (targetPosition - character.transform.position).normalized * range;
            CmdSpawnTwinBolt(character.transform.position + aimingPosition, character.gameObject);
            isChanneling = false;
        }
    }

    [Command]
    public void CmdSpawnTwinBolt(Vector3 targetPos, GameObject creator)
    {
        GameObject twinBoltBullet = (GameObject)Instantiate(twinBoltPrefab, creator.transform.position, Quaternion.identity);
        NetworkServer.Spawn(twinBoltBullet);
        twinBoltBullet.GetComponent<TwinBoltBullet>().ShootAt(targetPos);
        twinBoltBullet.GetComponent<TwinBoltBullet>().creator = creator.GetComponent<Character>();
            
    }

    protected override void AbilityAim(Vector3 position)
    {
        Vector3 aimingPosition = (position - character.transform.position).normalized * range;
        Debug.DrawLine(character.transform.position, character.transform.position + aimingPosition, Color.red);
    }

    public override void Interrupt()
    {
        isChanneling = false;
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed && character.resource > resourceCost && !character.IsMoving();
    }
}
