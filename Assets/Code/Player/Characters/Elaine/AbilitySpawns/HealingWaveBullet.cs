﻿using UnityEngine;
using System.Collections;
using System;

public class HealingWaveBullet : AICharacter {
    private float healAmount;

    public override void Update()
    {
        if (isServer)
        {
            base.Update();
            if (!isMoving)
            {
                Destroy(gameObject);
            }
        }
    }

    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            speed = HealingWave.spawnSpeed;
            isMoving = true;
        }
    }

    public void ShootAt(Vector3 target, float channelTime)
    {
        MoveTo(target);
        gameObject.transform.localScale += new Vector3(channelTime / 2, 0, 0);
        healAmount = HealingWave.healAmount * channelTime;
    }

    void OnTriggerEnter(Collider collision)
    {
        if (isServer)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                Character player = collision.gameObject.GetComponent<Character>();
                player.HealDamage(healAmount);
            }
            else if (collision.gameObject.CompareTag("Rune"))
            {
                Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, 4);
                foreach (Collider collider in hitColliders)
                {
                    if (collider.CompareTag("Player"))
                    {
                        collider.gameObject.GetComponent<Character>().HealDamage(healAmount);
                    }
                }
            }
        }
    }
}
