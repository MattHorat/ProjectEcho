﻿using UnityEngine;
using System.Collections;
using System;

public class EnergyBoltBullet : AICharacter {
    public Character creator;

    public override void Update()
    {
        if (isServer)
        {
            base.Update();
            if (!isMoving)
            {
                Destroy(gameObject);
            }
        }
    }

    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            speed = EnergyBolt.speed;
            isMoving = true;
        }
    }

    public void ShootAt(Vector3 target)
    {
        MoveTo(target);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Character enemy = collision.gameObject.GetComponent<Character>();
            creator.CauseDamage(enemy, EnergyBolt.damage, "Energy Bolt");
        }
        else if (collision.gameObject.CompareTag("Rune"))
        {
            Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, EnergyBolt.runeExplosionRange);
            foreach (Collider collider in hitColliders)
            {
                if (collider.CompareTag("Enemy"))
                {
                    creator.CauseDamage(collider.gameObject.GetComponent<Character>(), EnergyBolt.damage, "Rune");
                }
            }
        }
    }
}
