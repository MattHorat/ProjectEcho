﻿using UnityEngine;
using System.Collections;
using System;

public class TwinBoltBullet : AICharacter {
    public Character creator;

    public override void Update()
    {
        if (isServer)
        {
            base.Update();
            if (!isMoving)
            {
                Destroy(gameObject);
            }
        }
    }

    public override void Start()
    {
        base.Start();
        if (isServer) 
        {
            speed = TwinBolt.spawnSpeed;
            isMoving = true;
        }
    }

    public void ShootAt(Vector3 target)
    {
        MoveTo(target);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Character enemy = collision.gameObject.GetComponent<Character>();
            creator.CauseDamage(enemy, TwinBolt.damageAmount, "Twin Bolt");
        }
        else if (collision.gameObject.CompareTag("Rune"))
        {
            Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, TwinBolt.runeExplosionRadius);
            foreach (Collider collider in hitColliders)
            {
                if (collider.CompareTag("Enemy"))
                {
                    creator.CauseDamage(collider.gameObject.GetComponent<Character>(), TwinBolt.damageAmount, "Rune");
                }
                if (collider.CompareTag("Player"))
                {
                    collider.gameObject.GetComponent<Character>().HealDamage(TwinBolt.healAmount);
                }
            }
        } 
        else if (collision.gameObject.CompareTag("Player"))
        {
            Character player = collision.gameObject.GetComponent<Character>();
            player.HealDamage(TwinBolt.healAmount);
        }
    }
}
