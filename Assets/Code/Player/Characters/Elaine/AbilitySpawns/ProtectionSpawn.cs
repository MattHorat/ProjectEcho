﻿using UnityEngine;
using System.Collections;
using System;

public class ProtectionSpawn : AICharacter {
    public override void Update()
    {
    }

    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            speed = 4F;
            enabled = false;
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("EnemyProjectile"))
        {
            Destroy(collision.gameObject);
        }
    }

    private IEnumerator DestroyThis()
    {
        yield return new WaitForSeconds(Protection.spawnLength);
        Destroy(gameObject);
    }
}
