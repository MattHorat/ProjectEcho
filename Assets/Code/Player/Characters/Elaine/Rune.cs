﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;

public class Rune : Ability {
    private static float resourceCost = 50;
    private static float maxRunes = 4;
    private static float channelTime = 1;

    public GameObject runePrefab;
    private int currentRunes = 0;
    private Vector3 targetPosition;
    private List<GameObject> runes = new List<GameObject>();

    public Rune()
    {
        channelLength = channelTime;
    }
    
    public override string GetName()
    {
        return "Rune";
    }

	public override string GetTooltip()
	{
		return "<color=black>Cost - " + resourceCost + "		         <i>" + channelTime + " sec cast</i>\nPlace a rune at the targeted location. "
			+ "Abilities that interact with the rune will cause an explosion of energy either healing or damaging targets in a radius around "
			+ "the rune for the original damage/healing of the ability.\n<i>Maximum of " + maxRunes + " runes at one time.</i></color>";
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        channelTimer = Time.time + channelLength;
        targetPosition = position;
        isChanneling = true;
        character.resource -= resourceCost;
        currentRunes++;
    }

    public override void AbilityUpdate()
    {
        if (isChanneling && Time.time - channelTimer > 0)
        {
            CmdSpawnRune(targetPosition);
            isChanneling = false;
        }
    }

    [Command]
    public void CmdSpawnRune(Vector3 spawnPos)
    {
        GameObject spawnedRune = (GameObject)Instantiate(runePrefab, spawnPos, Quaternion.identity);
        NetworkServer.Spawn(spawnedRune);
        runes.Add(spawnedRune);
        if (runes.Count > maxRunes)
        {
            Destroy(runes[0]);
            runes.RemoveAt(0);
        }
    }

    protected override void AbilityAim(Vector3 position)
    {
        DrawRange(.5F, position, 1);
    }

    public override void Interrupt()
    {
        isChanneling = false;
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed && character.resource > resourceCost && !character.IsMoving();
    }
}
