﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Elaine : PlayerCharacter {
    private float energyGainTimer = -1000000;

    public override void Start()
    {
        if (isLocalPlayer)
        {
            health = 3800;
            maxHealth = 3800;
            speed = 8F;
            resource = 1000;
            resourceMax = 1000;
            damageMultiplier = 0.9F;
            abilities = new List<Ability>();
            abilities.Add(gameObject.GetComponent<EnergyBolt>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<HealingWave>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<Protection>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<Rune>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<Teleport>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<TwinBolt>().SetCharacter(this));
            base.Start();
        }
        characterName = "Elaine";
    }

    public override void Update()
    {
        if (isLocalPlayer)
        {
            base.Update();
            if (Time.time - energyGainTimer > .5)
            {
                energyGainTimer = Time.time;
                if (resource < resourceMax)
                {
                    resource += 8;
                }
            }
        }
    }
}
