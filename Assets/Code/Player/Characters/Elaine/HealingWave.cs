﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class HealingWave : Ability
{
    private static float resourceCost = 30;
    private static float channelTime = 3;
    private static float range = 10;
    public static float healAmount = 500;
    public static float spawnSpeed = 8;

    public GameObject healingWavePrefab;
    private Vector3 targetPosition;
    
    public override string GetName()
    {
        return "Healing Wave";
    }

	public override string GetTooltip()
	{
		return "<color=black>Cost - " + resourceCost + "	  \t<i>" + channelTime + " sec channel</i>\nCharges a wave of healing energy that heals for "
			+ "<b><color=green>" + healAmount + "</color></b> health per second charged.\n<i>Interacts with Rune</i></color>";
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        channelTimer = Time.time;
        targetPosition = position;
        isChanneling = true;
        character.resource -= resourceCost;
    }

    public override void AbilityUpdate()
    {
        if (isChanneling)
        {
            if (character.isMovementAllowed)
            {
                if (Array.Exists(character.moveDirections, delegate(bool x) { return x; }) || Time.time - channelTimer > channelTime)
                {
                    Vector3 aimingPosition = (targetPosition - character.transform.position).normalized * range;
                    isChanneling = false;
                    CmdSpawnHealingWave(character.transform.position + aimingPosition, character.gameObject, Time.time - channelTimer);
                }
            }
            else
            {
                // We have been stunned, healing wave should still fire
                Vector3 aimingPosition = (targetPosition - character.transform.position).normalized * range;
                isChanneling = false;
                CmdSpawnHealingWave(character.transform.position + aimingPosition, character.gameObject, Time.time - channelTimer);
            }
        }
    }

    [Command]
    public void CmdSpawnHealingWave(Vector3 position, GameObject creator, float objectSize)
    {
        GameObject waveBullet = (GameObject)Instantiate(healingWavePrefab, creator.transform.position, Quaternion.identity);
        NetworkServer.Spawn(waveBullet);
        waveBullet.GetComponent<HealingWaveBullet>().ShootAt(position, objectSize);
    }

    protected override void AbilityAim(Vector3 position)
    {
        Vector3 aimingPosition = (position - character.transform.position).normalized * range;
        Debug.DrawLine(character.transform.position, character.transform.position + aimingPosition, Color.red);
    }

    public override void Interrupt()
    {
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed && character.resource > resourceCost && !character.IsMoving();
    }
}
