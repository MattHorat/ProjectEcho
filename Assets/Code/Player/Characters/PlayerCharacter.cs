﻿using UnityEngine;
using System.Collections;
using System.Timers;
using System.Collections.Generic;
using System;
using UnityEngine.Networking;

public abstract class PlayerCharacter : Character, Interactable {
    public static float resurrectTime = 4;

    public float cameraZPos = 7F;
    public float cameraOffset = 5F;
    public bool[] moveDirections = new bool[4];
    public bool isMovementAllowed = true;
    public float resource;
    public float resourceMax;

    protected List<Ability> possibleAbilities;
    protected List<Ability> abilities = new List<Ability>();
    private bool isAimingAbility = false;
    private int aimedAbility;
    public Character boss;
    private Vector3 desiredPosition;
    private int stunCounter = 0;
    private bool canCast = true;
    Ability currentAbility;
    private bool movementForced = false;
    private float forcedMovementTimer;
    private Vector3 forcedMovementPosition;
    private bool pullForced = false;
    private float pullMovementTimer;
    private Vector3 pullMovementPosition;
    public bool isActive = true;

    private Vector3 cameraVelocity = Vector3.zero;
    private Animator animator;

    public Respawn respawnAbility;
    public GameStateController gameState;

    virtual public void Start()
    {
        respawnAbility = gameObject.GetComponent<Respawn>();
        respawnAbility.SetCharacter(this);
        gameState = GameObject.Find("GameStateController(Clone)").GetComponent<GameStateController>();
        respawnAbility.gameState = gameState;

        isMovementAllowed = true;
        isAimingAbility = false;
        stunCounter = 0;
        movementForced = false;
        pullForced = false;
        isActive = true;

        animator = GetComponentInChildren<Animator>();
    }

    public override void Update()
    {
        //Rigidbody rb = GetComponent<Rigidbody>();
    }
    
    public void FixedUpdate()
    {
        if (!isLocalPlayer || !isActive)
        {
            return;
        }
        
        Rigidbody rb = GetComponent<Rigidbody>();
        Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, new Vector3(rb.position.x, cameraZPos, rb.position.z - cameraOffset), ref cameraVelocity, 0.01F);
        if (isMovementAllowed)
        {
            Vector3 movementDirection = new Vector3();
            if (moveDirections[(int)Direction.Up])
            {
                movementDirection.z = 1;
            }
            if (moveDirections[(int)Direction.Down])
            {
                movementDirection.z = -1;
            }
            if (moveDirections[(int)Direction.Left])
            {
                movementDirection.x = -1;
            }
            if (moveDirections[(int)Direction.Right])
            {
                movementDirection.x = 1;
            }
            movementDirection = Vector3.Normalize(movementDirection);
            rb.MovePosition(rb.position + movementDirection * speed * Time.deltaTime);
            rb.velocity = Vector3.zero;
            if (movementDirection.magnitude > 0)
            {
                animator.SetFloat("Speed", 5);
            }
            else
            {
                animator.SetFloat("Speed", 0);
            }

            base.Update();
        }
        if (movementForced)
        {
            MoveTowardsPosition();
        }
        if (pullForced)
        {
            PullTowardsPosition();
        }
        foreach (Ability ability in abilities)
        {
            if(IsMoving() || pullForced) {
                ability.Interrupt();
                respawnAbility.Interrupt();
            }
            ability.AbilityTick();
            respawnAbility.AbilityTick();
        }
        transform.LookAt(InputController.GetMousePosition());
    }

    void OnGUI()
    {
        if (isLocalPlayer && isActive)
        {
            GUI.Label(new Rect(10, 10, 50, 50), "Health: " + health);
            GUI.Label(new Rect(70, 10, 50, 50), "Resource: " + resource);
            if (boss != null)
            {
                GUI.Label(new Rect(130, 10, 50, 50), "Boss Health: " + boss.GetHealth());
            }
            else
            {
                GameObject bossObject = GameObject.Find("Boss(Clone)");
                if (bossObject)
                {
                    boss = bossObject.GetComponent<Character>();
                }
            }
            GUI.Label(new Rect(190, 10, 50, 50), "Lives Remaining: " + gameState.GetLives());
        }
    }

    public void StartMove(Direction direction)
    {
        moveDirections[(int)direction] = true;
    }

    public void StopMove(Direction direction)
    {
        moveDirections[(int)direction] = false;
    }

    private void DisallowMovement(float timeToStop)
    {
        isMovementAllowed = false;
        stunCounter++;
        StartCoroutine(AllowMovement(timeToStop));
        foreach (Ability ability in abilities)
        {
            ability.Interrupt();
            respawnAbility.Interrupt();
        }
    }

    public void StunPermanently()
    {
        isMovementAllowed = false;
        stunCounter++;
        foreach (Ability ability in abilities)
        {
            ability.Interrupt();
            respawnAbility.Interrupt();
        }
    }

    public void RemovePermanentStun()
    {
        stunCounter--;
        if (stunCounter == 0)
        {
            isMovementAllowed = true;
        }
    }

    private IEnumerator AllowMovement(float timeToStop)
    {
        yield return new WaitForSeconds(timeToStop);
        stunCounter--;
        if (stunCounter == 0)
        {
            isMovementAllowed = true;
        }
    }

    public void UseAbility(int abilityNumber)
    {
        if (canCast)
        {
            currentAbility = abilities[abilityNumber];
            if (currentAbility.WillAbilityTrigger())
            {
                if (!isAimingAbility)
                {
                    currentAbility.StartAiming();
                    isAimingAbility = true;
                    aimedAbility = abilityNumber;
                }
                else
                {
                    abilities[aimedAbility].StopAiming();
                    isAimingAbility = false;
                }
            }
        }
    }

    public void TriggerAbility()
    {
        if (isAimingAbility && canCast && isActiveAndEnabled)
        {
            currentAbility.HandleAbilityTrigger();
            isAimingAbility = false;
            currentAbility.StopAiming();
        }
    }

    public List<Ability> GetPossibleAbilities()
    {
        return possibleAbilities;
    }

    public void SetAbility(int abilityIndex)
    {
        abilities.Add(possibleAbilities[abilityIndex]);
    }

    public List<Ability> GetAbilities()
    {
        return abilities;
    }

    public void Stun(float stunTime)
    {
        DisallowMovement(stunTime);
    }

    public bool IsMoving()
    {
        return Array.Exists(moveDirections, delegate(bool x) { return x; });
    }

    public void DisallowCasts(float stopTime)
    {
        canCast = false;
        StartCoroutine(AllowCasts(stopTime));
    }

    public void DisallowCasts()
    {
        canCast = false;
    }

    public void AllowCasts()
    {
        canCast = true;
    }

    private IEnumerator AllowCasts(float stopTime)
    {
        yield return new WaitForSeconds(stopTime);
        canCast = true;
    }

    public void ForceAllowCasts()
    {
        canCast = true;
    }

    public void InteractWithObject()
    {
        GameObject[] interactables = GameObject.FindGameObjectsWithTag("Interactable");
        foreach (GameObject interactable in interactables)
        {
            if (Vector3.Distance(transform.position, interactable.transform.position) < 2)
            {
                if (interactable.GetComponent<PlayerCharacter>() != null)
                {
                    respawnAbility.SetTarget(interactable);
                    respawnAbility.HandleAbilityTrigger();
                }
                else
                {
                    CmdInteract(interactable);
                }
            }
        }
    }

    [Command]
    public void CmdInteract(GameObject interactable)
    {
        interactable.GetComponent<Interactable>().Interact();
    }

    public void CauseFlash()
    {
        if (isActive)
        {
            GetComponent<PlayerUI>().CauseFlash();
        }
    }

    public void ForceMovementTo(Vector3 pos, float time, bool disallowMovement = true)
    {
        if (isLocalPlayer)
        {
            LocalForceMovementTo(pos, time);
        }
        else
        {
            RpcForceMovementTo(pos, time);
        }
    }

    [ClientRpc]
    public void RpcPullTowards(Vector3 pos, float time)
    {
        pullMovementPosition = pos;
        pullMovementTimer = time;
        pullForced = true;
    }

    [ClientRpc]
    public void RpcForceMovementTo(Vector3 pos, float time)
    {
        if (isLocalPlayer)
        {
            LocalForceMovementTo(pos, time);
        }
    }

    private void LocalForceMovementTo(Vector3 pos, float time)
    {
        DisallowMovement(time);
        forcedMovementPosition = pos;
        forcedMovementTimer = time;
        movementForced = true;
    }

    private void MoveTowardsPosition()
    {
        float xPos = transform.position.x + (forcedMovementPosition.x * Time.deltaTime);
        float zPos = transform.position.z + (forcedMovementPosition.z * Time.deltaTime);
        gameObject.transform.position = new Vector3(xPos, gameObject.transform.position.y, zPos);
        forcedMovementTimer -= Time.deltaTime;
        if (forcedMovementTimer <= 0)
        {
            movementForced = false;
        }
    }

    private void PullTowardsPosition()
    {
        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        if (Vector3.Distance(rb.position, pullMovementPosition) > 1)
        {
            Vector3 moveDirection = (pullMovementPosition - rb.position).normalized * (speed * 0.8F) * Time.deltaTime;
            Vector3 movementPos = new Vector3(rb.position.x + moveDirection.x, 0, rb.position.z + moveDirection.z);
            rb.MovePosition(movementPos);
        }
        rb.velocity = Vector3.zero;
        
        pullMovementTimer -= Time.deltaTime;
        if (pullMovementTimer <= 0)
        {
            pullForced = false;
        }
    }

    [ClientRpc]
    public void RpcSlow(float percentage, float time)
    {
        float slowAmount = speed * percentage;
        speed = speed - slowAmount;
        StartCoroutine(RemoveSlow(time, slowAmount));
    }

    private IEnumerator RemoveSlow(float timeToStop, float amountToReturn)
    {
        yield return new WaitForSeconds(timeToStop);
        speed = speed + amountToReturn;
    }

    protected override void LocalTakeDamage(float damageAmount, GameObject damageDealer, string moveName = null)
    {
        if (isActive)
        {
            damageAmount *= damageMultiplier;
            health -= damageAmount;
            if (moveName != null)
            {
                GameObject.FindObjectOfType<DamageLog>().AddDamageAmount(moveName, damageAmount);
            }
            if (health <= 0)
            {
                health = 0;
                isActive = false;
                gameObject.tag = "Interactable";
                GetComponent<Collider>().isTrigger = true;
                CmdRemoveCollision();
                if (currentAbility != null)
                {
                    currentAbility.StopAiming();
                }
            }
        }
    }

    public override void LocalTakeHealing(float healAmount)
    {
        if (isActive)
        {
            base.LocalTakeHealing(healAmount);
        }
    }

    [Command]
    public void CmdRemoveCollision()
    {
        RpcRemoveCollision();
        isActive = false;
        if (gameState != null)
        {
            gameState.RemoveLife();
        }
        else
        {
            gameState = GameObject.Find("GameStateController(Clone)").GetComponent<GameStateController>();
            gameState.RemoveLife();
        }
    }

    [ClientRpc]
    public void RpcRemoveCollision()
    {
        GetComponent<Collider>().isTrigger = true;
        gameObject.tag = "Interactable";
        isActive = false;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

    public void Interact()
    {
        if (!isActive)
        {
            isActive = true;
            RpcRespawn();
            GetComponent<Collider>().isTrigger = false;
            health = maxHealth / 2.5F;
            gameObject.tag = "Player";
        }
    }

    [ClientRpc]
    public void RpcRespawn()
    {
        isActive = true;
        GetComponent<Collider>().isTrigger = false;
        health = maxHealth / 2.5F;
        gameObject.tag = "Player";
    }

    [ClientRpc]
    public void RpcCallStart()
    {
        if (isLocalPlayer)
        {
            Start();
            gameObject.tag = "Player";
        }
    }
}
