﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;

public abstract class Ability : NetworkBehaviour {
    public PlayerCharacter character;
    protected bool isAiming = false;

    public float channelTimer = 0;
    public bool isChanneling = false;
    public float channelLength = 0;

    public float cooldownTimer = 0;
    private List<GameObject> lineRenderers = new List<GameObject>();

    public abstract void AbilityUpdate();
    public abstract string GetName();
    public abstract void Interrupt();
    public abstract bool CanCast();
    protected abstract void AbilityTrigger(Vector3 position);
    protected abstract void AbilityAim(Vector3 position);
    public abstract string GetTooltip();

    public void HandleAbilityTrigger()
    {
        if (WillAbilityTrigger())
        {
            AbilityTrigger(InputController.GetMousePosition());
        }
    }
    
    public Ability SetCharacter(PlayerCharacter character)
    {
        this.character = character;
        return this;
    }

    public void StartAiming()
    {
        isAiming = true;
    }

    public void StopAiming()
    {
        isAiming = false;
        StopRendering();
    }

    public float GetCooldownTimeRemaining()
    {
        return cooldownTimer - Time.time;
    }

    public bool WillAbilityTrigger()
    {
        return CanCast() && GetCooldownTimeRemaining() < 0;
    }

    public void AbilityTick()
    {
        if (isAiming)
        {
            AbilityAim(InputController.GetMousePosition());
        }
        AbilityUpdate();
    }

    private void StopRendering()
    {
        foreach (GameObject lineRender in lineRenderers)
        {
            LineRenderer lineRenderer = lineRender.GetComponent<LineRenderer>();
            lineRenderer.enabled = false;
        }
    }

    protected void DrawRange(float range, Vector3 position, int renderIndex) {
        float theta_scale = 0.1F;
        int size = (int)((2.0 * Mathf.PI) / theta_scale) + 3;
        LineRenderer lineRenderer;
        if (lineRenderers.Count < renderIndex)
        {
            lineRenderers.Add(new GameObject());
            lineRenderer = lineRenderers[renderIndex - 1].AddComponent<LineRenderer>();
        }
        else
        {
            lineRenderer = lineRenderers[renderIndex - 1].GetComponent<LineRenderer>();
        }
        lineRenderer.enabled = true;
        //lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
        lineRenderer.SetColors(Color.green, Color.green);
        lineRenderer.SetWidth(0.1F, 0.1F);
        lineRenderer.SetVertexCount(size);

        float theta = 0F;
        for(int i = 0; i < size; i++) {
            float x = range * Mathf.Cos(theta);
            float z = range * Mathf.Sin(theta);

            Vector3 pos = new Vector3(x + position.x, 0, z + position.z);
            lineRenderer.SetPosition(i, pos);
            theta += 0.1F;
        }
    }

    public virtual string ExtraRenderInfo()
    {
        return "";
    }

    [Command]
    public void CmdGenerateThreat(float threatAmount, string dealer, GameObject boss)
    {
        if (boss != null)
        {
            boss.GetComponent<Boss>().GenerateThreat(threatAmount, dealer);
        }
    }
}
