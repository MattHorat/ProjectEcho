﻿using UnityEngine;
using UnityEngine.Networking;

/*
 * This class needs to store all the properties and methods that abilities and the UI class
 * will need to display buffs and debuffs. It should handle TTL.
 */
public abstract class StatusEffect : NetworkBehaviour
{
    protected Character character;
    protected float timer;
    protected string title;
    public float displayNumber;
    public bool readyToDestroy = false;

    public void SetCharacter(Character character)
    {
        this.character = character;
    }

    public void SetTitle(string title)
    {
        this.title = title;
    }

    public string GetTitle()
    {
        return title;
    }

    public abstract string GetTooltip();

    public abstract void Tick();

    public virtual void AddNumber(float number) {
        displayNumber += number;
    }
}
