﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class JarOfSnakes : Ability {
    private static float resourceCost = 15;
    private static float range = 10;
    public static float damage = 540;
    public static float damageTimeLength = 6;
    public static float spawnLength = 12;
    
    public GameObject snakesPrefab;
    public static string AbilityName = "Jar of Snakes";
    
    public override string GetName()
    {
        return AbilityName;
    }

	public override string GetTooltip()
	{
		return "<color=black>Cost - " + resourceCost + "		       <i>Instant</i>\nThrows a jar at the targeted area causing snakes to burst forth "
			+ "poisoning anyone in the area for <b><color=maroon>" + damage + "</color></b> damage over " + damageTimeLength + " seconds. The snakes linger for "
			+ spawnLength + " seconds biting anyone who enters the area.</color>";
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        if (Vector3.Distance(character.transform.position, position) < range)
        {
            character.resource -= resourceCost;
            CmdSpawnJar(position, character.gameObject);
        }
    }

    [Command]
    public void CmdSpawnJar(Vector3 targetPosition, GameObject creator)
    {
        GameObject jar = (GameObject)Instantiate(snakesPrefab, targetPosition, Quaternion.identity);
        NetworkServer.Spawn(jar);
        jar.GetComponent<SnakesAOE>().creator = creator.GetComponent<Character>();
    }

    protected override void AbilityAim(Vector3 position)
    {
        DrawRange(range, character.transform.position, 1);
        DrawRange(1, position, 2);
    }

    public override void AbilityUpdate()
    {
    }

    public override void Interrupt()
    {
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed && character.resource > resourceCost;
    }
}
