﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class PogiStatue : Ability {
    private static float resourceCost = 15;
    private static float cooldown = 20;
    private static float channelTime = 1.2F;

    public GameObject statuePrefab;
    public GameObject statue;
    private Vector3 targetPosition;
    [SyncVar]
    private Vector3 statuePosition;
    [SyncVar]
    private bool statueSpawned = false;

    public void Start()
    {
        channelLength = channelTime;
    }
    
    public override string GetName()
    {
        return "Pogi Statue";
    }

	public override string GetTooltip()
	{
        string toolTip;
        if (!statueSpawned)
        {
            toolTip = "<color=black>Cost - " + resourceCost + "		     <i>1.2 sec cast</i>\nPlaces a miniature Pogi statue on the ground that Pogi can switch places with.</color>";
        }
        else
        {
            toolTip = "<color=black>Cost - 0		     <i>Instant</i>\nSwitch places with your statue.</color>";
        }
		return toolTip;
	}


    protected override void AbilityTrigger(Vector3 position)
    {
        if (!statueSpawned)
        {
            channelTimer = Time.time + channelLength;
            targetPosition = position;
            isChanneling = true;
        }
        else
        {
            cooldownTimer = Time.time + cooldown;
            Vector3 currentPosition = character.transform.position;
            character.transform.position = statuePosition;
            statuePosition = currentPosition;
            CmdMoveStatue(currentPosition);
        }
    }

    [Command]
    public void CmdSpawnStatue(Vector3 position)
    {
        if (!statueSpawned)
        {
            statue = (GameObject)Instantiate(statuePrefab, position, Quaternion.identity);
            statuePosition = position;
            NetworkServer.Spawn(statue);
            statueSpawned = true;
        }
    }

    [Command]
    public void CmdMoveStatue(Vector3 position)
    {
        statue.transform.position = position;
    }

    protected override void AbilityAim(Vector3 position)
    {
        if (!statueSpawned)
        {
            DrawRange(1, position, 1);
        }
    }

    public override void AbilityUpdate()
    {
        if (isChanneling && Time.time - channelTimer > 0)
        {
            CmdSpawnStatue(targetPosition);
            isChanneling = false;
            character.resource -= resourceCost;
        }
    }

    public override void Interrupt()
    {
        isChanneling = false;
    }

    public override bool CanCast()
    {
        if (!statueSpawned)
        {
            return character.isMovementAllowed && character.resource > resourceCost;
        }
        else
        {
            return character.isMovementAllowed;
        }
    }
}
