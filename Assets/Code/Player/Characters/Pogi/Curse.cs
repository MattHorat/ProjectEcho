﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class Curse : Ability {
    private static float channelTime = .7F;
    private static float resourceCost = 13;
    private static float range = 8;
    public static float spawnSpeed = 9;
    public static float damage = 100;
    public static float damageOverTime = 1600;
    public static float damageTime = 10;

    public static string AbilityName = "Curse";
    
    public GameObject cursePrefab;
    private Vector3 targetPosition;

    public void Start()
    {
        channelLength = channelTime;
    }
    
    public override string GetName()
    {
        return AbilityName;
    }

	public override string GetTooltip()
	{
		return "<color=black>Cost - " + resourceCost + "		    <i>" + channelTime + " sec cast</i>\nLaunches a spirit forward that deals "
			+ "<b><color=maroon>" + damage + "</color></b> initial damage and <b><color=maroon>" + damageOverTime + "</color></b> damage over " + damageTime + " seconds.</color>";
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        channelTimer = Time.time + channelLength;
        character.resource -= resourceCost;
        targetPosition = position;
        isChanneling = true;
    }

    public override void AbilityUpdate()
    {
        if (isChanneling && Time.time - channelTimer > 0)
        {
            Vector3 aimingPosition = (targetPosition - character.transform.position).normalized * range;
            CmdSpawnCurse(aimingPosition, character.gameObject);
            isChanneling = false;
        }
    }

    [Command]
    private void CmdSpawnCurse(Vector3 aimingPosition, GameObject character)
    {
        GameObject curseBullet = (GameObject)Instantiate(cursePrefab, character.transform.position + aimingPosition.normalized / 2, Quaternion.identity);
        NetworkServer.Spawn(curseBullet);

        curseBullet.GetComponent<CurseBullet>().ShootAt(character.transform.position + aimingPosition);
        curseBullet.GetComponent<CurseBullet>().creator = character.GetComponent<Character>();
    }

    protected override void AbilityAim(Vector3 position)
    {
        Vector3 aimingPosition = (position - character.transform.position).normalized * range;
        Debug.DrawLine(character.transform.position, character.transform.position + aimingPosition, Color.green);
    }

    public override void Interrupt()
    {
        isChanneling = false;
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed && character.resource > resourceCost && !character.IsMoving();
    }
}
