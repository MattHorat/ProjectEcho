﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class MojoMissile : Ability {
    private static float range = 7;
    private static float resourceCost = 8;
    private static float chargeCooldown = 5;
    public static float spawnSpeed = 9;
    public static float damageAmount = 200;
    public static float dotMultiplier = 50;

    private int charges = 3;

    public GameObject missilePrefab;
    
    public override string GetName()
    {
        return "Mojo Missile";
    }

	public override string GetTooltip()
	{
		return "<color=black>Cost - " + resourceCost + "		         <i>Instant</i>\nFires a missile that deals "
			+ "<b><color=maroon>" + damageAmount + "</color></b> damage to the first enemy target hit. Deals "
			+ "<b><color=maroon>" + dotMultiplier + "%</color></b> more damage for every DoT already on the target.\n<i>" + charges + " charges</i></color>";
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        Vector3 aimingPosition = (position - character.transform.position).normalized * range;
        charges--;
        character.resource -= resourceCost;
        CmdSpawnMojoMissile(aimingPosition, character.gameObject);
        character.StartCoroutine(AddCharge(chargeCooldown));
    }

    [Command]
    public void CmdSpawnMojoMissile(Vector3 aimingPosition, GameObject creator)
    {
        GameObject missileBullet = (GameObject)Instantiate(missilePrefab, creator.transform.position, Quaternion.identity);
        NetworkServer.Spawn(missileBullet);
        missileBullet.GetComponent<MojoMissileBullet>().ShootAt(creator.transform.position + aimingPosition);
        missileBullet.GetComponent<MojoMissileBullet>().creator = creator;
    }

    protected override void AbilityAim(Vector3 position)
    {
        Vector3 aimingPosition = (position - character.transform.position).normalized * range;
        Debug.DrawLine(character.transform.position, character.transform.position + aimingPosition, Color.green);
    }

    public override void AbilityUpdate()
    {
    }

    private IEnumerator AddCharge(float yieldTime)
    {
        yield return new WaitForSeconds(yieldTime);
        charges++;
    }

    public override void Interrupt()
    {
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed && charges > 0 && character.resource >= resourceCost;
    }
}
