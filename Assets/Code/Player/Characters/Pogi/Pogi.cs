﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Pogi : PlayerCharacter {
    public override void Start()
    {
        if (isLocalPlayer)
        {
            maxHealth = 3000;
            health = maxHealth;
            speed = 8F;
            resource = 0;
            resourceMax = 100;
            abilities = new List<Ability>();
            abilities.Add(gameObject.GetComponent<DrainEnergy>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<Curse>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<FlamingZombie>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<JarOfSnakes>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<MojoMissile>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<PogiStatue>().SetCharacter(this));
            base.Start();
        }
        characterName = "Pogi";
    }
}
