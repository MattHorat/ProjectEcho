﻿using UnityEngine;
using System.Collections;
using System;

public class CurseBullet : AICharacter {
    public Character creator;

    public override void Update()
    {
        if (isServer)
        {
            base.Update();
            if (!isMoving)
            {
                Destroy(gameObject);
            }
        }
    }

    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            speed = Curse.spawnSpeed;
            isMoving = true;
        }
    }

    public void ShootAt(Vector3 target)
    {
        MoveTo(target);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            creator.CauseDamage(collision.gameObject.GetComponent<Character>(), Curse.damage, "Curse");
            creator.CmdCauseDamageOverTime(collision.gameObject, Curse.damageOverTime, Curse.damageTime, Curse.AbilityName);
            Destroy(gameObject);
        }
    }
}
