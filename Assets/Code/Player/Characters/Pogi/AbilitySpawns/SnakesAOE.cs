﻿using UnityEngine;
using System.Collections;
using System;
using System.Timers;
using UnityEngine.Networking;

public class SnakesAOE : NetworkBehaviour {
    public Character creator;

    public void Start()
    {
        if (isServer)
        {
            StartCoroutine(DestroyThis());
        }
    }

    private IEnumerator DestroyThis()
    {
        yield return new WaitForSeconds(JarOfSnakes.spawnLength);
        Destroy(gameObject);
    }

    void OnTriggerStay(Collider collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            creator.CmdCauseDamageOverTime(collision.gameObject, JarOfSnakes.damage, JarOfSnakes.damageTimeLength, JarOfSnakes.AbilityName);
        }
    }
}
