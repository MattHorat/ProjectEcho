﻿using UnityEngine;
using System.Collections;
using System;

public class Zombie : AICharacter {
    public Character creator;

    private GameObject targetCharacter;
    public override void Update()
    {
        if (isServer)
        {
            base.Update();
            if (targetCharacter != null)
            {
                MoveTo(targetCharacter.transform.position);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }

    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            speed = FlamingZombie.zombieSpeed;
            isMoving = true;
            StartCoroutine(DestroyThis());
        }
    }

    public void ShootAt(GameObject target)
    {
        targetCharacter = target;
        MoveTo(targetCharacter.transform.position);
    }

    void OnTriggerStay(Collider collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, FlamingZombie.damageRadius);
            foreach (Collider collider in hitColliders)
            {
                if (collider.CompareTag("Enemy"))
                {
                    creator.CauseDamage(collider.gameObject.GetComponent<Character>(), FlamingZombie.damageAmount, "Flaming Zombie");
                }
            }
            Destroy(gameObject);
        }
    }

    private IEnumerator DestroyThis()
    {
        yield return new WaitForSeconds(FlamingZombie.spawnLength);
        Destroy(gameObject);
    }

    public override void TakeDamage(float damageAmount, Character damageDealer, string moveName = null)
    {
    }
}
