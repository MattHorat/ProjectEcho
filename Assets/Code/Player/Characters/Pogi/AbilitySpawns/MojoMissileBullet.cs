﻿using UnityEngine;
using System.Collections;
using System;

public class MojoMissileBullet : AICharacter {
    public GameObject creator;

    public override void Update()
    {
        if (isServer)
        {
            base.Update();
            if (!isMoving)
            {
                Destroy(gameObject);
            }
        }
    }

    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            speed = MojoMissile.spawnSpeed;
            isMoving = true;
        }
    }

    public void ShootAt(Vector3 target)
    {
        MoveTo(target);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Character enemy = collision.gameObject.GetComponent<Character>();
            int dotCount = 0;
            if (enemy.IsDoTApplied(Curse.AbilityName)) { dotCount++; }
            if (enemy.IsDoTApplied(JarOfSnakes.AbilityName)) { dotCount++; }
            creator.GetComponent<Character>().CauseDamage(enemy, MojoMissile.damageAmount + MojoMissile.dotMultiplier * dotCount, "Mojo Missile");
            Destroy(gameObject);
        }
    }
}
