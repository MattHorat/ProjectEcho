﻿using UnityEngine;
using System.Collections;
using System;

public class DrainEnergy : Ability {
    private float range = 6;
    private float energyRegen = 12;
    private float damageAmount = 45;

    private Vector3 firingPosition;
    
    public override string GetName()
    {
        return "Drain Energy";
    }

	public override string GetTooltip()
	{
		return "<color=black>Gen - " + energyRegen + " per sec	      <i>Channelled</i>\nShoots a beam that deals <b><color=maroon>" + damageAmount + "</color></b> damage per second to the first enemy target hit.</color>";
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        isChanneling = true;
        firingPosition = character.transform.forward;
        firingPosition.y = 0;
        character.DisallowCasts();
    }

    public override void AbilityUpdate()
    {
        if (isChanneling)
        {
            RaycastHit hit;
            Ray ray;
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                firingPosition = Vector3.RotateTowards(firingPosition, character.gameObject.transform.forward, .03F, 1F);
                firingPosition.y = 0;
            }

            Debug.DrawLine(character.gameObject.transform.position, character.gameObject.transform.position + firingPosition * range, Color.green);
            RaycastHit hitObject;
            int layerMask = ~(1 << 8);
            if (Physics.Raycast(character.gameObject.transform.position, firingPosition, out hitObject, range, layerMask))
            {
                if (hitObject.transform.gameObject != null)
                {
                    if (hitObject.transform.gameObject.CompareTag("Enemy"))
                    {
                        character.resource += energyRegen * Time.deltaTime;
                        if (character.resource > character.resourceMax)
                        {
                            character.resource = character.resourceMax;
                        }
                        Character boss = hitObject.transform.gameObject.GetComponent<Character>();
                        if (boss)
                        {
                            character.CauseDamage(boss, damageAmount * Time.deltaTime, GetName());
                        }
                    }
                }
            }
        }
    }

    protected override void AbilityAim(Vector3 position)
    {
        firingPosition = character.gameObject.transform.forward;
        Debug.DrawLine(character.gameObject.transform.position, character.gameObject.transform.position + firingPosition * range, Color.red);
    }

    public override void Interrupt()
    {
        isChanneling = false;
        character.AllowCasts();
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed && !character.IsMoving();
    }
}
