﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class FlamingZombie : Ability {
    private static float resourceCost = 18;
    private static float castTime = 1;
    public static float damageAmount = 525;
    public static float damageRadius = 4;
    public static float spawnLength = 3;
    public static float zombieSpeed = 9;

    public GameObject zombiePrefab;

    public void Start()
    {
        channelLength = castTime;
    }
    
    public override string GetName()
    {
        return "Flaming Zombie";
    }

	public override string GetTooltip()
	{
		return "<color=black>Cost - " + resourceCost + "		    <i>" + castTime + " sec cast</i>\nSummons a flaming zombie that runs at the closest enemy and explodes for "
			+ "<b><color=maroon>" + damageAmount + "</color></b> damage in a small radius.</color>";
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        channelTimer = Time.time + channelLength;
        character.resource -= resourceCost;
        isChanneling = true;
    }

    public override void AbilityUpdate()
    {
        if (isChanneling && Time.time - channelTimer > 0)
        {
            CmdSpawnZombies(FindClosestEnemy(), character.gameObject);
            isChanneling = false;
        }
    }

    [Command]
    private void CmdSpawnZombies(GameObject target, GameObject spawnChar)
    {
        GameObject flamingZombie = (GameObject)Instantiate(zombiePrefab, spawnChar.transform.position, Quaternion.identity);
        NetworkServer.Spawn(flamingZombie);
        flamingZombie.GetComponent<Zombie>().ShootAt(target);
        flamingZombie.GetComponent<Zombie>().creator = spawnChar.GetComponent<Character>();
    }

    private GameObject FindClosestEnemy()
    {
        GameObject closestEnemy = null;
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject enemy in enemies)
        {
            if (closestEnemy == null)
            {
                closestEnemy = enemy;
            }
            else if (Vector3.Distance(enemy.transform.position, character.transform.position) < Vector3.Distance(closestEnemy.transform.position, character.transform.position))
            {
                closestEnemy = enemy;
            }
        }
        return closestEnemy;
    }

    protected override void AbilityAim(Vector3 position)
    {
        DrawRange(spawnLength * zombieSpeed / 2, character.transform.position, 1);
    }

    public override void Interrupt()
    {
        isChanneling = false;
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed && character.resource > resourceCost && !character.IsMoving();
    }
}
