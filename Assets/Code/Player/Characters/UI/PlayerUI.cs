﻿using UnityEngine;
using System.Collections;
using System.Timers;
using System.Collections.Generic;
using System;
using UnityEngine.Networking;

public class PlayerUI : NetworkBehaviour {
    public PlayerCharacter character;
    private Vector2 pos = new Vector2(0.1F, 0.1F);
    private Vector2 size = new Vector2(0.1F, 0.1F);
    public Texture2D emptyTex;
    public Texture2D fullTex;
    private Vector2 resourcePos = new Vector2(0.8F, 0.1F);
    private Vector2 resourceSize = new Vector2(0.1F, 0.1F);
    private Vector2 actionBarPos = new Vector2(0.1F, 0.8F);
    private Vector2 statusBarPos = new Vector2(0.1F, 0.7F);
    public Texture2D resourceEmptyTex;
    public Texture2D resourceFullTex;
    private GUIStyle tooltipStyle = new GUIStyle();

    private bool flashed = false;
    public CanvasGroup canvasGroup;

    void Start()
    {
        canvasGroup = GameObject.Find("Canvas_Flash").GetComponent<CanvasGroup>();
    }

    void OnGUI()
    {
        if (isLocalPlayer)
        {
            DrawHealthBar();
            DrawResourceBar();
            DrawSkillBar();
            DrawChannelBar();
            DrawStatusBar();
        }
    }

    private void DrawHealthBar()
    {
        //draw the background:
        GUI.BeginGroup(CreateRelativeRect(pos.x, pos.y, size.x + .3F, size.y + .3F));
        GUI.Box(CreateRelativeRect(0, 0, size.x, size.y), emptyTex);

        //draw the filled-in part:
        GUI.BeginGroup(CreateRelativeRect(0, 0, size.x, size.y * (1 - character.health / character.maxHealth)));
        GUI.Box(CreateRelativeRect(0, 0, size.x, size.y), fullTex);
        GUI.EndGroup();
        GUI.EndGroup();
    }

    private void DrawResourceBar()
    {
        //draw the background:
        GUI.BeginGroup(CreateRelativeRect(resourcePos.x, resourcePos.y, resourceSize.x, resourceSize.y));
        GUI.Box(CreateRelativeRect(0, 0, resourceSize.x, resourceSize.y), resourceEmptyTex);

        //draw the filled-in part:
        GUI.BeginGroup(CreateRelativeRect(0, 0, resourceSize.x, resourceSize.y * (1 - character.resource / character.resourceMax)));
        GUI.Box(CreateRelativeRect(0, 0, resourceSize.x, resourceSize.y), resourceFullTex);
        GUI.EndGroup();
        GUI.EndGroup();
    }

    private void DrawSkillBar()
    {
        float xPos = 0;
        float xTooltipPos = -1;
        foreach (Ability ability in character.GetAbilities())
        {
            string abilityDisplay = ability.GetName();
            if (!ability.WillAbilityTrigger())
            {
                abilityDisplay = "Disabled! \n" + abilityDisplay + "\n";
                float cooldownTimer =  ability.GetCooldownTimeRemaining();
                if (cooldownTimer > 0)
                {
                    abilityDisplay = abilityDisplay + Math.Round(cooldownTimer, 1);
                }
            }
            GUI.Box(CreateRelativeRect(actionBarPos.x + xPos, actionBarPos.y, .1F, .1F), new GUIContent(abilityDisplay, ability.GetTooltip()));
            GUI.Label(CreateRelativeRect(actionBarPos.x + xPos, actionBarPos.y - .05F, .1F, .1F), ability.ExtraRenderInfo());
            if (GUI.tooltip != "" && xTooltipPos == -1)
            {
                xTooltipPos = xPos;
            }
            xPos += .1F;
        }
        if (GUI.tooltip != "")
        {
            GUI.Box(CreateRelativeRect(actionBarPos.x + xTooltipPos - .055F, actionBarPos.y - .105F, .22F, .22F), "", tooltipStyle);
            GUI.Label(CreateRelativeRect(actionBarPos.x + xTooltipPos - .05F, actionBarPos.y - .1F, .2F, .2F), GUI.tooltip);
        }
        GUI.tooltip = null;
        xTooltipPos = 0;
    }

    private void DrawChannelBar()
    {
        foreach (Ability ability in character.GetAbilities())
        {
            if (ability.isChanneling)
            {
                DrawIndividualChannel(ability);
            }
        }
        if (character.respawnAbility.isChanneling)
        {
            DrawIndividualChannel(character.respawnAbility);
        }
    }

    private void DrawIndividualChannel(Ability ability)
    {
        if (ability.channelTimer > Time.time)
        {
            GUI.BeginGroup(CreateRelativeRect(.4F, .7F, .2F, .1F));
            GUI.Box(CreateRelativeRect(0, 0, .2F, .1F), resourceEmptyTex);
            GUI.BeginGroup(CreateRelativeRect(0, 0, .2F * (ability.channelTimer - Time.time) / ability.channelLength, .1F));
            GUI.Box(CreateRelativeRect(0, 0, .2F, .1F), resourceFullTex);
            GUI.EndGroup();
            GUI.EndGroup();
        }
        else
        {
            GUI.Box(CreateRelativeRect(.4F, .7F, .2F, .1F), "Channeling");
        }
    }

    private Rect CreateRelativeRect(float positionX, float positionY, float width, float height)
    {
        return new Rect(Screen.width * positionX, Screen.height * positionY, Screen.width * width, Screen.height * height);
    }

    private void DrawStatusBar()
    {
        float xPos = 0;
        float xTooltipPos = -1;
        foreach (StatusEffect statusEffect in character.GetStatusEffects())
        {
            //string title = statusEffect.GetTitle() + "\n" + Math.Round(statusEffect.displayNumber);
            string title = Math.Round(statusEffect.displayNumber) + "";
            GUI.Box(CreateRelativeRect(statusBarPos.x + xPos, statusBarPos.y, .05F, .05F), new GUIContent(title, statusEffect.GetTooltip()));
            //GUI.Label(CreateRelativeRect(statusBarPos.x + xPos, statusBarPos.y - .05F, .05F, .05F), statusEffect.displayNumber + "");
            if (GUI.tooltip != "" && xTooltipPos == -1)
            {
                xTooltipPos = xPos;
            }
            xPos += .05F;
        }
        if (GUI.tooltip != "")
        {
            GUI.Box(CreateRelativeRect(statusBarPos.x + xTooltipPos - .055F, statusBarPos.y - .105F, .22F, .22F), "", tooltipStyle);
            GUI.Label(CreateRelativeRect(statusBarPos.x + xTooltipPos - .05F, statusBarPos.y - .1F, .2F, .2F), GUI.tooltip);
        }
        GUI.tooltip = null;
        xTooltipPos = 0;
    }

    void Update()
    {
        if (flashed)
        {
            canvasGroup.alpha = canvasGroup.alpha - (Time.deltaTime / 2);
            if (canvasGroup.alpha <= 0)
            {
                canvasGroup.alpha = 0;
                flashed = false;
            }
        }
    }

    public void CauseFlash()
    {
        canvasGroup.alpha = 1;
        flashed = true;
    }
}
