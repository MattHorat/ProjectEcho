﻿using UnityEngine;
using System.Collections;
using System.Timers;
using System.Collections.Generic;

public class DamageLog : MonoBehaviour {
    private Dictionary<string, float> damageAmounts = new Dictionary<string, float>();

    public void AddDamageAmount(string abilityName, float damageAmount)
    {
        if (damageAmounts.ContainsKey(abilityName))
        {
            damageAmounts[abilityName] += damageAmount;
        }
        else
        {
            damageAmounts.Add(abilityName, damageAmount);
        }
    }

    public void OutputDamageAmounts()
    {
        foreach (KeyValuePair<string, float> damageAmount in damageAmounts)
        {
            Debug.Log("Move: " + damageAmount.Key + " Damage: " + damageAmount.Value);
        }
    }
}
