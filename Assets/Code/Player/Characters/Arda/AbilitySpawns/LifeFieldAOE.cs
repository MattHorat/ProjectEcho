﻿using UnityEngine;
using System.Collections;
using System;
using System.Timers;
using System.Collections.Generic;
using UnityEngine.Networking;

public class LifeFieldAOE : NetworkBehaviour {
    public Character creator;

    private float pulseTimer = -999999;
    private List<GameObject> enemies = new List<GameObject>();
    private List<GameObject> players = new List<GameObject>();
    
    public void Start()
    {
        if (isServer)
        {
            StartCoroutine(DestroyThis());
        }
    }

    private IEnumerator DestroyThis()
    {
        yield return new WaitForSeconds(LifeField.fieldLength);
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Enemy"))
        {
            enemies.Add(collider.gameObject);
        }
        else if (collider.gameObject.CompareTag("Player"))
        {
            players.Add(collider.gameObject);
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.CompareTag("Enemy"))
        {
            enemies.Remove(collider.gameObject);
        }
        else if (collider.gameObject.CompareTag("Player"))
        {
            players.Remove(collider.gameObject);
        }
    }
    
    void OnTriggerStay(Collider collision)
    {
        if (isServer)
        {
            if (Time.time - pulseTimer > LifeField.tickFrequency)
            {
                foreach (GameObject enemy in enemies)
                {
                    if (enemy != null)
                    {
                        creator.CauseDamage(enemy.GetComponent<Character>(), LifeField.damageTickAmount, "Life Field");
                        enemy.GetComponent<Boss>().GenerateThreat(LifeField.extraThreatAmount, "Arda");
                    }
                }
                foreach (GameObject player in players)
                {
                    player.GetComponent<Character>().HealDamage(LifeField.healTickAmount);
                }
                pulseTimer = Time.time;
            }
        }
    }
}
