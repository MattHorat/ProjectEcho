﻿using UnityEngine;
using System.Collections;
using System;

//Arda burns the target inflicting a dot on them, every tick of the dot causes Arda to regain a small portion of his health. 
public class FlameStrike : Ability {
    private float range = 3;
    private float angleOffset = 35;
    private float damage = 400;
    private float resourceCost = 5;
    private float cooldown = 5;

    public override string GetName()
    {
        return "Flame Strike";
    }

	public override string GetTooltip()
	{
		return "<color=black>Cost - " + resourceCost + "		           <i>Instant</i>\nStrike the target inflicting "
			+ "<b><color=maroon>" + damage + "</color></b> damage and taunts the target.\n<i>" + cooldown + " sec cooldown</i></color>";
	}
    
    protected override void AbilityTrigger(Vector3 position)
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        Character closestEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            if (Vector3.Distance(character.transform.position, enemy.transform.position) < (range + .5))
            {
                float cone = Mathf.Cos(angleOffset * Mathf.Deg2Rad);
                Vector3 heading = (enemy.transform.position - character.transform.position).normalized;

                if (Vector3.Dot(character.transform.forward, heading) > cone)
                {
                    if (closestEnemy == null || Vector3.Distance(character.transform.position, enemy.transform.position) < Vector3.Distance(character.transform.position, closestEnemy.transform.position))
                    {
                        closestEnemy = enemy.GetComponent<Character>();
                    }
                }
            }
        }
        if (closestEnemy != null)
        {
            closestEnemy.TakeTaunt("Arda");
            character.CauseDamage(closestEnemy, damage, GetName());
        };
        character.resource -= resourceCost;
        cooldownTimer = Time.time + cooldown;
        isAiming = false;
    }

    protected override void AbilityAim(Vector3 position)
    {
        Transform newRotation = character.transform;
        newRotation.Rotate(Vector3.up, angleOffset);
        Debug.DrawLine(character.transform.position, character.transform.position + newRotation.forward * range, Color.green);
        newRotation.Rotate(Vector3.up, -2 * angleOffset);
        Debug.DrawLine(character.transform.position, character.transform.position + newRotation.forward * range, Color.green);
    }

    public override void AbilityUpdate()
    {
    }

    public override void Interrupt()
    {
    }

    public override bool CanCast()
    {
        return character.resource >= resourceCost;
    }
}
