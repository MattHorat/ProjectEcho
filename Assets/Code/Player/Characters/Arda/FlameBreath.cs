﻿using UnityEngine;
using System.Collections;
using System;


/* 
 * Arda blows fire in a frontal cone for up to 3 seconds, drains candle energy while you hold the ability.
 * Arda heals for 10% of the damage done over the next 10 seconds.
 */
public class FlameBreath : Ability {
    private static float channelTime = 3;
    private float range = 2.5F;
    private float angleOffset = 35;
    private float healFactor = 50;
    private float healLength = 8;
    private float damagePerSecond = 300;
    private float resourceUse = 20;
    
    private float damageDone = 0;

    public void Start()
    {
        channelLength = channelTime;
    }

    public override string GetName()
    {
        return "Flame Breath";
    }

	public override string GetTooltip()
	{
		return "<color=black>Cost - 20 per sec	<i>5 sec channel</i>\nDeal <b><color=maroon>80</color></b> damage per sec to all targets in a small cone. "
			+ "At the end of the channel heal for <b><color=green>" + healFactor + "%</color></b> of the damage done over " + healLength + " seconds.</color>";
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        channelTimer = Time.time + channelLength;
        isChanneling = true;
        damageDone = 0;
    }

    public override void AbilityUpdate()
    {
        if (character.resource < 1 || Time.time - channelTimer > 0)
        {
            Interrupt();
        }
        if (isChanneling)
        {
            character.resource -= resourceUse * Time.deltaTime;
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (GameObject enemy in enemies)
            {
                if (Vector3.Distance(character.transform.position, enemy.transform.position) < range)
                {
                    float cone = Mathf.Cos(angleOffset * Mathf.Deg2Rad);
                    Vector3 heading = (enemy.transform.position - character.transform.position).normalized;

                    if (Vector3.Dot(character.transform.forward, heading) > cone)
                    {
                        if (enemy.gameObject.GetComponent<Boss>() != null)
                        {
                            CmdGenerateThreat(1, "Arda", enemy.gameObject);
                        }
                        character.CauseDamage(enemy.GetComponent<Character>(), damagePerSecond * Time.deltaTime, GetName());
                        damageDone += 1;
                    }
                }
            }
            Transform newRotation = character.transform;
            newRotation.Rotate(Vector3.up, angleOffset);
            Debug.DrawLine(character.transform.position, character.transform.position + newRotation.forward * range, Color.red);
            newRotation.Rotate(Vector3.up, -2 * angleOffset);
            Debug.DrawLine(character.transform.position, character.transform.position + newRotation.forward * range, Color.red);
        }
    }

    protected override void AbilityAim(Vector3 position)
    {
        Transform newRotation = character.transform;
        newRotation.Rotate(Vector3.up, 35);
        Debug.DrawLine(character.transform.position, character.transform.position + newRotation.forward * range, Color.green);
        newRotation.Rotate(Vector3.up, -70);
        Debug.DrawLine(character.transform.position, character.transform.position + newRotation.forward * range, Color.green);
    }

    public override void Interrupt()
    {
        if (isChanneling)
        {
            isChanneling = false;
            character.HealOverTime(damageDone * (healFactor / 100), healLength);
            damageDone = 0;
        }
    }

    public override bool CanCast()
    {
        return !character.IsMoving() && character.resource >= 1 && character.isMovementAllowed && !isChanneling;
    }
}
