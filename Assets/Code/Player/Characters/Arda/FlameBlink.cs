﻿using UnityEngine;
using System.Collections;
using System;


/* 
 * Arda disappears from view and then quickly reappears a certain distance in front of where he was.
 * As he appears he deals a large amount of damage in a small AoE and a large amount of threat.
 */
public class FlameBlink : Ability {
    private float damage = 500;
    private float extraThreat = 500;
    private float cooldown = 12;
    private float teleportRange = 5;
    private float damageRange = 2;
    private float resourceCost = 15;


    public override string GetName()
    {
        return "Flame Blink";
    }

	public override string GetTooltip()
	{
		return "<color=black>Cost - " + resourceCost + "		           <i>Instant</i>\nTeleport a small distance dealing "
            + "<b><color=maroon>" + damage + "</color></b> damage to all targets in a small radius and causing a large amount of threat.\n<i>"
            + cooldown + " sec cooldown</i></color>";
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        float distance = (position - character.transform.position).magnitude;
        if (distance > teleportRange)
        {
            distance = teleportRange;
        }
        Vector3 newPos = character.transform.position + character.transform.forward * distance;
        newPos.y = 0;
        character.transform.position = Vector3.MoveTowards(character.transform.position, position, distance);
            
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject enemy in enemies)
        {
            if (Vector3.Distance(character.transform.position, enemy.transform.position) < damageRange)
            {
                character.CauseDamage(enemy.GetComponent<Character>(), damage, GetName());
                if (enemy.gameObject.GetComponent<Boss>() != null)
                {
                    CmdGenerateThreat(extraThreat, "Arda", enemy.gameObject);
                }
            }
        }

        cooldownTimer = Time.time + cooldown;
        character.resource -= resourceCost;
    }

    protected override void AbilityAim(Vector3 position)
    {
        DrawRange(teleportRange, character.transform.position, 1);
        DrawRange(damageRange, position, 2);
    }

    public override void AbilityUpdate()
    {
    }

    public override void Interrupt()
    {
    }

    public override bool CanCast()
    {
        return character.resource > resourceCost && character.isMovementAllowed;
    }
}
