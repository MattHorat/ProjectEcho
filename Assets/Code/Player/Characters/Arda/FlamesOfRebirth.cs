﻿using UnityEngine;
using System.Collections;
using System;

public class FlamesOfRebirth : Ability {
    private static float abilityLength = 8;
    private static float cooldownTime = 15;
    private static float healIncrease = 20;
    
    private float healTotal = 0;
    private bool isActive = false;
    private bool isStored = false;
    private float activeTimer = 0;

    public override string GetTooltip()
    {
		return "<color=black>Cost - free		     <i>Instant</i>\nFor the next " + abilityLength + "seconds any self healing done is now saved for "
			+ "later use and has its strength increased by <color=green><b>" + healIncrease + "%</b></color>. Using this ability again will restore the health saved and <b><color=blue>100</color></b> "
			+ "flame energy over 10 seconds.\n<i>" + cooldownTime + " sec cooldown.</i></color>";
    }

    public override string GetName()
    {
		return "Flames of Rebirth";
    }
    
    protected override void AbilityTrigger(Vector3 position)
    {
        if (isStored)
        {
            isActive = false;
            isStored = false;
            cooldownTimer = Time.time + cooldownTime;
            ((Arda)character).isStoringHeal = false;
            character.HealDamage(healTotal / 100 * (100 + healIncrease));
            character.resource += character.resourceMax / 2;
            if (character.resource > character.resourceMax)
            {
                character.resource = character.resourceMax;
            }
        }
        else
        {
            ((Arda)character).healStorer = this;
            ((Arda)character).isStoringHeal = true;
            healTotal = 0;
            isActive = true;
            isStored = true;
            activeTimer = Time.time + abilityLength;
        }
    }

    protected override void AbilityAim(Vector3 position)
    {
    }

    public override void AbilityUpdate()
    {
        if (isActive && isStored && Time.time > activeTimer)
        {
            isActive = false;
            ((Arda)character).isStoringHeal = false;
        }
    }

    public override void Interrupt()
    {
    }

    public override string ExtraRenderInfo()
    {
        if (isStored) 
        {
            return "" + healTotal / 100 * (100 + healIncrease);
        }
        else
        {
            return "";
        }
    }

    public override bool CanCast()
    {
        return true;
    }

    public void StoreHeal(float healAmount) 
    {
        healTotal += healAmount;
    }
}
