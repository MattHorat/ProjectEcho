﻿using UnityEngine;
using System.Collections;
using System;

//Arda burns the target inflicting a dot on them, every tick of the dot causes Arda to regain a small portion of his health. 
public class Burn : Ability {
    private float angleOffset = 35;
    private float range = 2;
    private float damageAmount = 600;
    private float damageTimer = 8;
    private float resourceCost = 10;
    private float cooldown = 5;
    private float healAmount = 600;
    private float healTime = 8;

    public override string GetName()
    {
        return "Burn";
    }

	public override string GetTooltip()
	{
		return "<color=black>Cost - " + resourceCost + "		           <i>Instant</i>\nBurn the target causing "
			+ "<b><color=maroon>" + damageAmount + "</color></b> damage over " + damageTimer + " seconds and healing for <b><color=green>" + healAmount + "</color></b> health over " + healTime + " seconds.\n"
			+ "<i>" + cooldown + " sec cooldown</i></color>";
	}
    
    protected override void AbilityTrigger(Vector3 position)
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject enemy in enemies)
        {
            if (Vector3.Distance(character.transform.position, enemy.transform.position) < range)
            {
                float cone = Mathf.Cos(angleOffset * Mathf.Deg2Rad); 
                Vector3 heading = (enemy.transform.position - character.transform.position).normalized;

                if (Vector3.Dot(character.transform.forward, heading) > cone)
                {
                    character.CmdCauseDamageOverTime(enemy, damageAmount, damageTimer, GetName());
                    character.HealOverTime(healAmount, healTime);
                    if (enemy.gameObject.GetComponent<Boss>() != null)
                    {
                        CmdGenerateThreat(damageAmount, "Arda", enemy.gameObject);
                    }
                }
            }
        }
        character.resource -= resourceCost;
        cooldownTimer = Time.time + cooldown;
        isAiming = false;
    }

    protected override void AbilityAim(Vector3 position)
    {
        Transform newRotation = character.transform;
        newRotation.Rotate(Vector3.up, angleOffset);
        Debug.DrawLine(character.transform.position, character.transform.position + newRotation.forward * range, Color.green);
        newRotation.Rotate(Vector3.up, -2 * angleOffset);
        Debug.DrawLine(character.transform.position, character.transform.position + newRotation.forward * range, Color.green);
    }

    public override void AbilityUpdate()
    {
    }

    public override void Interrupt()
    {
        if (!character.isMovementAllowed)
        {
            isAiming = false;
        }
    }

    public override bool CanCast()
    {
        return character.resource >= resourceCost;
    }
}
