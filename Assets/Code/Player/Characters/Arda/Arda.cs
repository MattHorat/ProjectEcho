﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;

public class Arda : PlayerCharacter {
    public bool isStoringHeal = false;
    public FlamesOfRebirth healStorer;
    private float energyGainTimer = -1000000;

    public override void Start()
    {
        if (isLocalPlayer)
        {
            health = 6000;
            maxHealth = 6000;
            speed = 8F;
            resource = 200;
            resourceMax = 200;
            damageMultiplier = 0.8F;
            abilities = new List<Ability>();
            abilities.Add(gameObject.GetComponent<Burn>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<FlameBlink>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<FlameBreath>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<FlamesOfRebirth>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<FlameStrike>().SetCharacter(this));
            abilities.Add(gameObject.GetComponent<LifeField>().SetCharacter(this));
            base.Start();
        }
        characterName = "Arda";
    }

    public override void Update()
    {
        if (isLocalPlayer)
        {
            base.Update();
            if (Time.time - energyGainTimer > .5)
            {
                energyGainTimer = Time.time;
                if (resource < resourceMax)
                {
                    resource += 4;
                }
            }
        }
    }

    public override void LocalTakeHealing(float healAmount)
    {
        if (isStoringHeal)
        {
            healStorer.StoreHeal(healAmount);
        }
        else
        {
            base.LocalTakeHealing(healAmount);
        }
    }
}
