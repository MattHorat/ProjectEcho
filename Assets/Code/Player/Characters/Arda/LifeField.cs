﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class LifeField : Ability {
    private float range = 4;
    private float resourceCost = 15;
    private float cooldown = 18;
    public static float damageTickAmount = 95;
    public static float healTickAmount = 100;
    public static float extraThreatAmount = 95;
    public static float fieldLength = 8;
    public static float tickFrequency = 2;

    public GameObject lifeFieldPrefab;
    
    public override string GetName()
    {
        return "Life Field";
    }

	public override string GetTooltip()
	{
		return "<color=black>Cost - " + resourceCost + "		           <i>Instant</i>\nConjure a field of fire at the targeted location. Enemies take "
			+ "<b><color=maroon>" + damageTickAmount + "</color></b> damage per sec. If Arda stands in the field he will heal for "
			+ "<b><color=green>" + healTickAmount + "</color></b> health per sec. The field persists for " + fieldLength + " seconds.\n<i>" + cooldown + " sec cooldown</i></color>";
	}

    protected override void AbilityTrigger(Vector3 position)
    {
        if (Vector3.Distance(character.transform.position, position) < range)
        {
            character.resource -= resourceCost;
            CmdSpawnLifeField(position, character.gameObject);
            cooldownTimer = Time.time + cooldown;
        }
    }

    [Command]
    public void CmdSpawnLifeField(Vector3 position, GameObject creator)
    {
        GameObject spawn = (GameObject)Instantiate(lifeFieldPrefab, position, Quaternion.identity);
        NetworkServer.Spawn(spawn);
        spawn.GetComponent<LifeFieldAOE>().creator = creator.GetComponent<Character>();
    }

    protected override void AbilityAim(Vector3 position)
    {
        DrawRange(range, character.transform.position, 1);
        DrawRange(1, position, 2);
    }

    public override void AbilityUpdate()
    {
    }

    public override void Interrupt()
    {
    }

    public override bool CanCast()
    {
        return character.isMovementAllowed && character.resource > 35;
    }
}
