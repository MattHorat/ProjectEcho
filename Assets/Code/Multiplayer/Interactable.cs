﻿using System;
using UnityEngine;

interface Interactable
{
    void Interact();
}