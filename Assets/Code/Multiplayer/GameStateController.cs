﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameStateController : NetworkBehaviour {
    [SyncVar]
    private int lives = 5;
    private Dictionary<string, string> propertyList = new Dictionary<string, string>();
    private int readyClients = 0;

    public void StartGame(BossSpawner spawner)
    {
        if (readyClients == Network.connections.Length)
        {
            GameObject boss = spawner.SpawnBoss();
            GameObject[] characterSpawnerObjects = GameObject.FindGameObjectsWithTag("CharacterSpawner");
            foreach (GameObject characterSpawnerObject in characterSpawnerObjects)
            {
                CharacterSpawner characterSpawner = characterSpawnerObject.GetComponent<CharacterSpawner>();
                characterSpawner.StartGame();
                characterSpawner.RpcSetBoss(boss);
                characterSpawner.character.RpcCallStart();
                characterSpawner.character.tag = "Player";
                characterSpawner.character.isActive = true;
                characterSpawner.GetComponent<GameStartGUI>().isRunning = true;
            }
        }
    }

    public void StartGame()
    {
        if (readyClients == Network.connections.Length)
        {
            GameObject[] characterSpawnerObjects = GameObject.FindGameObjectsWithTag("CharacterSpawner");
            foreach (GameObject characterSpawnerObject in characterSpawnerObjects)
            {
                CharacterSpawner characterSpawner = characterSpawnerObject.GetComponent<CharacterSpawner>();
                characterSpawner.StartGame();
                characterSpawner.character.RpcCallStart();
                characterSpawner.character.tag = "Player";
                characterSpawner.character.isActive = true;
                characterSpawner.GetComponent<GameStartGUI>().isRunning = true;
            }
        }
    }

    public void RestartGame()
    {
        GameObject[] characterSpawners = GameObject.FindGameObjectsWithTag("CharacterSpawner");
        foreach (GameObject characterSpawner in characterSpawners)
        {
            characterSpawner.GetComponent<GameStartGUI>().isRunning = false;
            characterSpawner.GetComponent<CharacterSpawner>().character.isActive = false;
            characterSpawner.GetComponent<CharacterSpawner>().enabled = false;
        }
        lives = 5;
    }

    public void RemoveLife()
    {
        lives--;
        bool allDead = true;
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            if (player.GetComponent<PlayerCharacter>().isActive)
            {
                allDead = false;
            }
        }
        if (allDead)
        {
            foreach (GameObject player in players)
            {
                PlayerCharacter playerChar = player.GetComponent<PlayerCharacter>();
                playerChar.RpcRemoveCollision();
            }

            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (GameObject enemy in enemies)
            {
                Destroy(enemy);
            }

            RestartGame();
        }
    }

    public int GetLives()
    {
        return lives;
    }

    public void AddProperty(string propertyName, string propertyValue)
    {
        propertyList.Add(propertyName, propertyValue);
    }

    public string GetProperty(string propertyName)
    {
        string value = "";
        propertyList.TryGetValue(propertyName, out value);
        return value;
    }

    [Command]
    public void CmdSetReady()
    {
        readyClients++;
    }
}
