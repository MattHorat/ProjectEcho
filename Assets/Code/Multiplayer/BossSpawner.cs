﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;

public abstract class BossSpawner : NetworkBehaviour
{
    public abstract GameObject SpawnBoss();
    public abstract void DestroyBoss();
}