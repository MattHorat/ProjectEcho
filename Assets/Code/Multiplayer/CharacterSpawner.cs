﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;

public class CharacterSpawner : NetworkBehaviour
{
    public GameObject bossPrefab;
    public GameObject pogiPrefab;
    public GameObject ardaPrefab;
    public GameObject elainePrefab;
    public GameObject sevinaPrefab;

    //private List<Ability> abilityList;
    public PlayerCharacter character;

    public enum PlayerChars { Pogi, Arda, Elaine, Sevina };
    private bool characterSpawned = false;

    [ClientRpc]
    private void RpcSetCharacter(GameObject charToSet, GameObject owner)
    {
        if (owner.GetComponent<GameStartGUI>().isLocalPlayer)
        {
            character = charToSet.GetComponent<PlayerCharacter>();
            //abilityList = character.GetPossibleAbilities();
            PlayerUI healthBar = character.GetComponent<PlayerUI>();
            healthBar.character = character;
            character.enabled = false;
            GameObject multiplayerController = GameObject.Find("MultiplayerController");
            multiplayerController.GetComponent<InputController>().playerCharacter = character;
        }
    }

    [Command]
    public void CmdSpawnPlayer(PlayerChars selectedChar, GameObject owner)
    {
        GameObject spawnPrefab;
        switch (selectedChar)
        {
            case PlayerChars.Arda:
                spawnPrefab = ardaPrefab;
                break;
            case PlayerChars.Elaine:
                spawnPrefab = elainePrefab;
                break;
            case PlayerChars.Pogi:
                spawnPrefab = pogiPrefab;
                break;
            case PlayerChars.Sevina:
                spawnPrefab = sevinaPrefab;
                break;
            default:
                spawnPrefab = null;
                break;
        }

        GameObject spawnChar = (GameObject)Instantiate(spawnPrefab, new Vector3(5f, -0.5f, 0f), Quaternion.identity);
        NetworkServer.Spawn(spawnChar);
        character = spawnChar.GetComponent<PlayerCharacter>();
        character.enabled = false;
        RpcSetCharacter(spawnChar, owner);
    }

    [ClientRpc]
    public void RpcStartGame()
    {
        if (character != null)
        {
            character.enabled = true;
        }
    }

    public void StartGame()
    {
        var conn = connectionToClient;
        RpcStartGame();
        if (!characterSpawned)
        {
            NetworkServer.ReplacePlayerForConnection(conn, character.gameObject, 0);
            characterSpawned = true;
        }
    }

    [ClientRpc]
    public void RpcSetBoss(GameObject boss)
    {
        SetBoss(boss);
    }

    private void SetBoss(GameObject boss)
    {
        if (character)
        {
            character.boss = boss.GetComponent<Character>();
        }
    }
}