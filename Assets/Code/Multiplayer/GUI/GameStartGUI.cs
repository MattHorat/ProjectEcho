﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;

public class GameStartGUI : NetworkBehaviour
{
    public GameObject gameStateControllerPrefab;

    private bool characterSelected = false;
    //private List<Ability> abilityList;
    private CharacterSpawner characterSpawner;
    private GameStateController gameState;

    public bool isRunning;

    public void Start()
    {
        if (isServer)
        {
            GameObject gameStateController = (GameObject)Instantiate(gameStateControllerPrefab, new Vector3(0f, 0f, 0f), Quaternion.identity);
            NetworkServer.Spawn(gameStateController);
            gameState = gameStateController.GetComponent<GameStateController>();
        }
        else
        {
            gameState = GameObject.Find("GameStateController(Clone)").GetComponent<GameStateController>();
        }

        characterSpawner = GetComponent<CharacterSpawner>();
    }

    void OnGUI()
    {
        if (hasAuthority)
        {
            if (!characterSelected)
            {
                if (GUI.Button(new Rect(100, 100, 250, 100), "Pogi"))
                {
                    characterSpawner.CmdSpawnPlayer(CharacterSpawner.PlayerChars.Pogi, gameObject);
                    SetCharacterSelected();
                }
                if (GUI.Button(new Rect(400, 100, 250, 100), "Arda"))
                {
                    characterSpawner.CmdSpawnPlayer(CharacterSpawner.PlayerChars.Arda, gameObject);
                    SetCharacterSelected();
                }
                if (GUI.Button(new Rect(700, 100, 250, 100), "Elaine"))
                {
                    characterSpawner.CmdSpawnPlayer(CharacterSpawner.PlayerChars.Elaine, gameObject);
                    SetCharacterSelected();
                }
                if (GUI.Button(new Rect(100, 300, 250, 100), "Sevina"))
                {
                    characterSpawner.CmdSpawnPlayer(CharacterSpawner.PlayerChars.Sevina, gameObject);
                    SetCharacterSelected();
                }
            }
        }
        if (characterSelected && !isRunning)
        {
            if (isServer && GUI.Button(new Rect(100, 100, 150, 60), "Start game"))
            {
                ReolusSpawner spawner = gameObject.AddComponent<ReolusSpawner>();
                gameState.StartGame(spawner);
            }
            if (isServer && GUI.Button(new Rect(400, 100, 150, 60), "Start game - bossless"))
            {
                gameState.StartGame();
            }
            if (isClient && !isServer && GUI.Button(new Rect(100, 100, 150, 60), "Set Ready"))
            {
                gameState.CmdSetReady();
            }
        }
    }

    public void SetCharacterSelected()
    {
        characterSelected = true;
    }
}